<link href="./css/floating-labels.css" rel="stylesheet">
<form class="form-signin" id="login-form" action="./validaciones/validar-login.php">
  <div class="text-center mb-4">
	<img class="mb-4" src="./images/logo-login.png" alt="" width="90" height="72">
	<h1 class="h3 mb-3 font-weight-normal">Asistente Docente</h1>
	<p>Para ingresar al asistente docente por favor inicie sesion aqui con su matricula y contraseña.</p>
  </div>

  <div class="form-label-group">
	<input type="text" id="matricula" class="form-control" placeholder="Email address" required autofocus>
	<label for="matricula">Matricula</label>
  </div>

  <div class="form-label-group">
	<input type="password" id="contraseña" class="form-control" placeholder="Password" required>
	<label for="contraseña">Contraseña</label>
  </div>

  <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar Sesion</button>
  <p class="mt-5 mb-3 text-muted text-center">&copy; 2017-2018</p>
</form>