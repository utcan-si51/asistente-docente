<?php
if(session_status() == PHP_SESSION_NONE){
	session_start();
}
?>
<div class="nav-scroller bg-white box-shadow">
  <nav class="nav nav-underline">
	<a class="nav-link active" id="nav-1" href="./vista-rapida-horario.php">Horario</a>
  </nav>
</div>

<main role="main" class="container" id="container">
	<?php include "./vista-rapida-horario.php";?>
</main>