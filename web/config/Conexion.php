<?php
include('parametros.php');

class Conexion{
	
	function __construct(){ //constructor de la clase conexion, dos guiones bajos y construct()
		try{
		   $this->conexion=new mysqli(SERVIDOR,USUARIO,CONTRASENIA,BD)
		   or die ("No se pudo conectar, base de datos no cargada?");
		   mysqli_set_charset($this->conexion,"utf8");
		}catch(Exception $ex){
			throw $ex;  
		}
	}
	// para saber cuántos registros arroja la consulta
	function totalRegistros($sql){ 
		$resultado_consulta=$this->conexion->query($sql);
		$total_registros=$resultado_consulta->num_rows;
		return $total_registros;
	}
	
	// para cuando implement un SELECT
	function traerValores($sql){
		$resultado_consulta=$this->conexion->query($sql);
		$datos=null;
		while($fila=mysqli_fetch_assoc($resultado_consulta)){ //usa el nombre del campo como indice
			$datos=$fila;
		}
		return $datos;
	}
	//se utiliza para mostrar una lista de registros (SELECT)
	function seleccionarValores($sql){
		//se ejecuta la consulta
		$resultado_consulta=$this->conexion->query($sql);
		$datos=array();
		while ($datos[]=$resultado_consulta->fetch_assoc());
		return $datos;
	}
	
	function insertar($sql){
		$this->conexion->query($sql);
	}
  
}
?>