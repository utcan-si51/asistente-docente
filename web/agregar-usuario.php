<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Agregar un Usuario.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="agregar-usuario-form" action ="./validaciones/validar-agregar-usuario.php" method="post">
				<input class="form-control form-control-lg" type="text" id="au-matricula" placeholder="Matricula" required>
				<br>
				<input class="form-control form-control-lg" type="password" id="au-contrasena" placeholder="Contraseña" required>
				<br>
				<input class="form-control form-control-lg" type="text" id="au-nombre" placeholder="Nombre" required>
				<br>
				<input class="form-control form-control-lg" type="text" id="au-apellido" placeholder="Apellido" required>
				<br>
				<select class="custom-select custom-select-lg mb-3" id="au-tipo">
					<option value="0" selected>Profesor de Asignatura</option>
					<option value="1">Profesor de Tiempo Completo</option>
				</select>
				<br>
				<center><button type="submit" class="btn btn-success">Agregar</button></center>
			</form>
		</div>
	</div>
</div>