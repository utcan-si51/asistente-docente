<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$id = $_SESSION['id_Usuario'];
	$idAccion = mysqli_real_escape_string($conexion->conexion, $_REQUEST['id_editar']);
	$sql2 = "SELECT * FROM pendientes WHERE IDPendiente = $idAccion";
	$resultado=$conexion->traerValores($sql2);
	
	$titulo = $resultado['TituloPendiente'];
	$descripcion = $resultado['CuerpoPendiente'];
	$fecha = date( 'Y-m-d', strtotime($resultado['FechaHora']));
	$hora = date( 'h:m', strtotime($resultado['FechaHora']));

?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Modifcar Pendiente.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="modificar-pendiente-form" value="<?php echo $idAccion;?>" action ="./validaciones/validar-modificar-pendiente.php" method="post">
				<input class="form-control form-control-lg" type="text" id="ap-titulo" value="<?php echo $titulo;?>" required>
				<br>
				<input class="form-control form-control-lg" type="text" id="ap-descripcion" value="<?php echo $descripcion;?>" required>
				<br>
				<input class="form-control form-control-lg" type="datetime-local" id="ap-fecha" value="<?php echo $fecha.'T'.$hora;?>" required>
				<br>				
				<center><button type="submit" class="btn btn-success">Modficar</button></center>
			</form>
		</div>
	</div>
</div>