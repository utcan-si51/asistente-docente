<?php
if(session_status() == PHP_SESSION_NONE){
	session_start();
}
?>
<div class="nav-scroller bg-white box-shadow">
  <nav class="nav nav-underline">
	<a class="nav-link active" id="nav-1" href="./vista-rapida-accionMejora.php"><i class="fas fa-arrow-right"></i> Acciones de Mejora</a>
  </nav>
</div>

<main role="main" class="container" id="container">
	<?php include "vista-rapida-accionMejora.php";?>
</main>