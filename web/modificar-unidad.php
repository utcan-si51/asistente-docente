<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$id = $_REQUEST['id_editar'];
	$datos=$conexion->traerValores("SELECT * FROM temarios WHERE Estatus != 1 AND IDTemario= $id;");
	$titulo = $datos['Unidad'];
	$contenido = $datos['Contenido'];
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Modificar la unidad.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="modificar-unidad-form" value="<?php echo $id;?>" action ="./validaciones/validar-modificar-unidad.php" method="post">
				<input class="form-control form-control-lg" type="text" id="u-titulo" value="<?php echo $titulo;?>" required>
				<br>
				<textarea class="form-control" id="u-contenido" rows="3" required><?php echo $contenido;?></textarea>
				<br>
				<center><button type="submit" class="btn btn-success">Modificar</button></center>
			</form>
		</div>
	</div>
</div>