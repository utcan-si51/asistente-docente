<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Agregar una Asignatura.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="agregar-asignatura-form" action ="./validaciones/validar-agregar-asignatura.php" method="post">
				<input class="form-control form-control-lg" type="text" id="u-titulo" placeholder="Nombre de la Asignatura" required>
				<br>
				<center><button type="submit" class="btn btn-success">Agregar</button></center>
			</form>
		</div>
	</div>
</div>