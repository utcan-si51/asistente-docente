<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$id = $_SESSION['id_Usuario'];
	$sql = "SELECT alumnos.IDAlumno,alumnos.NombreAlumno,grupos.NombreGrupo FROM alumnos JOIN grupos ON alumnos.IDGrupo = grupos.IDGrupo WHERE alumnos.IDProfesor = $id ORDER BY grupos.NombreGrupo;";
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Agregar una Accion de Mejora.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="agregar-accionMejora-form" action ="./validaciones/validar-agregar-accionMejora.php" method="post">
				<input class="form-control form-control-lg" type="text" id="aa-actividad" placeholder="Actividad" required>
				<br>
				<select class="custom-select custom-select-lg mb-3" id="aa-alumno">
<?php
					$resultado=array_filter($conexion->seleccionarValores($sql));
					$i = 1;
					foreach($resultado as $datos){
						$id = $datos['IDAlumno'];
						$alumno = $datos['NombreAlumno'].' '.$datos['NombreGrupo'];
					echo <<<HTML
					<option value="$id">$alumno</option>
HTML;
					}
?>
				</select>
				<br>
				<input class="form-control form-control-lg" type="datetime-local" id="aa-fecha" placeholder="Fecha y Hora" required>
				<br>				
				<center><button type="submit" class="btn btn-success">Agregar</button></center>
			</form>
		</div>
	</div>
</div>