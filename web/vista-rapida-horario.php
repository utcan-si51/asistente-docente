<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$idU = $_SESSION['id_Usuario'];
?>
<?php
	
	$sqldias = "SELECT * FROM dias;";
	
	$resultado=array_filter($conexion->seleccionarValores($sqldias));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDDia'];
		$nombre = $datos['Dia'];
		$cantArray=$conexion->traerValores("SELECT COUNT(IDHorario) AS cant FROM horariodetalle WHERE Dia= $id AND IDProfesor = $idU;");
		$cant = $cantArray['cant'];
	echo <<<HTML
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">
		$nombre <span class="badge badge-pill bg-light align-text-bottom" id="cantX$i">$cant</span>
	</h6>
HTML;
		$resultado=array_filter($conexion->seleccionarValores("SELECT m.IDHorario, grupos.NombreGrupo, asignaturas.NombreAsignatura, aulas.NombreAula, u1.Hora AS 'HoraInicio', u2.Hora AS 'HoraFin' FROM horariodetalle m JOIN grupos ON m.IDGrupo = grupos.IDGrupo JOIN asignaturas ON m.IDAsignatura = asignaturas.IDAsignatura JOIN aulas ON m.IDAula = aulas.IDAula LEFT JOIN `horas` u1 ON (u1.IDHora = m.HoraInicio) LEFT JOIN `horas` u2 ON (u2.IDHora = m.HoraFinal) WHERE m.IDProfesor = $idU AND m.Dia = $id ORDER BY u1.Hora;"));
		$j = 1;
		foreach($resultado as $datos){
		$id2 = $datos['IDHorario'];
		$grupo = $datos['NombreGrupo'];
		$asignatura = $datos['NombreAsignatura'];
		$aula = $datos['NombreAula'];
		$inicio = $datos['HoraInicio'];
		$fin = $datos['HoraFin'];
	echo <<<HTML
		<div class="media text-muted pt-3">
			<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
				<strong class="text-gray-dark">$inicio - $fin</strong> - Grupo: <strong class="text-gray-dark">$grupo</strong> - Aula: <strong class="text-gray-dark">$aula</strong><br><br>
				$asignatura
			</p>
			<div class="botones-listado">
				<form id="eliminar$i-$j" value="$id2" action="./validaciones/validar-eliminar-clase.php" method="post">
					<center><button type="submit" class="btn btn-danger btn-sm">Eliminar</button></center>
				</form>
			</div>
		</div>
HTML;
		$j++;
		}
	echo <<<HTML
	<small class="d-block text-right mt-3">
	<form id="agregar-clase$i" value = "$id" action="./agregar-clase.php" method="post">
		<button type="submit" class="btn btn-success btn-sm">Agregar Clase</button>
	</form>
</small>
</div>
HTML;
	$i++;
	}
?>	