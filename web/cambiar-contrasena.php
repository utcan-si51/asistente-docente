<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once('./config/Conexion.php');
	
	$conexion= new Conexion;
	
	if(!isset($_REQUEST['id_editar'])){
		$id = $_SESSION['id_Usuario'];
	}else {
		$id=mysqli_real_escape_string($conexion->conexion, $_REQUEST['id_editar']);
	}
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<?php
		if(isset($_REQUEST['id_editar'])){
			echo '	<h6 class="border-bottom border-gray pb-2 mb-0">Ingrese una nueva contraseña para el usuario.</h6>';
		}else{
			echo '<h6 class="border-bottom border-gray pb-2 mb-0">Ingrese una nueva contraseña para su cuenta.</h6>';
		}
	?>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="cambiar-contra-form" value="<?php echo $id;?>" action ="./validaciones/validar-cambiar-contra.php" method="post">
				<input class="form-control form-control-lg" type="password" id="cc-contrasena" placeholder="Contraseña" required>
				<br>
				<center><button type="submit" class="btn btn-success">Cambiar</button></center>
			</form>
		</div>
	</div>
</div>