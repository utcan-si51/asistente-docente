<?php
session_start();
?>
<!DOCTYPE html>
<html>	
<head>
	<title>Asistente Docente</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="./css/bootstrap.css" rel='stylesheet' type='text/css' />
	<link href="./css/offcanvas.css" rel="stylesheet">
	<!--<script src="./js/funciones-min.js"></script>-->
	<script src="./js/funciones.js"></script>
</head>
<body id="content" class="bg-light">
<?php 
		if (!isset($_SESSION['id_Usuario'])) {
			include "login.php";
		} else {
			include "home.php";
		}
	?>
</body>
</html>