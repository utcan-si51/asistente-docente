<?php
if(session_status() == PHP_SESSION_NONE){
	session_start();
}
?>
<div class="nav-scroller bg-white box-shadow">
  <nav class="nav nav-underline">
	<a class="nav-link active" id="nav-1" href="./vista-rapida-config.php">Configuraciones</a>
	<?php
		if($_SESSION['tipo_Usuario'] == 2){
			echo '<a class="nav-link" id="nav-2" href="./usuarios.php">Usuarios</a>';
		}
	?>
	<a class="nav-link" id="nav-3" href="grupos.php">Grupos</a>
	<a class="nav-link" id="nav-4" href="alumnos.php">Alumnos</a>
	<a class="nav-link" id="nav-5" href="cambiar-contrasena.php">Contraseña</a>
  </nav>
</div>

<main role="main" class="container" id="container">
	<?php include "vista-rapida-config.php";?>
</main>