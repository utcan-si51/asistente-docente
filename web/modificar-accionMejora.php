<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$id = $_SESSION['id_Usuario'];
	$sql = "SELECT alumnos.IDAlumno,alumnos.NombreAlumno,grupos.NombreGrupo FROM alumnos JOIN grupos ON alumnos.IDGrupo = grupos.IDGrupo WHERE alumnos.IDProfesor = $id ORDER BY grupos.NombreGrupo;";
	
	$idAccion = mysqli_real_escape_string($conexion->conexion, $_REQUEST['id_editar']);
	$sql2 = "SELECT * FROM acciones_de_mejora WHERE IDActMejora = $idAccion";
	$resultado=$conexion->traerValores($sql2);
	
	$actividad = $resultado['Actividad'];
	$alumno = $resultado['IDAlumno'];
	$fecha = date( 'Y-m-d', strtotime($resultado['FechaHora']));
	$hora = date( 'h:m', strtotime($resultado['FechaHora']));

?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Modifcar Accion de Mejora.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="modificar-accionMejora-form" value="<?php echo $idAccion;?>" action ="./validaciones/validar-modificar-accionMejora.php" method="post">
				<input class="form-control form-control-lg" type="text" id="aa-actividad" value="<?php echo $actividad;?>" required>
				<br>
				<select class="custom-select custom-select-lg mb-3" id="aa-alumno">
<?php
					$resultado=array_filter($conexion->seleccionarValores($sql));
					$i = 1;
					foreach($resultado as $datos){
						$idAlumno = $datos['IDAlumno'];
						$alumnoNombre = $datos['NombreAlumno'].' '.$datos['NombreGrupo'];
						if($idAlumno == $alumno){
							echo '<option value="'.$idAlumno.'" selected>'.$alumnoNombre.'</option>';
						}else{
							echo '<option value="'.$idAlumno.'">'.$alumnoNombre.'</option>';
						}						
					echo <<<HTML
					<option value="$id">$alumno</option>
HTML;
					}
?>
				</select>
				<br>
				<input class="form-control form-control-lg" type="datetime-local" id="aa-fecha" value="<?php echo $fecha.'T'.$hora;?>" required>
				<br>				
				<center><button type="submit" class="btn btn-success">Modficar</button></center>
			</form>
		</div>
	</div>
</div>