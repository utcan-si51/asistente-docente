<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$id = $_SESSION['id_Usuario'];
	
	$sqlcant = "SELECT COUNT(IDCurso) AS cant FROM cursos WHERE Estatus != 1 AND IdProfesor= $id;";
	$sql = "SELECT * FROM cursos WHERE Estatus != 1 AND IdProfesor= $id ORDER BY FechaHora;";
	
	$cant=$conexion->traerValores($sqlcant);
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Cursos <span class="badge badge-pill bg-light align-text-bottom" id="cant"><?php echo $cant['cant'];?></span></h6>
<?php
	$resultado=array_filter($conexion->seleccionarValores($sql));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDCurso'];
		$descripcion = $datos['DescripcionCurso'];
		$fecha = date( 'd-m-Y H:i', strtotime($datos['FechaHora']));
		$lugar = $datos['Lugar'];
	echo <<<HTML
		<div class="media text-muted pt-3">
			<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
				<strong class="text-gray-dark">Fecha y Hora:</strong> $fecha <strong class="text-gray-dark">- Lugar:</strong> $lugar<br><br>
				$descripcion
			</p>
			<div class="botones-listado">
				<form id="eliminar$i" value="$id" action="./validaciones/validar-concluir-curso.php" method="post">
					<center><button type="submit" class="btn btn-info btn-sm">Concluir</button></center>
				</form>
				<form id="editar$i" value="$id" action="./modificar-curso.php" method="post">
					<center><button type="submit" class="btn btn-warning btn-sm">Modificar</button></center>
				</form>
			</div>
		</div>
HTML;
	$i++;
	}
?>	
<small class="d-block text-right mt-3">
	<form id="agregar-usuario" action="./agregar-curso.php" method="post">
		<button type="submit" class="btn btn-success">Agregar</button>
	</form>
</small>
</div>