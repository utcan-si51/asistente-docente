<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$idU = $_SESSION['id_Usuario'];
	
	$sqlcant = "SELECT COUNT(IDGrupo) AS cant FROM grupos WHERE IDProfesor = $idU AND Estatus != 1;";
	$sql = "SELECT grupos.IDGrupo, grupos.NombreGrupo, carreras.NombreCarrera FROM grupos JOIN carreras ON grupos.IDCarrera = carreras.IDCarrera WHERE IDProfesor = $idU AND Estatus != 1;";
	
	$cant=$conexion->traerValores($sqlcant);
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Grupos <span class="badge badge-pill bg-light align-text-bottom" id="cant"><?php echo $cant['cant'];?></span></h6>
<?php
	$resultado=array_filter($conexion->seleccionarValores($sql));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDGrupo'];
		$nombre = $datos['NombreGrupo'];
		$carrera = $datos['NombreCarrera'];
	echo <<<HTML
		<div class="media text-muted pt-3">
			<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
				<strong class="text-gray-dark">Grupo:</strong> $nombre<br><br>
				$carrera
			</p>
			<div class="botones-listado">
				<form id="eliminar$i" value="$id" action="./validaciones/validar-eliminar-grupo.php" method="post">
					<center><button type="submit" class="btn btn-danger btn-sm">Eliminar</button></center>
				</form>
			</div>
		</div>
HTML;
	$i++;
	}
?>	
<small class="d-block text-right mt-3">
	<form id="agregar-usuario" action="./agregar-grupo.php" method="post">
		<button type="submit" class="btn btn-success">Agregar</button>
	</form>
</small>
</div>