<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
$conexion=new Conexion;
?>
<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$dia = date('N', strtotime(date('l')));
$fecha = date("Y-m-d");

$idU = $_SESSION['id_Usuario'];

$cantArray = $conexion->traerValores("SELECT COUNT(IDHorario) AS cant FROM horariodetalle WHERE Dia= $dia AND IDProfesor = $idU;");
$cantHorario = $cantArray['cant'];
$cantArray = $conexion->traerValores("SELECT COUNT(IDPendiente) AS cant FROM pendientes WHERE DATE(FechaHora) = CURRENT_DATE() AND IdProfesor = $idU AND Estatus != 1;");
$cantAgenda = $cantArray['cant'];

?>
<div class="nav-scroller bg-white box-shadow">
  <nav class="nav nav-underline">
	<a class="nav-link active" id="nav-1" href="./vista-rapida-inicio.php">Resumen</a>
  </nav>
</div>

<main role="main" class="container">
  <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded box-shadow">
	<img class="mr-3" src="./images/usuarios/a4.jpg" alt="" width="48" height="48">
	<div class="lh-100">
		<?php
if ($_SESSION['tipo_Usuario'] == 2) {
    echo '<h6 class="mb-0 text-white lh-100">Bienvenido Administrador!</h6>';
} else {
    echo '<h6 class="mb-0 text-white lh-100">Bienvenido Profesor!</h6>';
}
?>
	  <small><?php
echo $_SESSION['nombre_Usuario'] . ' ' . $_SESSION['apellido_Usuario'];
?></small>
	</div>
  </div>
<?php
	if($_SESSION['tipo_Usuario'] != 2){
?>
  <div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Horario de Hoy <span class="badge badge-pill bg-light align-text-bottom"><?php echo $cantHorario;?></span></h6>
<?php
$resultado = array_filter($conexion->seleccionarValores("SELECT m.IDHorario, grupos.NombreGrupo, asignaturas.NombreAsignatura, aulas.NombreAula, u1.Hora AS 'HoraInicio', u2.Hora AS 'HoraFin' FROM horariodetalle m JOIN grupos ON m.IDGrupo = grupos.IDGrupo JOIN asignaturas ON m.IDAsignatura = asignaturas.IDAsignatura JOIN aulas ON m.IDAula = aulas.IDAula LEFT JOIN `horas` u1 ON (u1.IDHora = m.HoraInicio) LEFT JOIN `horas` u2 ON (u2.IDHora = m.HoraFinal) WHERE m.IDProfesor = $idU AND m.Dia = $dia ORDER BY u1.Hora;"));
if (empty($resultado)) {
    echo '<center>¡Usted no tiene clases hoy!</center>';
}
$j = 1;
foreach ($resultado as $datos) {
    $id2        = $datos['IDHorario'];
    $grupo      = $datos['NombreGrupo'];
    $asignatura = $datos['NombreAsignatura'];
    $aula       = $datos['NombreAula'];
    $inicio     = $datos['HoraInicio'];
    $fin        = $datos['HoraFin'];
    echo <<<HTML
		<div class="media text-muted pt-3">
			<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
				<strong class="text-gray-dark">$inicio - $fin</strong> - Grupo: <strong class="text-gray-dark">$grupo</strong> - Aula: <strong class="text-gray-dark">$aula</strong><br><br>
				$asignatura
			</p>
		</div>
HTML;
    $j++;
}
?>	
	<small class="d-block text-right mt-3">
	  <a id="homenav1" href="./horario.php">Ver Horario Completo</a>
	</small>
  </div>
  
  <div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Pendientes para Hoy <span class="badge badge-pill bg-light align-text-bottom"><?php echo $cantAgenda;?></span></h6>
<?php
	$resultado=array_filter($conexion->seleccionarValores("SELECT * FROM pendientes WHERE DATE(FechaHora) = CURRENT_DATE() AND IdProfesor = $idU AND Estatus != 1;"));
	if (empty($resultado)) {
		echo '<center>¡Usted no tiene pendientes para hoy!</center>';
	}	
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDPendiente'];
		$nombre = $datos['TituloPendiente'];
		$descripcion = $datos['CuerpoPendiente'];
		$fecha = date( 'd-m-Y H:i', strtotime($datos['FechaHora']));
	echo <<<HTML
		<div class="media text-muted pt-3">
			<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
				<strong class="text-gray-dark">Fecha y Hora:</strong> $fecha <strong class="text-gray-dark">- Titulo:</strong> $nombre<br><br>
				$descripcion
			</p>
		</div>
HTML;
	$i++;
	}
?>	
	<small class="d-block text-right mt-3">
	  <a id="homenav2" href="./agenda.php">Ver Agenda</a>
	</small>
  </div>
<?php
	}
?>  
</main>