<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
      <a class="navbar-brand" href=""><img src="./images/logo-login.png" alt="" width="40" height="30"> Asistente Docente</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" id="menu-1" href="./vista-rapida-inicio.php">Inicio</a>
          </li>
		  <?php
			if($_SESSION['tipo_Usuario'] != 2){
				echo <<<HTML
				<li class="nav-item">
					<a class="nav-link" id="menu-2" href="./horario.php">Horario</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="menu-3" href="./asignaturas.php">Asignaturas</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="menu-4" href="./agenda.php">Agenda</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="menu-5" href="./accionMejora.php">Acciones de Mejora</a>
				</li>
HTML;
			}

			if($_SESSION['tipo_Usuario'] != 0 && $_SESSION['tipo_Usuario'] != 2){
				echo <<<HTML
				<li class="nav-item">
					<a class="nav-link" id="menu-6" href="./visitas.php">Visitas</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="menu-7" href="./cursos.php">Cursos</a>
				</li>
HTML;
			}
			?>
          <li class="nav-item">
            <a class="nav-link" id="menu-8" href="./configuraciones.php">Configuraciones</a>
          </li>		  
        </ul>
        <form class="form-inline my-2 my-lg-0" id="cerrar-form" action="./cerrar-sesion.php">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cerrar Sesion</button>
        </form>
      </div>
    </nav>
	
	<div id="inner">
		<?php include "vista-rapida-inicio.php"?>
	</div>