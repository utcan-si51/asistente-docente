<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$id = $_SESSION['id_Usuario'];
	$sql = "SELECT * FROM grupos WHERE IDProfesor = $id;";
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Agregar una Visita.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="agregar-visita-form" action ="./validaciones/validar-agregar-visita.php" method="post">
				<input class="form-control form-control-lg" type="text" id="av-nombre" placeholder="Nombre" required>
				<br>			
				<input class="form-control form-control-lg" type="text" id="av-descripcion" placeholder="Descripcion" required>
				<br>
				<input class="form-control form-control-lg" type="datetime-local" id="av-fecha" placeholder="Fecha y Hora" required>
				<br>
				<select class="custom-select custom-select-lg mb-3" id="av-grupo">
<?php
					$resultado=array_filter($conexion->seleccionarValores($sql));
					$i = 1;
					foreach($resultado as $datos){
						$id = $datos['IDGrupo'];
						$grupo = $datos['NombreGrupo'];
					echo <<<HTML
					<option value="$id">$grupo</option>
HTML;
					}
?>
				</select>
				<br>
				<center><button type="submit" class="btn btn-success">Agregar</button></center>
			</form>
		</div>
	</div>
</div>