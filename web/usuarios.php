<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$sqlcant = "SELECT COUNT(IDProfesor) AS cant FROM profesores WHERE TipoProfesor != 2 AND Estatus != 1;";
	$sql = "SELECT * FROM profesores WHERE TipoProfesor != 2 AND Estatus != 1;";
	
	$cant=$conexion->traerValores($sqlcant);
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Usuarios <span class="badge badge-pill bg-light align-text-bottom" id="cant"><?php echo $cant['cant'];?></span></h6>
<?php
	$resultado=array_filter($conexion->seleccionarValores($sql));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IdProfesor'];
		$matricula = $datos['MatriculaProfesor'];
		$nombre = $datos['NombreProfesor'];
		$apellido = $datos['ApellidoProfesor'];
		$tipo = $datos['TipoProfesor'];
		if($tipo == 0){
			$tipo = "Profesor de Asignatura";
		} else if($tipo == 1){
			$tipo = "Profesor de Tiempo Completo";
		}
	echo <<<HTML
		<div class="media text-muted pt-3">
			<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
				<strong class="text-gray-dark">Matricula:</strong> $matricula <strong class="text-gray-dark">- Nombre:</strong> $nombre $apellido<br><br>
				$tipo
			</p>
			<div class="botones-listado">
				<form id="eliminar$i" value="$id" action="./validaciones/validar-eliminar-usuario.php" method="post">
					<center><button type="submit" class="btn btn-danger btn-sm">Eliminar</button></center>
				</form>
				<form id="editar$i" value="$id" action="./cambiar-contrasena.php" method="post">
					<center><button type="submit" class="btn btn-warning btn-sm">Cambiar Contraseña</button></center>
				</form>
			</div>
		</div>
HTML;
	$i++;
	}
?>	
<small class="d-block text-right mt-3">
	<form id="agregar-usuario" action="./agregar-usuario.php" method="post">
		<button type="submit" class="btn btn-success">Agregar</button>
	</form>
</small>
</div>