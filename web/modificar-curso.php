<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once('./config/Conexion.php');
	
	$conexion= new Conexion;	
	
	$id = $_SESSION['id_Usuario'];
	$idCurso = mysqli_real_escape_string($conexion->conexion, $_REQUEST['id_editar']);
	
	$sql = "SELECT * FROM cursos WHERE IDCurso = $idCurso;";
	
	$resultado=$conexion->traerValores($sql);

	$descripcion=$resultado['DescripcionCurso'];
	$fecha = date( 'Y-m-d', strtotime($resultado['FechaHora']));
	$hora = date( 'h:m', strtotime($resultado['FechaHora']));
	$lugar=$resultado['Lugar'];
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Modificar este Curso.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="modificar-curso-form" value="<?php echo $idCurso;?>" action ="./validaciones/validar-modificar-curso.php" method="post">
				<input class="form-control form-control-lg" type="text" id="ac-descripcion" value="<?php echo $descripcion;?>" required>
				<br>
				<input class="form-control form-control-lg" type="datetime-local" id="ac-fechahora" value="<?php echo $fecha.'T'.$hora;?>" required>
				<br>
				<input class="form-control form-control-lg" type="text" id="ac-lugar" value="<?php echo $lugar;?>" required>
				<br>
				<center><button type="submit" class="btn btn-success">Guardar</button></center>
			</form>
		</div>
	</div>
</div>