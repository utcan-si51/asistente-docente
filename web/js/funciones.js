addEventListener('load', inicializarEventos, false);

function inicializarEventos(){
	var obLogin = document.getElementById('login-form');
	if(obLogin != null){
		obLogin.addEventListener('submit', presionEnlace, false);
	}
	
	var obCerrar = document.getElementById('cerrar-form');
	if(obCerrar != null){
		obCerrar.addEventListener('submit', presionEnlaceCerrar, false);
	}
	
	for(var f=1; f<=8; f++){
		var obMenu = document.getElementById('menu-'+f);
		if(obMenu != null){
			obMenu.addEventListener('click', presionEnlaceMenu, false);
		}
	}
	
	for(var f=1; f<=5; f++){
		var obNav = document.getElementById('nav-'+f);
		if(obNav != null){
			obNav.addEventListener('click', presionEnlaceNav, false);
		}
	}
	
	if(document.getElementById('cant') != null){
		var cant = document.getElementById('cant').innerHTML;
	}
	
	for(var f = 1; f<=cant; f++){
		var obEliminar = document.getElementById('eliminar'+f);
		if(obEliminar != null){
			obEliminar.addEventListener('submit', presionEnlaceEliminar, false);			
		}
	}	
	
	for(var f = 1; f<=cant; f++){	
		var obEditar = document.getElementById('editar'+f);
		if(obEditar != null){
			obEditar.addEventListener('submit', presionEnlaceEditar, false);
		}
	}
	
	var obCambiarContra = document.getElementById('cambiar-contra-form');
	if(obCambiarContra != null){
		obCambiarContra.addEventListener('submit', presionEnlaceCC, false);
	}
	
	var obAgregarUsuario = document.getElementById('agregar-usuario');
	if(obAgregarUsuario != null){
		obAgregarUsuario.addEventListener('submit', presionEnlaceAU, false);
	}
	
	var obAgregarUsuario = document.getElementById('agregar-usuario-form');
	if(obAgregarUsuario != null){
		obAgregarUsuario.addEventListener('submit', presionEnlaceAUF, false);
	}
	
	var obAgregarCurso = document.getElementById('agregar-curso-form');
	if(obAgregarCurso != null){
		obAgregarCurso.addEventListener('submit', presionEnlaceACF, false);
	}
	
	var obModificarCurso = document.getElementById('modificar-curso-form');
	if(obModificarCurso != null){
		obModificarCurso.addEventListener('submit', presionEnlaceMCF, false);
	}

	var obAgregarVisita = document.getElementById('agregar-visita-form');
	if(obAgregarVisita != null){
		obAgregarVisita.addEventListener('submit', presionEnlaceAVF, false);
	}
	
	var obModificarVisita = document.getElementById('modificar-visita-form');
	if(obModificarVisita != null){
		obModificarVisita.addEventListener('submit', presionEnlaceMVF, false);
	}
	
	var obAgregarAccionMejora = document.getElementById('agregar-accionMejora-form');
	if(obAgregarAccionMejora != null){
		obAgregarAccionMejora.addEventListener('submit', presionEnlaceAAF, false);
	}

	var obModificarAccion = document.getElementById('modificar-accionMejora-form');
	if(obModificarAccion != null){
		obModificarAccion.addEventListener('submit', presionEnlaceMAF, false);
	}
	
	for(var f = 1; f<=cant; f++){
		var obEliminar = document.getElementById('concluirA'+f);
		if(obEliminar != null){
			obEliminar.addEventListener('submit', presionEnlaceConcluirA, false);			
		}
	}
	
	var obAgregarPendiente = document.getElementById('agregar-pendiente-form');
	if(obAgregarPendiente != null){
		obAgregarPendiente.addEventListener('submit', presionEnlaceAPF, false);
	}
	var obModificarPendiente = document.getElementById('modificar-pendiente-form');
	if(obModificarPendiente != null){
		obModificarPendiente.addEventListener('submit', presionEnlaceMPF, false);
	}
	
	if(document.getElementById('cantAsig') != null){
		var cantAsig = document.getElementById('cantAsig').value;
	}
	
	if(cantAsig != null){
		for(var f=1; f<=cantAsig; f++){
			var obAgregarUnidad = document.getElementById('agregar-usuario'+f);
			if(obAgregarUnidad != null){
				obAgregarUnidad.addEventListener('submit', presionEnlaceAUN, false);
			}
		}
	}
	
	for(var f = 1; f<=cantAsig; f++){
		var obEliminarAsig = document.getElementById('eliminarasig'+f);
		if(obEliminarAsig != null){
			obEliminarAsig.addEventListener('submit', presionEnlaceEliminar, false);			
		}
	}	
	
	var obAgregarUnidadF = document.getElementById('agregar-unidad-form');
	if(obAgregarUnidadF != null){
		obAgregarUnidadF.addEventListener('submit', presionEnlaceAUFO, false);
	}
	var obModificarUnidad = document.getElementById('modificar-unidad-form');
	if(obModificarUnidad != null){
		obModificarUnidad.addEventListener('submit', presionEnlaceAUFO, false);
	}	
	
	var obAgregarAsignatura = document.getElementById('agregar-asignatura-form');
	if(obAgregarAsignatura != null){
		obAgregarAsignatura.addEventListener('submit', presionEnlaceAAAF, false);
	}
	
	for(var f=1; f<=5; f++){
		var obAgregarClase = document.getElementById('agregar-clase'+f);
		if(obAgregarClase != null){
			obAgregarClase.addEventListener('submit', presionEnlaceAUN, false);
		}
	}
	
	var obAgregarClase = document.getElementById('agregar-clase-form');
	if(obAgregarClase != null){
		obAgregarClase.addEventListener('submit', presionEnlaceAACF, false);
	}
	
	for(var f=1; f<=5; f++){
		if(document.getElementById('cantX'+f) != null){
			var cantHorario = document.getElementById('cantX'+f).innerHTML;
		}
		if(cantHorario != null){
			for(var e=1; e<=cantHorario; e++){
				var obEliminarHorario = document.getElementById('eliminar'+f+'-'+e);
				if(obEliminarHorario != null){
					obEliminarHorario.addEventListener('submit', presionEnlaceEliminar, false);
				}
			}
		}
	}
	
	for(var f=1; f<=2; f++){
		var obNav = document.getElementById('homenav'+f);
		if(obNav != null){
			obNav.addEventListener('click', presionEnlaceMenu, false);
		}
	}
	
	var obAgregarGrupo = document.getElementById('agregar-grupo-form');
	if(obAgregarGrupo != null){
		obAgregarGrupo.addEventListener('submit', presionEnlaceAAGF, false);
	}	
	
	var obAgregarAlumno = document.getElementById('agregar-alumno-form');
	if(obAgregarAlumno != null){
		obAgregarAlumno.addEventListener('submit', presionEnlaceAF, false);
	}	

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function presionEnlace(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargar(url);
}
function presionEnlaceCerrar(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarCerrar(url);
}
function presionEnlaceMenu(e){
	e.preventDefault();
	var url = e.target.href;
	cargarMenu(url);
}
function presionEnlaceNav(e){
	e.preventDefault();
	var url = e.target.href;
	cargarNav(url);
}
function presionEnlaceEliminar(e){
	e.preventDefault();
	if (window.confirm("¿Esta seguro que quiere eliminar esto?")) {
		var id = e.target.getAttribute('value');
		var url = e.target.getAttribute('action');
		cargarEliminar(url,id);
	}
}
function presionEnlaceEditar(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
	cargarEditar(url,id);
}
function presionEnlaceCC(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
	cargarCC(url, id);
}
function presionEnlaceAU(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarAU(url);
}
function presionEnlaceAUF(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarAUF(url);
}
function presionEnlaceACF(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarACF(url);
}
function presionEnlaceMCF(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
	cargarMCF(url,id);
}
function presionEnlaceAVF(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarAVF(url);
}
function presionEnlaceMVF(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
	cargarMVF(url,id);
}
function presionEnlaceAAF(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarAAF(url);
}
function presionEnlaceMAF(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
	cargarMAF(url,id);
}
function presionEnlaceConcluirA(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
    var califcacion = prompt("Ingrese la califcacion obtenida:");
	
	cargarConcluirA(url,id,califcacion);
}
function presionEnlaceAPF(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarAPF(url);
}
function presionEnlaceMPF(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
	cargarMPF(url,id);
}
function presionEnlaceAUN(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
	cargarAUN(url,id);
}
function presionEnlaceAUFO(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
	cargarAUFO(url,id);
}
function presionEnlaceAAAF(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarAAAF(url);
}
function presionEnlaceAACF(e){
	e.preventDefault();
	var id = e.target.getAttribute('value');
	var url = e.target.getAttribute('action');
	cargarAACF(url,id);
}
function presionEnlaceAAGF(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarAAGF(url);
}
function presionEnlaceAF(e){
	e.preventDefault();
	var url = e.target.getAttribute('action');
	cargarAF(url);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function obtenerdatos(){
	var data="";
	var dato1 = document.getElementById('matricula').value;
	var dato2 = SHA1(document.getElementById('contraseña').value);
	data="matricula="+encodeURIComponent(dato1)+"&contraseña="+encodeURIComponent(dato2);
	return data;
}
function obtenerdatosCC(id){
	var data="";
	var dato1 = SHA1(document.getElementById('cc-contrasena').value);
	data="contrasena="+encodeURIComponent(dato1)+"&id="+encodeURIComponent(id);
	return data;
}
function obtenerdatosAUF(){
	var data="";
	var dato1 = document.getElementById('au-matricula').value;
	var dato2 = SHA1(document.getElementById('au-contrasena').value);
	var dato3 = document.getElementById('au-nombre').value;
	var dato4 = document.getElementById('au-apellido').value;
	var dato5 = document.getElementById('au-tipo').value;
	data="matricula="+encodeURIComponent(dato1)+"&contrasena="+encodeURIComponent(dato2)+"&nombre="+encodeURIComponent(dato3)+"&apellido="+encodeURIComponent(dato4)+"&tipo="+encodeURIComponent(dato5);
	return data;
}
function obtenerdatosACF(){
	var data="";
	var dato1 = document.getElementById('ac-descripcion').value;
	var dato2 = document.getElementById('ac-fechahora').value;
	var dato3 = document.getElementById('ac-lugar').value;
	data="descripcion="+encodeURIComponent(dato1)+"&fechahora="+encodeURIComponent(dato2)+"&lugar="+encodeURIComponent(dato3);
	return data;
}
function obtenerdatosMCF(id){
	var data="";
	var dato1 = document.getElementById('ac-descripcion').value;
	var dato2 = document.getElementById('ac-fechahora').value;
	var dato3 = document.getElementById('ac-lugar').value;
	data="descripcion="+encodeURIComponent(dato1)+"&fechahora="+encodeURIComponent(dato2)+"&lugar="+encodeURIComponent(dato3)+"&id="+encodeURIComponent(id);
	return data;
}
function obtenerdatosAVF(){
	var data="";
	var dato1 = document.getElementById('av-nombre').value;
	var dato2 = document.getElementById('av-descripcion').value;
	var dato3 = document.getElementById('av-fecha').value;
	var dato4 = document.getElementById('av-grupo').value;	
	data="nombre="+encodeURIComponent(dato1)+"&descripcion="+encodeURIComponent(dato2)+"&fecha="+encodeURIComponent(dato3)+"&grupo="+encodeURIComponent(dato4);
	return data;
}
function obtenerdatosMVF(id){
	var data="";
	var dato1 = document.getElementById('av-nombre').value;
	var dato2 = document.getElementById('av-descripcion').value;
	var dato3 = document.getElementById('av-fecha').value;
	var dato4 = document.getElementById('av-grupo').value;	
	data="nombre="+encodeURIComponent(dato1)+"&descripcion="+encodeURIComponent(dato2)+"&fecha="+encodeURIComponent(dato3)+"&grupo="+encodeURIComponent(dato4)+"&id="+encodeURIComponent(id);
	return data;
}
function obtenerdatosAAF(){
	var data="";
	var dato1 = document.getElementById('aa-actividad').value;
	var dato2 = document.getElementById('aa-alumno').value;
	var dato3 = document.getElementById('aa-fecha').value;	
	data="actividad="+encodeURIComponent(dato1)+"&alumno="+encodeURIComponent(dato2)+"&fecha="+encodeURIComponent(dato3);
	return data;
}
function obtenerdatosMAF(id){
	var data="";
	var dato1 = document.getElementById('aa-actividad').value;
	var dato2 = document.getElementById('aa-alumno').value;
	var dato3 = document.getElementById('aa-fecha').value;	
	data="actividad="+encodeURIComponent(dato1)+"&alumno="+encodeURIComponent(dato2)+"&fecha="+encodeURIComponent(dato3)+"&id="+encodeURIComponent(id);
	return data;
}
function obtenerdatosAPF(){
	var data="";
	var dato1 = document.getElementById('ap-titulo').value;
	var dato2 = document.getElementById('ap-descripcion').value;
	var dato3 = document.getElementById('ap-fecha').value;	
	data="titulo="+encodeURIComponent(dato1)+"&descripcion="+encodeURIComponent(dato2)+"&fecha="+encodeURIComponent(dato3);
	return data;
}
function obtenerdatosMPF(id){
	var data="";
	var dato1 = document.getElementById('ap-titulo').value;
	var dato2 = document.getElementById('ap-descripcion').value;
	var dato3 = document.getElementById('ap-fecha').value;
	data="titulo="+encodeURIComponent(dato1)+"&descripcion="+encodeURIComponent(dato2)+"&fecha="+encodeURIComponent(dato3)+"&id="+encodeURIComponent(id);
	return data;
}
function obtenerdatosAUFO(id){
	var data="";
	var dato1 = document.getElementById('u-titulo').value;
	var dato2 = document.getElementById('u-contenido').value;
	data="titulo="+encodeURIComponent(dato1)+"&contenido="+encodeURIComponent(dato2)+"&id_asig="+encodeURIComponent(id);
	return data;
}
function obtenerdatosAAAF(){
	var data="";
	var dato1 = document.getElementById('u-titulo').value;
	data="titulo="+encodeURIComponent(dato1);
	return data;
}
function obtenerdatosAACF(id){
	var data="";
	var dato1 = document.getElementById('ac-asignatura').value;
	var dato2 = document.getElementById('ac-grupo').value;
	var dato3 = document.getElementById('ac-aula').value;
	var dato4 = document.getElementById('ac-inicio').value;
	var dato5 = document.getElementById('ac-fin').value;
	data="asignatura="+encodeURIComponent(dato1)+"&grupo="+encodeURIComponent(dato2)+"&aula="+encodeURIComponent(dato3)+"&inicio="+encodeURIComponent(dato4)+"&fin="+encodeURIComponent(dato5)+"&id="+encodeURIComponent(id);
	return data;
}
function obtenerdatosAAGF(){
	var data="";
	var dato1 = document.getElementById('ag-grupo').value;
	var dato2 = document.getElementById('ag-carrera').value;
	data="grupo="+encodeURIComponent(dato1)+"&carrera="+encodeURIComponent(dato2);
	return data;
}
function obtenerdatosAF(){
	var data="";
	var dato1 = document.getElementById('aa-nombre').value;
	var dato2 = document.getElementById('aa-grupo').value;
	data="nombre="+encodeURIComponent(dato1)+"&grupo="+encodeURIComponent(dato2);
	return data;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


var conexion1;

function cargar(url){
	var datos = obtenerdatos();
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventos;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarCerrar(url){
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventos;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send();
}
function cargarMenu(url){
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosMenu;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send();
}
function cargarNav(url){
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send();
}
function cargarEliminar(url, id){
	var datos = "id_eliminar="+encodeURIComponent(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarEditar(url,id){
	var datos = "id_editar="+encodeURIComponent(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarCC(url, id){
	var datos = obtenerdatosCC(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAU(url){
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosAU;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send();
}
function cargarAUF(url){
	var datos = obtenerdatosAUF();
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarACF(url){
	var datos = obtenerdatosACF();
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarMCF(url,id){
	var datos = obtenerdatosMCF(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAVF(url){
	var datos = obtenerdatosAVF();
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarMVF(url,id){
	var datos = obtenerdatosMVF(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAAF(url){
	var datos = obtenerdatosAAF();
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarMAF(url,id){
	var datos = obtenerdatosMAF(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarConcluirA(url, id, califcacion){
	var datos = "id_eliminar="+encodeURIComponent(id)+"&califcacion="+encodeURIComponent(califcacion);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAPF(url){
	var datos = obtenerdatosAPF();
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarMPF(url,id){
	var datos = obtenerdatosMPF(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAUN(url,id){
	var datos = "id_asig="+encodeURIComponent(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAUFO(url,id){
	var datos = obtenerdatosAUFO(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAAAF(url){
	var datos = obtenerdatosAAAF();
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAACF(url,id){
	var datos = obtenerdatosAACF(id);
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAAGF(url){
	var datos = obtenerdatosAAGF();
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}
function cargarAF(url){
	var datos = obtenerdatosAF();
	conexion1 = new XMLHttpRequest();
	conexion1.onreadystatechange = procesarEventosNav;
	conexion1.open("POST", url, true);
	conexion1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	conexion1.send(datos);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function procesarEventos(){
	var detalles = document.getElementById("content");
	if(conexion1.readyState == 4){
		detalles.innerHTML = conexion1.responseText;
		inicializarEventos();
	}else {
		detalles.innerHTML = '<center><img src="./images/loading.gif" width="100"><br><p>Cargando...</p></center>';
	}
}
function procesarEventosMenu(){
	var detalles = document.getElementById("inner");
	if(conexion1.readyState == 4){
		detalles.innerHTML = conexion1.responseText;
		inicializarEventos();
	}else {
		detalles.innerHTML = '<center><img src="./images/loading.gif" width="100"><br><p>Cargando...</p></center>';
	}
}
function procesarEventosNav(){
	var detalles = document.getElementById("container");
	if(conexion1.readyState == 4){
		detalles.innerHTML = conexion1.responseText;
		inicializarEventos();
	}else {
		detalles.innerHTML = '<center><img src="./images/loading.gif" width="100"><br><p>Cargando...</p></center>';
	}
}
function procesarEventosAU(){
	var detalles = document.getElementById("container");
	if(conexion1.readyState == 4){
		detalles.innerHTML = conexion1.responseText;
		inicializarEventos();
	}else {
		detalles.innerHTML = '<center><img src="./images/loading.gif" width="100"><br><p>Cargando...</p></center>';
	}
}



//Encrytar Contraseña
function SHA1(msg) {
  function rotate_left(n,s) {
    var t4 = ( n<<s ) | (n>>>(32-s));
    return t4;
  };
  function lsb_hex(val) {
    var str="";
    var i;
    var vh;
    var vl;
    for( i=0; i<=6; i+=2 ) {
      vh = (val>>>(i*4+4))&0x0f;
      vl = (val>>>(i*4))&0x0f;
      str += vh.toString(16) + vl.toString(16);
    }
    return str;
  };
  function cvt_hex(val) {
    var str="";
    var i;
    var v;
    for( i=7; i>=0; i-- ) {
      v = (val>>>(i*4))&0x0f;
      str += v.toString(16);
    }
    return str;
  };
  function Utf8Encode(string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";
    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);
      if (c < 128) {
        utftext += String.fromCharCode(c);
      }
      else if((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      }
      else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }
    }
    return utftext;
  };
  var blockstart;
  var i, j;
  var W = new Array(80);
  var H0 = 0x67452301;
  var H1 = 0xEFCDAB89;
  var H2 = 0x98BADCFE;
  var H3 = 0x10325476;
  var H4 = 0xC3D2E1F0;
  var A, B, C, D, E;
  var temp;
  msg = Utf8Encode(msg);
  var msg_len = msg.length;
  var word_array = new Array();
  for( i=0; i<msg_len-3; i+=4 ) {
    j = msg.charCodeAt(i)<<24 | msg.charCodeAt(i+1)<<16 |
    msg.charCodeAt(i+2)<<8 | msg.charCodeAt(i+3);
    word_array.push( j );
  }
  switch( msg_len % 4 ) {
    case 0:
      i = 0x080000000;
    break;
    case 1:
      i = msg.charCodeAt(msg_len-1)<<24 | 0x0800000;
    break;
    case 2:
      i = msg.charCodeAt(msg_len-2)<<24 | msg.charCodeAt(msg_len-1)<<16 | 0x08000;
    break;
    case 3:
      i = msg.charCodeAt(msg_len-3)<<24 | msg.charCodeAt(msg_len-2)<<16 | msg.charCodeAt(msg_len-1)<<8  | 0x80;
    break;
  }
  word_array.push( i );
  while( (word_array.length % 16) != 14 ) word_array.push( 0 );
  word_array.push( msg_len>>>29 );
  word_array.push( (msg_len<<3)&0x0ffffffff );
  for ( blockstart=0; blockstart<word_array.length; blockstart+=16 ) {
    for( i=0; i<16; i++ ) W[i] = word_array[blockstart+i];
    for( i=16; i<=79; i++ ) W[i] = rotate_left(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);
    A = H0;
    B = H1;
    C = H2;
    D = H3;
    E = H4;
    for( i= 0; i<=19; i++ ) {
      temp = (rotate_left(A,5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B,30);
      B = A;
      A = temp;
    }
    for( i=20; i<=39; i++ ) {
      temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B,30);
      B = A;
      A = temp;
    }
    for( i=40; i<=59; i++ ) {
      temp = (rotate_left(A,5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B,30);
      B = A;
      A = temp;
    }
    for( i=60; i<=79; i++ ) {
      temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B,30);
      B = A;
      A = temp;
    }
    H0 = (H0 + A) & 0x0ffffffff;
    H1 = (H1 + B) & 0x0ffffffff;
    H2 = (H2 + C) & 0x0ffffffff;
    H3 = (H3 + D) & 0x0ffffffff;
    H4 = (H4 + E) & 0x0ffffffff;
  }
  var temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);

  return temp.toLowerCase();
}