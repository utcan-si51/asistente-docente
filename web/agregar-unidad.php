<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	
	$idAsig = $_REQUEST['id_asig'];
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Agregar una nueva unidad.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="agregar-unidad-form" value="<?php echo $idAsig;?>" action ="./validaciones/validar-agregar-unidad.php" method="post">
				<input class="form-control form-control-lg" type="text" id="u-titulo" placeholder="Titulo de la Unidad" required>
				<br>
				<textarea class="form-control" id="u-contenido" rows="3" placeholder="Contenido" required></textarea>
				<br>
				<center><button type="submit" class="btn btn-success">Agregar</button></center>
			</form>
		</div>
	</div>
</div>