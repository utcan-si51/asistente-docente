<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once('./config/Conexion.php');
	
	$conexion = new Conexion;
	
	$dia = $_REQUEST['id_asig'];
	$idU = $_SESSION['id_Usuario'];
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Agregar una Clase.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="agregar-clase-form" value="<?php echo $dia;?>" action ="./validaciones/validar-agregar-clase.php" method="post">
				<select class="custom-select custom-select-lg mb-3" id="ac-asignatura" title="Asignatura" required>
<?php
	$sqldias = "SELECT * FROM asignaturas WHERE IDProfesor = $idU AND Estatus != 1;";
	
	$resultado=array_filter($conexion->seleccionarValores($sqldias));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDAsignatura'];
		$nombre = $datos['NombreAsignatura'];
		echo <<<HTML
		<option value="$id">$nombre</option>
HTML;
	}
?>				
				</select>
				<br>
				<select class="custom-select custom-select-lg mb-3" id="ac-grupo" title="Grupo" required>
<?php
	$sqldias = "SELECT * FROM grupos WHERE IDProfesor = $idU AND Estatus != 1;";
	
	$resultado=array_filter($conexion->seleccionarValores($sqldias));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDGrupo'];
		$nombre = $datos['NombreGrupo'];
		echo <<<HTML
		<option value="$id">$nombre</option>
HTML;
	}
?>
				</select>
				<br>
				<select class="custom-select custom-select-lg mb-3" id="ac-aula" title="Aula" required>
<?php
	$sqldias = "SELECT * FROM aulas;";
	
	$resultado=array_filter($conexion->seleccionarValores($sqldias));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDAula'];
		$nombre = $datos['NombreAula'];
		echo <<<HTML
		<option value="$id">$nombre</option>
HTML;
	}
?>
				</select>				
				<br>
				<select class="custom-select custom-select-lg mb-3" id="ac-inicio" title="Hora Inicio" required>
<?php
	$sqldias = "SELECT * FROM horas;";
	
	$resultado=array_filter($conexion->seleccionarValores($sqldias));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDHora'];
		$nombre = $datos['Hora'];
		echo <<<HTML
		<option value="$id">$nombre</option>
HTML;
	}
?>
				</select>				
				<br>
				<select class="custom-select custom-select-lg mb-3" id="ac-fin" title="Hora Fin" required>
<?php
	$sqldias = "SELECT * FROM horas;";
	
	$resultado=array_filter($conexion->seleccionarValores($sqldias));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDHora'];
		$nombre = $datos['Hora'];
		if($id == 20){
		echo <<<HTML
		<option value="$id" selected>$nombre</option>
HTML;
		}else{
		echo <<<HTML
		<option value="$id">$nombre</option>
HTML;
		}
	}
?>
				</select>
				<br>
				<center><button type="submit" class="btn btn-success">Agregar</button></center>
			</form>
		</div>
	</div>
</div>