<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once('./config/Conexion.php');
	
	$conexion = new Conexion;	
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Agregar un Alumno.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="agregar-alumno-form" action ="./validaciones/validar-agregar-alumno.php" method="post">
				<input class="form-control form-control-lg" type="text" id="aa-nombre" placeholder="Nombre del Alumno" required>
				<br>
				<select class="custom-select custom-select-lg mb-3" id="aa-grupo" title="Carrera" required>
<?php
	$sqldias = "SELECT * FROM grupos WHERE Estatus != 1;";
	
	$resultado=array_filter($conexion->seleccionarValores($sqldias));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDGrupo'];
		$nombre = $datos['NombreGrupo'];
		echo <<<HTML
		<option value="$id">$nombre</option>
HTML;
	}
?>
				</select>
				<br>
				<center><button type="submit" class="btn btn-success">Agregar</button></center>
			</form>
		</div>
	</div>
</div>