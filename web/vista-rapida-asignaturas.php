<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$id = $_SESSION['id_Usuario'];
	$id_Asig = "";
	
	$cantArrayAsig=$conexion->traerValores("SELECT COUNT(IDAsignatura) AS cant FROM asignaturas WHERE Estatus != 1 AND IdProfesor= $id;");
	$cantAsig = $cantArrayAsig['cant'];
?>
<br>
<form id="agregar-usuario" action="./agregar-asignatura.php" method="post">
	<button type="submit" class="btn btn-success">Agregar Asignatura</button>
</form>
<input type="text" id="cantAsig" value="<?php echo $cantAsig;?>" hidden>
<?php
	
	$sqlAsig = "SELECT * FROM asignaturas WHERE Estatus != 1 AND IdProfesor = $id;";
	
	$resultado=array_filter($conexion->seleccionarValores($sqlAsig));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDAsignatura'];
		$nombre = $datos['NombreAsignatura'];
		$cantArray=$conexion->traerValores("SELECT COUNT(IDTemario) AS cant FROM temarios WHERE Estatus != 1 AND IDAsignatura= $id;");
		$cant = $cantArray['cant'];
	echo <<<HTML
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">
		<form id="eliminarasig$i" value="$id" action="./validaciones/validar-eliminar-asignatura.php" method="post">
			<button type="submit" title="Eliminar Asignatura" class="btn btn-danger btn-sm"><img src="./images/delete.png"/ width="15"></button> $nombre <span class="badge badge-pill bg-light align-text-bottom" id="cant">$cant</span>
		</form>
	</h6>
HTML;
		$resultado=array_filter($conexion->seleccionarValores("SELECT * FROM temarios WHERE Estatus != 1 AND IDAsignatura = $id;"));
		$j = 1;
		foreach($resultado as $datos){
		$id2 = $datos['IDTemario'];
		$nombre = $datos['Unidad'];
		$descripcion = $datos['Contenido'];
	echo <<<HTML
		<div class="media text-muted pt-3">
			<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
				<strong class="text-gray-dark">$nombre</strong><br><br>
				$descripcion
			</p>
			<div class="botones-listado">
				<form id="eliminar$j" value="$id2" action="./validaciones/validar-eliminar-unidad.php" method="post">
					<center><button type="submit" class="btn btn-danger btn-sm">Eliminar</button></center>
				</form>
				<form id="editar$j" value="$id2" action="./modificar-unidad.php" method="post">
					<center><button type="submit" class="btn btn-warning btn-sm">Modificar</button></center>
				</form>
			</div>
		</div>
HTML;
		$j++;
		}
	echo <<<HTML
	<small class="d-block text-right mt-3">
	<form id="agregar-usuario$i" value = "$id" action="./agregar-unidad.php" method="post">
		<button type="submit" class="btn btn-success btn-sm">Agregar Unidad</button>
	</form>
</small>
</div>
HTML;
	$i++;
	}
?>	