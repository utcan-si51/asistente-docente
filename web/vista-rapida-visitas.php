<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT'].'/asdoc/config/Conexion.php');
	
	$conexion = new Conexion;
	
	$id = $_SESSION['id_Usuario'];
	
	$sqlcant = "SELECT COUNT(IDVisita) AS cant FROM visitas WHERE Estatus != 1 AND IdProfesor= $id;";
	$sql = "SELECT visitas.IDVisita, visitas.NombreVisita, visitas.DesripcionVisita, visitas.Fecha, grupos.NombreGrupo AS Grupo FROM visitas JOIN grupos ON visitas.Grupo = grupos.IDGrupo WHERE visitas.Estatus != 1 AND visitas.Estatus != 2 AND visitas.IdProfesor = $id ORDER BY visitas.Fecha;";
	
	$cant=$conexion->traerValores($sqlcant);
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Visitas <span class="badge badge-pill bg-light align-text-bottom" id="cant"><?php echo $cant['cant'];?></span></h6>
<?php
	$resultado=array_filter($conexion->seleccionarValores($sql));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDVisita'];
		$nombre = $datos['NombreVisita'];
		$descripcion = $datos['DesripcionVisita'];
		$fecha = date( 'd-m-Y H:i', strtotime($datos['Fecha']));
		$grupo = $datos['Grupo'];
	echo <<<HTML
		<div class="media text-muted pt-3">
			<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
				<strong class="text-gray-dark">Fecha y Hora:</strong> $fecha <strong class="text-gray-dark">- Nombre:</strong> $nombre <strong class="text-gray-dark">- Grupo:</strong> $grupo<br><br>
				$descripcion
			</p>
			<div class="botones-listado">
				<form id="eliminar$i" value="$id" action="./validaciones/validar-concluir-visita.php" method="post">
					<center><button type="submit" class="btn btn-info btn-sm">Concluir</button></center>
				</form>
				<form id="editar$i" value="$id" action="./modificar-visita.php" method="post">
					<center><button type="submit" class="btn btn-warning btn-sm">Modificar</button></center>
				</form>
			</div>
		</div>
HTML;
	$i++;
	}
?>	
<small class="d-block text-right mt-3">
	<form id="agregar-usuario" action="./agregar-visita.php" method="post">
		<button type="submit" class="btn btn-success">Agregar</button>
	</form>
</small>
</div>