<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once('./config/Conexion.php');
	
	$conexion= new Conexion;	
	
	$id = $_SESSION['id_Usuario'];
	$idVisita = mysqli_real_escape_string($conexion->conexion, $_REQUEST['id_editar']);
	
	$sql = "SELECT * FROM visitas WHERE IDVisita = $idVisita;";
	$sql2 = "SELECT * FROM grupos WHERE IDProfesor = $id;";
	
	$resultado=$conexion->traerValores($sql);
	
	$nombre=$resultado['NombreVisita'];
	$descripcion=$resultado['DesripcionVisita'];
	$fecha = date( 'Y-m-d', strtotime($resultado['Fecha']));
	$hora = date( 'h:m', strtotime($resultado['Fecha']));
	$grupo=$resultado['Grupo'];
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Modificar esta Visita.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="modificar-visita-form" value="<?php echo $idVisita;?>" action ="./validaciones/validar-modificar-visita.php" method="post">
				<input class="form-control form-control-lg" type="text" id="av-nombre" value="<?php echo $nombre;?>" required>
				<br>			
				<input class="form-control form-control-lg" type="text" id="av-descripcion" value="<?php echo $descripcion;?>" required>
				<br>
				<input class="form-control form-control-lg" type="datetime-local" id="av-fecha" value="<?php echo $fecha.'T'.$hora;?>" required>
				<br>
				<select class="custom-select custom-select-lg mb-3" id="av-grupo">
<?php
					$resultado=array_filter($conexion->seleccionarValores($sql2));
					$i = 1;
					foreach($resultado as $datos){
						$idGrupo = $datos['IDGrupo'];
						$NombreGrupo = $datos['NombreGrupo'];
						if($idGrupo == $grupo){
							echo '<option value="'.$idGrupo.'" selected>'.$NombreGrupo.'</option>';
						}else{
							echo '<option value="'.$idGrupo.'">'.$NombreGrupo.'</option>';
						}
					}
?>
				</select>
				<br>
				<center><button type="submit" class="btn btn-success">Guardar</button></center>
			</form>
		</div>
	</div>
</div>