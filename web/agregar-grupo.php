<?php
	if(session_status() == PHP_SESSION_NONE){
		session_start();
	}
	require_once('./config/Conexion.php');
	
	$conexion = new Conexion;	
?>
<div class="my-3 p-3 bg-white rounded box-shadow">
	<h6 class="border-bottom border-gray pb-2 mb-0">Agregar un Grupo.</h6>
	<div class="media text-muted pt-3">
		<div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
			<form id="agregar-grupo-form" action ="./validaciones/validar-agregar-grupo.php" method="post">
				<input class="form-control form-control-lg" type="text" id="ag-grupo" placeholder="Nombre del Grupo" required>
				<br>
				<select class="custom-select custom-select-lg mb-3" id="ag-carrera" title="Carrera" required>
<?php
	$sqldias = "SELECT * FROM carreras";
	
	$resultado=array_filter($conexion->seleccionarValores($sqldias));
	$i = 1;
	foreach($resultado as $datos){
		$id = $datos['IDCarrera'];
		$nombre = $datos['NombreCarrera'];
		echo <<<HTML
		<option value="$id">$nombre</option>
HTML;
	}
?>
				</select>
				<br>
				<center><button type="submit" class="btn btn-success">Agregar</button></center>
			</form>
		</div>
	</div>
</div>