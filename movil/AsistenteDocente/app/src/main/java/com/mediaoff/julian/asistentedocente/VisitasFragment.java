package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ismael on 02-Apr-2018.
 */

public class VisitasFragment extends Fragment {
    View rootView = null; //Variable de vista maestrs
    LinearLayout lista = null; //Variable de vista con estilo LinearLayout
    SwipeRefreshLayout swipeLayout;

    //Función que recupera el ID del profesor
    public int ObtieneIDUsuario() {
        int idProfesor = getActivity().getIntent().getIntExtra("id", 0);
        return idProfesor;
    }

    //Función que obtiene la fecha y hora de conclusión de actividades
    public String ObtieneFechaHoraCierre() {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        int hora = today.hour;
        int minuto = today.minute;
        int dia = today.monthDay;
        int mes = today.month;
        int anio = today.year;
        mes = mes + 1;
        String fechaHora = anio + "/" + mes + "/" + dia + " " + hora + ":" + minuto + ":00";

        return fechaHora;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_visitas, container, false);
        lista = rootView.findViewById(R.id.visitasLista);
        //Ejecución de la consulta de Agenda
        TraerVisitas intentoTraer = new TraerVisitas();
        intentoTraer.execute(String.valueOf(ObtieneIDUsuario()));

        swipeLayout = rootView.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                lista.removeAllViews();

                TraerVisitas intentoTraer = new TraerVisitas();
                intentoTraer.execute(String.valueOf(ObtieneIDUsuario()));
                swipeLayout.setRefreshing(false);
            }
        });

        final FloatingActionButton fab = rootView.findViewById(R.id.fabtoolbar_fab);
        MainActivity.morph = rootView.findViewById(R.id.fabtoolbar);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fab.getId() == R.id.fabtoolbar_fab) {
                    MainActivity.morph.show();
                }

                MainActivity.morph.hide();
            }
        });

        RelativeLayout btnAgregar = rootView.findViewById(R.id.layoutAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.morph.hide();

                int PICK_CONTACT_REQUEST = 1;

                Intent pickContactIntent = new Intent(getActivity(), VisitasAgregarFragment.class);
                pickContactIntent.putExtra("id", ObtieneIDUsuario());
                startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
            }
        });

        return rootView;
    }

    //Función que se ejecuta cuando la vista es creada en ejecución
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    //Recarga la pantalla de inicio con los datos actualzados
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        lista.removeAllViews();

        TraerVisitas intentoTraer = new TraerVisitas();
        intentoTraer.execute(String.valueOf(ObtieneIDUsuario()));
    }

    private class TraerVisitas extends AsyncTask<String, String, JSONObject> {
        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=15";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        //Funcion que ejecuta los parámetros dentro del WebService
        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> parametros = new HashMap<>();
                parametros.put("id", strings[0]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", parametros);
                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        //Si la aplicación está esperando información del webservice muestra un mensaje de espera
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getActivity());
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        //Ejecución de recogida y despliegue de datos de la agenda
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<>();

            if (jsonObject != null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("visitas"); //Revisar el nombre: agenda o pendientes
                    int size = jsonArray.length();

                    if (size == 0) {
                        TextView vacio = new TextView(getActivity());
                        vacio.setText("¡No tiene ninguna visita registrada!");
                        vacio.setGravity(Gravity.CENTER_HORIZONTAL);

                        lista.addView(vacio);
                    } else {

                        for (int i = 0; i < size; i++) {
                            JSONObject another_json_object = jsonArray.getJSONObject(i);
                            arrays.add(another_json_object);
                        }

                        JSONObject[] ArrJson = new JSONObject[arrays.size()];
                        arrays.toArray(ArrJson);

                        //Muestra la información recuperada de la agenda en pantalla
                        for (final JSONObject valores : arrays) {
                            LinearLayout visitas = (LinearLayout) getLayoutInflater().inflate(R.layout.item_visitas, null);

                            TextView Fecha = visitas.findViewById(R.id.lFechaHora);
                            Fecha.setText("Fecha y hora: " + valores.getString("fechainicio")); //Modificar valores con respecto al WS
                            TextView Nombre = visitas.findViewById(R.id.lNombre);
                            Nombre.setText("Titulo: " + valores.getString("nombre")); //Modificar valores con respecto al WS
                            TextView Grupo = visitas.findViewById(R.id.lGrupo);
                            Grupo.setText("Grupo: " + valores.getString("grupo")); //Modificar valores con respecto al WS
                            TextView Contenido = visitas.findViewById(R.id.lContenidoVis);
                            Contenido.setText("Descripcion: " + valores.getString("descripcion"));
                            //Añade funcionalidad al botón de Finalizar
                            LinearLayout btnFin = visitas.findViewById(R.id.layoutConcluir);
                            btnFin.setOnClickListener(new View.OnClickListener() {
                                //Muestra un mensaje de alerta en pantalla
                                @Override
                                public void onClick(View view) {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setTitle("¿Esta seguro?");
                                    alertDialogBuilder.setMessage("¿Esta seguro que quiere eliminar este registro de visita?");
                                    alertDialogBuilder.setCancelable(false);

                                    //Opción verdadera, finalza la actividad de la agenda
                                    alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            concluirVisita intentoConcluir = new concluirVisita();
                                            try {
                                                intentoConcluir.execute(valores.getString("id"), ObtieneFechaHoraCierre());
                                                TraerVisitas intentoTraer = new TraerVisitas();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            lista.removeAllViews();

                                            TraerVisitas intentoTraer = new TraerVisitas();
                                            intentoTraer.execute(String.valueOf(ObtieneIDUsuario()));
                                        }
                                    });
                                    //Opcion falsa: No realiza ninguna acción y continpua el tiempo de ejecución
                                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();
                                }
                            });
                            //Añade funcionalidad al botón de modificar
                            LinearLayout btnEdit = visitas.findViewById(R.id.layoutMod);
                            btnEdit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    int PICK_CONTACT_REQUEST = 1;

                                    Intent pickContactIntent = new Intent(getActivity(), VisitasModificarFragment.class);
                                    try {
                                        pickContactIntent.putExtra("id", valores.getInt("id"));
                                        pickContactIntent.putExtra("idProfesor", ObtieneIDUsuario());
                                        pickContactIntent.putExtra("nombre", valores.getString("nombre"));
                                        pickContactIntent.putExtra("descripcion", valores.getString("descripcion"));
                                        pickContactIntent.putExtra("fecha", valores.getString("fechainicio"));
                                        pickContactIntent.putExtra("grupo", valores.getString("grupo"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
                                }
                            });
                            lista.addView(visitas);
                            //Añade elementos gráficos a la pantalla
                            LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                            paramsSep.height = 4;
                            paramsSep.setMargins(0, 0, 0, 20);

                            View separador = new View(getActivity());
                            separador.setLayoutParams(paramsSep);
                            separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                            lista.addView(separador);

                            SwipeLayout swipeNavLayout = visitas.findViewById(R.id.sample1);
                            swipeNavLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    //Funcion que permite dar de baja información de las Visitas
    private class concluirVisita extends AsyncTask<String, String, JSONObject> {
        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=13";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                params.put("fechafin", strings[1]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getActivity());
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }
}