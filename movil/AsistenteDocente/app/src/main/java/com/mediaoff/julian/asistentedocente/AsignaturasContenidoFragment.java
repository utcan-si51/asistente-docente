package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Julian on 21-Feb-18.
 */

public class AsignaturasContenidoFragment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_asignaturas_contenido);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String idAsig = getIntent().getStringExtra("id");

        TraerUnidades intentoTraer = new TraerUnidades(AsignaturasContenidoFragment.this);
        intentoTraer.execute(idAsig);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class TraerUnidades extends AsyncTask<String, Context, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=41";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        private final Context mContext;
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        public TraerUnidades(final Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(AsignaturasContenidoFragment.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("unidades");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    for (final JSONObject valores : arrays) {
                        TextView titulo = findViewById(R.id.titulo2);
                        titulo.setText(valores.getString("nombreAsignatura") + " - " + valores.getString("unidad"));

                        TextView contenido = findViewById(R.id.tVisita);
                        contenido.setText(valores.getString("contenido"));

                        Button btnEliminar = findViewById(R.id.btnEliminar);
                        btnEliminar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                                alertDialogBuilder.setTitle("¿Esta seguro?");
                                alertDialogBuilder.setMessage("¿Esta seguro que quiere eliminar a esta unidad?");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        EliminarUnidad intenoEliminar = new EliminarUnidad();
                                        try {
                                            intenoEliminar.execute(valores.getString("id"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class EliminarUnidad extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=43";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(AsignaturasContenidoFragment.this);
            this.pDialog.setMessage("Eliminando Unidad ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(AsignaturasContenidoFragment.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
                finish();
            } else if (success == 2) {
                Log.d("Exists", message);
            } else {
                Log.d("Error", message);
            }
        }
    }
}
