package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Julian on 21-Feb-18.
 */

public class ReportesFragment extends Fragment {

    View rootView;
    SwipeRefreshLayout swipeLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reportes, container, false);

        swipeLayout = rootView.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

            }
        });


        LinearLayout b = rootView.findViewById(R.id.layoutContenido);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GenerarProfes intentoTraer = new GenerarProfes();
                intentoTraer.execute();
            }
        });

        return rootView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private class GenerarProfes extends AsyncTask<Integer, Integer, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=50";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "url";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getActivity());
            this.pDialog.setMessage("Generando Reporte ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Integer... integers) {
            try {
                HashMap<String, String> params = new HashMap<>();
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String url = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    url = jsonObject.getString(TAG_MESSAGE);

                    Intent generar = new Intent(getActivity(), ReporteContenidoFragment.class);
                    generar.putExtra("url",url);
                    startActivity(generar);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
