package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Julian on 21-Feb-18.
 */

public class AsignaturaFragmentUnidadAgregar extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_asignaturas_contenido_agregar);

        final String id = getIntent().getStringExtra("id");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Button btnAgregar = findViewById(R.id.btnAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView txtnombre = findViewById(R.id.nombreUnidad);
                String nombre = txtnombre.getText().toString();

                TextView txtcontenido = findViewById(R.id.contenidoUnidad);
                String contenido = txtcontenido.getText().toString();

                if (nombre.equals("") || contenido.equals("")) {
                    Toast.makeText(AsignaturaFragmentUnidadAgregar.this, "Los datos ingresados no son validos", Toast.LENGTH_LONG).show();
                } else {
                    AgregarUnidad intentoAgregar = new AgregarUnidad();
                    intentoAgregar.execute(id, nombre, contenido);
                }

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private class AgregarUnidad extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=42";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(AsignaturaFragmentUnidadAgregar.this);
            this.pDialog.setMessage("Agregando Unidad ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                params.put("nombre", strings[1]);
                params.put("contenido", strings[2]);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(AsignaturaFragmentUnidadAgregar.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
                finish();

            } else if (success == 2) {
                Log.d("Exists", message);
            } else {
                Log.d("Error", message);
            }
        }
    }
}
