package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Julian on 21-Feb-18.
 */


public class AlumnosAgregarFragment extends AppCompatActivity {

    Spinner spGrupo;
    HashMap<Integer, String> spMapGrupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_alumnos_agregar);

        spGrupo = findViewById(R.id.spGrupo);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TraerGrupos intentoTraer = new TraerGrupos();
        intentoTraer.execute();

        Button agregar = findViewById(R.id.btnAgregar);

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText txtNombre = findViewById(R.id.tVisita);
                Spinner spGrupo = findViewById(R.id.spGrupo);

                String nombre = txtNombre.getText().toString();
                String grupo = spMapGrupo.get(spGrupo.getSelectedItemPosition());

                if (nombre.equals("")) {
                    Toast.makeText(AlumnosAgregarFragment.this, "Los datos no son validos!", Toast.LENGTH_SHORT).show();
                } else {
                    AgregarAlumno intentoAgregar = new AgregarAlumno();
                    intentoAgregar.execute(nombre, grupo);
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class TraerGrupos extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=8";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(AlumnosAgregarFragment.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("grupos");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    String[] spinnerArray = new String[arrays.size()];
                    spMapGrupo = new HashMap<Integer, String>();

                    int i = 0;

                    for (final JSONObject valores : arrays) {
                        spMapGrupo.put(i, valores.getString("id"));
                        spinnerArray[i] = valores.getString("NombreGrupo");
                        i++;
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AlumnosAgregarFragment.this, android.R.layout.simple_spinner_item, spinnerArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spGrupo.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class AgregarAlumno extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=9";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(AlumnosAgregarFragment.this);
            this.pDialog.setMessage("Agregando Alumno ...");
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setIndeterminate(false);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("nombre", strings[0]);
                params.put("grupo", strings[1]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(AlumnosAgregarFragment.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
                finish();

            } else if (success == 2) {
                Log.d("Exists", message);
            } else {
                Log.d("Error", message);
            }
        }
    }
}
