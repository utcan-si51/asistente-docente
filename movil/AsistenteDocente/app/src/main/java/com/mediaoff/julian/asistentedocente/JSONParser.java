package com.mediaoff.julian.asistentedocente;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Julian on 3/14/18.
 */

public class JSONParser {

    String char_cod = "UTF-8";
    HttpsURLConnection connection;
    DataOutputStream dos;
    StringBuilder result, parametros;
    URL urlObj;
    JSONObject jObj = null;
    String paramsString;

    public JSONObject makeHttpRequest(String url, String method, HashMap<String, String> params) {

        this.parametros = new StringBuilder();

        int i = 0;
        for (String key : params.keySet()) {
            try {
                if (i != 0) {
                    this.parametros.append("&");
                }
                this.parametros.append(key).append("=").append(URLEncoder.encode(params.get(key), char_cod));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;
        }

        if (method.equals("POST")) {
            try {
                this.urlObj = new URL(url);
                this.connection = (HttpsURLConnection) this.urlObj.openConnection();
                this.connection.setDoOutput(true);
                this.connection.setRequestMethod("POST");
                this.connection.setRequestProperty("Accept-char_cod", this.char_cod);
                this.connection.setReadTimeout(10000);
                this.connection.setConnectTimeout(15000);
                this.connection.connect();
                this.paramsString = this.parametros.toString();
                this.dos = new DataOutputStream(this.connection.getOutputStream());
                this.dos.writeBytes(this.paramsString);
                this.dos.flush();
                this.dos.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (method.equals("GET")) {
            if (this.parametros.length() != 0) {
                url += "&" + this.parametros.toString();
            }
            try {
                Log.d("URL", url);
                this.urlObj = new URL(url);
                this.connection = (HttpsURLConnection) this.urlObj.openConnection();
                this.connection.setDoOutput(false);
                this.connection.setRequestMethod("GET");
                this.connection.setRequestProperty("Accept-char_cod", this.char_cod);
                this.connection.setConnectTimeout(15000);
                this.connection.connect();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            InputStream in = new BufferedInputStream(this.connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            this.result = new StringBuilder();
            String line = "";
            while ((line = reader.readLine()) != null) {
                this.result.append(line);
                Log.d("JSON es: ", "result: " + this.result.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            this.jObj = new JSONObject(this.result.toString());

        } catch (JSONException e) {
            Log.e("JSON error:", "Error decodificando datos." + e.toString());
            e.printStackTrace();
        }

        return this.jObj;
    }

}
