package com.mediaoff.julian.asistentedocente;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daimajia.swipe.SwipeLayout;
import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Julian on 21-Feb-18.
 */

public class HorarioFragment extends Fragment {

    View rootView = null;
    LinearLayout listaLunes, listaMartes, listaMiercoles, listaJueves, listaViernes;
    String id;
    SwipeRefreshLayout swipeLayout;
    FABToolbarLayout morph;
    private int success = 0;
    private String message = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_horario, container, false);
        swipeLayout = rootView.findViewById(R.id.swipe_container);

        listaLunes = rootView.findViewById(R.id.horarioListaLunes);
        listaMartes = rootView.findViewById(R.id.horarioListaMartes);
        listaMiercoles = rootView.findViewById(R.id.horarioListaMiercoles);
        listaJueves = rootView.findViewById(R.id.horarioListaJueves);
        listaViernes = rootView.findViewById(R.id.horarioListaViernes);

        id = String.valueOf(getActivity().getIntent().getIntExtra("id", 0));

        TraerHorario(id);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listaLunes.removeAllViews();
                listaMartes.removeAllViews();
                listaMiercoles.removeAllViews();
                listaJueves.removeAllViews();
                listaViernes.removeAllViews();

                TraerHorario(id);
                swipeLayout.setRefreshing(false);
            }
        });


        final FloatingActionButton fab = rootView.findViewById(R.id.fabtoolbar_fab);
        morph = rootView.findViewById(R.id.fabtoolbar);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fab.getId() == R.id.fabtoolbar_fab) {
                    morph.show();
                }

                morph.hide();
            }
        });

        RelativeLayout btnAgregar = rootView.findViewById(R.id.layoutAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                morph.hide();

                int PICK_CONTACT_REQUEST = 1;

                Intent pickContactIntent = new Intent(getActivity(), HorarioFragmentAgregar.class);
                pickContactIntent.putExtra("id", id);
                startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
            }
        });

        return rootView;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        listaLunes.removeAllViews();
        listaMartes.removeAllViews();
        listaMiercoles.removeAllViews();
        listaJueves.removeAllViews();
        listaViernes.removeAllViews();

        TraerHorario(id);
    }

    private void TraerHorario(final String id){
        swipeLayout.setRefreshing(true);
        AndroidNetworking.post("https://jpapke.tk/webservice/?opcion=28")
                .addBodyParameter("id", id)
                .setPriority(Priority.LOW)
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d("Analytics", " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d("Analytics", " bytesSent : " + bytesSent);
                        Log.d("Analytics", " bytesReceived : " + bytesReceived);
                        Log.d("Analytics", " isFromCache : " + isFromCache);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

                        if (response != null) {
                            try {

                                JSONArray jsonArray = response.getJSONArray("horario");
                                int size = jsonArray.length();

                                for (int i = 0; i < size; i++) {
                                    JSONObject another_json_object = jsonArray.getJSONObject(i);
                                    arrays.add(another_json_object);
                                }

                                JSONObject[] jsons = new JSONObject[arrays.size()];
                                arrays.toArray(jsons);

                                for (final JSONObject valores : arrays) {

                                    LinearLayout registro = null;

                                    if (valores.getInt("dia") == 1) {

                                        registro = (LinearLayout) getLayoutInflater().inflate(R.layout.item_horario, null);

                                        TextView titulo = registro.findViewById(R.id.horarioHoraGrupo);
                                        titulo.setText(valores.getString("horainicio") + " - " + valores.getString("horafin") + " - Grupo " + valores.getString("grupo"));

                                        TextView materia = registro.findViewById(R.id.materia);
                                        materia.setText(valores.getString("asignatura"));

                                        TextView aula = registro.findViewById(R.id.aula);
                                        aula.setText("Aula: " + valores.getString("aula"));

                                        listaLunes.addView(registro);

                                        LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                                        paramsSep.height = 4;
                                        paramsSep.setMargins(0, 0, 0, 20);

                                        View separador = new View(getContext());
                                        separador.setLayoutParams(paramsSep);
                                        separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                                        LinearLayout layoutEliminar = registro.findViewById(R.id.layoutEliminar);
                                        layoutEliminar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.myDialog));
                                                alertDialogBuilder.setTitle("¿Esta seguro?");
                                                alertDialogBuilder.setMessage("¿Esta seguro que quiere eliminar esta hora?");
                                                alertDialogBuilder.setCancelable(false);

                                                alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface arg0, int arg1) {
                                                        try {
                                                            BajaHora(valores.getInt("id"));
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                        listaLunes.removeAllViews();
                                                        listaMartes.removeAllViews();
                                                        listaMiercoles.removeAllViews();
                                                        listaJueves.removeAllViews();
                                                        listaViernes.removeAllViews();

                                                        TraerHorario(id);
                                                    }
                                                });

                                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                });

                                                final AlertDialog alertDialog = alertDialogBuilder.create();

                                                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                                    @Override
                                                    public void onShow(DialogInterface arg0) {
                                                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                    }
                                                });

                                                alertDialog.show();
                                            }
                                        });

                                        LinearLayout layoutEditar = registro.findViewById(R.id.layoutEditar);
                                        layoutEditar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                int PICK_CONTACT_REQUEST = 1;

                                                Intent pickContactIntent = new Intent(getContext(), HorarioFragmentModificar.class);
                                                try {
                                                    pickContactIntent.putExtra("id", id);
                                                    pickContactIntent.putExtra("idHora", valores.getInt("id"));
                                                    startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });


                                        listaLunes.addView(separador);

                                    } else if (valores.getInt("dia") == 2) {
                                        registro = (LinearLayout) getLayoutInflater().inflate(R.layout.item_horario, null);

                                        TextView titulo = registro.findViewById(R.id.horarioHoraGrupo);
                                        titulo.setText(valores.getString("horainicio") + " - " + valores.getString("horafin") + " - Grupo " + valores.getString("grupo"));

                                        TextView materia = registro.findViewById(R.id.materia);
                                        materia.setText(valores.getString("asignatura"));

                                        TextView aula = registro.findViewById(R.id.aula);
                                        aula.setText("Aula: " + valores.getString("aula"));

                                        listaMartes.addView(registro);

                                        LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                                        paramsSep.height = 4;
                                        paramsSep.setMargins(0, 0, 0, 20);

                                        View separador = new View(getContext());
                                        separador.setLayoutParams(paramsSep);
                                        separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                                        LinearLayout layoutEliminar = registro.findViewById(R.id.layoutEliminar);
                                        layoutEliminar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.myDialog));
                                                alertDialogBuilder.setTitle("¿Esta seguro?");
                                                alertDialogBuilder.setMessage("¿Esta seguro que quiere eliminar esta hora?");
                                                alertDialogBuilder.setCancelable(false);

                                                alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface arg0, int arg1) {
                                                        try {
                                                            BajaHora(valores.getInt("id"));
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                        listaLunes.removeAllViews();
                                                        listaMartes.removeAllViews();
                                                        listaMiercoles.removeAllViews();
                                                        listaJueves.removeAllViews();
                                                        listaViernes.removeAllViews();

                                                        TraerHorario(id);
                                                    }
                                                });

                                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                });

                                                final AlertDialog alertDialog = alertDialogBuilder.create();

                                                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                                    @Override
                                                    public void onShow(DialogInterface arg0) {
                                                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                    }
                                                });

                                                alertDialog.show();
                                            }
                                        });

                                        LinearLayout layoutEditar = registro.findViewById(R.id.layoutEditar);
                                        layoutEditar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                int PICK_CONTACT_REQUEST = 1;

                                                Intent pickContactIntent = new Intent(getContext(), HorarioFragmentModificar.class);
                                                try {
                                                    pickContactIntent.putExtra("id", id);
                                                    pickContactIntent.putExtra("idHora", valores.getInt("id"));
                                                    startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        listaMartes.addView(separador);

                                    } else if (valores.getInt("dia") == 3) {

                                        registro = (LinearLayout) getLayoutInflater().inflate(R.layout.item_horario, null);

                                        TextView titulo = registro.findViewById(R.id.horarioHoraGrupo);
                                        titulo.setText(valores.getString("horainicio") + " - " + valores.getString("horafin") + " - Grupo " + valores.getString("grupo"));

                                        TextView materia = registro.findViewById(R.id.materia);
                                        materia.setText(valores.getString("asignatura"));

                                        TextView aula = registro.findViewById(R.id.aula);
                                        aula.setText("Aula: " + valores.getString("aula"));

                                        listaMiercoles.addView(registro);

                                        LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                                        paramsSep.height = 4;
                                        paramsSep.setMargins(0, 0, 0, 20);

                                        View separador = new View(getContext());
                                        separador.setLayoutParams(paramsSep);
                                        separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                                        LinearLayout layoutEliminar = registro.findViewById(R.id.layoutEliminar);
                                        layoutEliminar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.myDialog));
                                                alertDialogBuilder.setTitle("¿Esta seguro?");
                                                alertDialogBuilder.setMessage("¿Esta seguro que quiere eliminar esta hora?");
                                                alertDialogBuilder.setCancelable(false);

                                                alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface arg0, int arg1) {
                                                        try {
                                                            BajaHora(valores.getInt("id"));
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                        listaLunes.removeAllViews();
                                                        listaMartes.removeAllViews();
                                                        listaMiercoles.removeAllViews();
                                                        listaJueves.removeAllViews();
                                                        listaViernes.removeAllViews();

                                                        TraerHorario(id);
                                                    }
                                                });

                                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                });

                                                final AlertDialog alertDialog = alertDialogBuilder.create();

                                                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                                    @Override
                                                    public void onShow(DialogInterface arg0) {
                                                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                    }
                                                });

                                                alertDialog.show();
                                            }
                                        });

                                        LinearLayout layoutEditar = registro.findViewById(R.id.layoutEditar);
                                        layoutEditar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                int PICK_CONTACT_REQUEST = 1;

                                                Intent pickContactIntent = new Intent(getContext(), HorarioFragmentModificar.class);
                                                try {
                                                    pickContactIntent.putExtra("id", id);
                                                    pickContactIntent.putExtra("idHora", valores.getInt("id"));
                                                    startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        listaMiercoles.addView(separador);

                                    } else if (valores.getInt("dia") == 4) {

                                        registro = (LinearLayout) getLayoutInflater().inflate(R.layout.item_horario, null);

                                        TextView titulo = registro.findViewById(R.id.horarioHoraGrupo);
                                        titulo.setText(valores.getString("horainicio") + " - " + valores.getString("horafin") + " - Grupo " + valores.getString("grupo"));

                                        TextView materia = registro.findViewById(R.id.materia);
                                        materia.setText(valores.getString("asignatura"));

                                        TextView aula = registro.findViewById(R.id.aula);
                                        aula.setText("Aula: " + valores.getString("aula"));

                                        listaJueves.addView(registro);

                                        LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                                        paramsSep.height = 4;
                                        paramsSep.setMargins(0, 0, 0, 20);

                                        View separador = new View(getContext());
                                        separador.setLayoutParams(paramsSep);
                                        separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                                        LinearLayout layoutEliminar = registro.findViewById(R.id.layoutEliminar);
                                        layoutEliminar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.myDialog));
                                                alertDialogBuilder.setTitle("¿Esta seguro?");
                                                alertDialogBuilder.setMessage("¿Esta seguro que quiere eliminar esta hora?");
                                                alertDialogBuilder.setCancelable(false);

                                                alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface arg0, int arg1) {
                                                        try {
                                                            BajaHora(valores.getInt("id"));
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                        listaLunes.removeAllViews();
                                                        listaMartes.removeAllViews();
                                                        listaMiercoles.removeAllViews();
                                                        listaJueves.removeAllViews();
                                                        listaViernes.removeAllViews();

                                                        TraerHorario(id);
                                                    }
                                                });

                                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                });

                                                final AlertDialog alertDialog = alertDialogBuilder.create();

                                                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                                    @Override
                                                    public void onShow(DialogInterface arg0) {
                                                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                    }
                                                });

                                                alertDialog.show();
                                            }
                                        });

                                        LinearLayout layoutEditar = registro.findViewById(R.id.layoutEditar);
                                        layoutEditar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                int PICK_CONTACT_REQUEST = 1;

                                                Intent pickContactIntent = new Intent(getContext(), HorarioFragmentModificar.class);
                                                try {
                                                    pickContactIntent.putExtra("id", id);
                                                    pickContactIntent.putExtra("idHora", valores.getInt("id"));
                                                    startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        listaJueves.addView(separador);

                                    } else if (valores.getInt("dia") == 5) {

                                        registro = (LinearLayout) getLayoutInflater().inflate(R.layout.item_horario, null);

                                        TextView titulo = registro.findViewById(R.id.horarioHoraGrupo);
                                        titulo.setText(valores.getString("horainicio") + " - " + valores.getString("horafin") + " - Grupo " + valores.getString("grupo"));

                                        TextView materia = registro.findViewById(R.id.materia);
                                        materia.setText(valores.getString("asignatura"));

                                        TextView aula = registro.findViewById(R.id.aula);
                                        aula.setText("Aula: " + valores.getString("aula"));

                                        listaViernes.addView(registro);

                                        LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                                        paramsSep.height = 4;
                                        paramsSep.setMargins(0, 0, 0, 20);

                                        View separador = new View(getContext());
                                        separador.setLayoutParams(paramsSep);
                                        separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                                        LinearLayout layoutEliminar = registro.findViewById(R.id.layoutEliminar);
                                        layoutEliminar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.myDialog));
                                                alertDialogBuilder.setTitle("¿Esta seguro?");
                                                alertDialogBuilder.setMessage("¿Esta seguro que quiere eliminar esta hora?");
                                                alertDialogBuilder.setCancelable(false);

                                                alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface arg0, int arg1) {
                                                        try {
                                                            BajaHora(valores.getInt("id"));
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                        listaLunes.removeAllViews();
                                                        listaMartes.removeAllViews();
                                                        listaMiercoles.removeAllViews();
                                                        listaJueves.removeAllViews();
                                                        listaViernes.removeAllViews();

                                                        TraerHorario(id);
                                                    }
                                                });

                                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                });

                                                final AlertDialog alertDialog = alertDialogBuilder.create();

                                                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                                    @Override
                                                    public void onShow(DialogInterface arg0) {
                                                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                                                    }
                                                });

                                                alertDialog.show();
                                            }
                                        });

                                        LinearLayout layoutEditar = registro.findViewById(R.id.layoutEditar);
                                        layoutEditar.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                int PICK_CONTACT_REQUEST = 1;

                                                Intent pickContactIntent = new Intent(getContext(), HorarioFragmentModificar.class);
                                                try {
                                                    pickContactIntent.putExtra("id", id);
                                                    pickContactIntent.putExtra("idHora", valores.getInt("id"));
                                                    startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        listaViernes.addView(separador);

                                    }

                                    SwipeLayout swipeNavLayout = registro.findViewById(R.id.sample1);
                                    swipeNavLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            TextView vacioLunes = new TextView(getContext());
                            vacioLunes.setText("¡No hay ninguna hora programada!");
                            vacioLunes.setGravity(Gravity.CENTER_HORIZONTAL);

                            TextView vacioMartes = new TextView(getContext());
                            vacioMartes.setText("¡No hay ninguna hora programada!");
                            vacioMartes.setGravity(Gravity.CENTER_HORIZONTAL);

                            TextView vacioMiercoles = new TextView(getContext());
                            vacioMiercoles.setText("¡No hay ninguna hora programada!");
                            vacioMiercoles.setGravity(Gravity.CENTER_HORIZONTAL);

                            TextView vacioJueves = new TextView(getContext());
                            vacioJueves.setText("¡No hay ninguna hora programada!");
                            vacioJueves.setGravity(Gravity.CENTER_HORIZONTAL);

                            TextView vacioViernes = new TextView(getContext());
                            vacioViernes.setText("¡No hay ninguna hora programada!");
                            vacioViernes.setGravity(Gravity.CENTER_HORIZONTAL);

                            if(listaLunes.getChildCount() == 0){
                                listaLunes.addView(vacioLunes);
                            }
                            if(listaMartes.getChildCount() == 0){
                                listaMartes.addView(vacioMartes);
                            }
                            if(listaMiercoles.getChildCount() == 0){
                                listaMiercoles.addView(vacioMiercoles);
                            }
                            if(listaJueves.getChildCount() == 0){
                                listaJueves.addView(vacioJueves);
                            }
                            if(listaViernes.getChildCount() == 0){
                                listaViernes.addView(vacioViernes);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received ANError from server
                            Log.d("Horario Error", "onError errorCode : " + anError.getErrorCode());
                            Log.d("Horario Error", "onError errorBody : " + anError.getErrorBody());
                            Log.d("Horario Error", "onError errorDetail : " + anError.getErrorDetail());
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d("Horario Error", "onError errorDetail : " + anError.getErrorDetail());
                            Toast.makeText(getContext(), "Error al traer el Horario. ¿Tiene internet?", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        swipeLayout.setRefreshing(false);
    }

    private void BajaHora(final int id){
        swipeLayout.setRefreshing(true);
        AndroidNetworking.post("https://jpapke.tk/webservice/?opcion=35")
                .addBodyParameter("id", String.valueOf(id))
                .setPriority(Priority.LOW)
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d("Analytics", " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d("Analytics", " bytesSent : " + bytesSent);
                        Log.d("Analytics", " bytesReceived : " + bytesReceived);
                        Log.d("Analytics", " isFromCache : " + isFromCache);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            try {
                                success = response.getInt("success");
                                message = response.getString("message");
                                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        TraerHorario(String.valueOf(id));
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received ANError from server
                            Log.d("Horario Error", "onError errorCode : " + anError.getErrorCode());
                            Log.d("Horario Error", "onError errorBody : " + anError.getErrorBody());
                            Log.d("Horario Error", "onError errorDetail : " + anError.getErrorDetail());
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d("Horario Error", "onError errorDetail : " + anError.getErrorDetail());
                            Toast.makeText(getContext(), "Error al momento de dar de baja esta hora. ¿Tiene internet?", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        swipeLayout.setRefreshing(false);
    }

}