package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Julian on 21-Feb-18.
 */

public class HorarioFragmentModificar extends AppCompatActivity {

    Spinner spAsignatura, spGrupo, spAula, spDia, spHoraInicio, spHoraFin;
    HashMap<Integer, String> spMapAsignatura, spMapGrupo, spMapAula, spMapDia, spMapHoraInicio, spMapHoraFin;
    ArrayAdapter<String> adapterAsignatura, adapterGrupo, adapterAula, adapterDia, adapterHoraInicio, adapterHoraFin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_horario_modificar);

        spAsignatura = findViewById(R.id.tVisita);
        spGrupo = findViewById(R.id.spGrupo);
        spAula = findViewById(R.id.spAula);
        spDia = findViewById(R.id.spDia);
        spHoraInicio = findViewById(R.id.spHoraInicio);
        spHoraFin = findViewById(R.id.spHoraFin);

        final String id = getIntent().getStringExtra("id");
        final int idHora = getIntent().getIntExtra("idHora", 0);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TraerAsignaturas intentoTraer = new TraerAsignaturas();
        intentoTraer.execute(id);

        TraerGrupos intentoTraer2 = new TraerGrupos();
        intentoTraer2.execute(id);

        TraerAulas intentoTraer3 = new TraerAulas();
        intentoTraer3.execute(id);

        TraerDias intentoTraer4 = new TraerDias();
        intentoTraer4.execute(id);

        spDia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String idDia = spMapDia.get(spDia.getSelectedItemPosition());

                TraerHoraInicio intentoTraer = new TraerHoraInicio();
                intentoTraer.execute(id, idDia, String.valueOf(idHora));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spHoraInicio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String idDia = spMapDia.get(spDia.getSelectedItemPosition());
                String idHoraInicio = spMapHoraInicio.get(spHoraInicio.getSelectedItemPosition());

                TraerHoraFin intentoTraer = new TraerHoraFin();
                intentoTraer.execute(id, idDia, idHoraInicio, String.valueOf(idHora));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button btnModificar = findViewById(R.id.btnAgregar);
        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String idDia = spMapDia.get(spDia.getSelectedItemPosition());
                String idHoraInicio = spMapHoraInicio.get(spHoraInicio.getSelectedItemPosition());
                String idHoraFinal = spMapHoraFin.get(spHoraFin.getSelectedItemPosition());
                String idGrupo = spMapGrupo.get(spGrupo.getSelectedItemPosition());
                String idAsignatura = spMapAsignatura.get(spAsignatura.getSelectedItemPosition());
                String idAula = spMapAula.get(spAula.getSelectedItemPosition());

                UpdateHora intentoAgregar = new UpdateHora();
                intentoAgregar.execute(idDia, idHoraInicio, idHoraFinal, id, idGrupo, idAsignatura, idAula, String.valueOf(idHora));

            }
        });

        TraerDatosHora intentoTraerDatos = new TraerDatosHora();
        intentoTraerDatos.execute(String.valueOf(idHora));


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class TraerAsignaturas extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=30";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(HorarioFragmentModificar.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("asignaturas");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    String[] spinnerArray = new String[arrays.size()];
                    spMapAsignatura = new HashMap<Integer, String>();

                    int i = 0;

                    for (final JSONObject valores : arrays) {
                        spMapAsignatura.put(i, valores.getString("id"));
                        spinnerArray[i] = valores.getString("nombre");
                        i++;
                    }


                    adapterAsignatura = new ArrayAdapter<String>(HorarioFragmentModificar.this, android.R.layout.simple_spinner_item, spinnerArray);
                    adapterAsignatura.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spAsignatura.setAdapter(adapterAsignatura);
                    spAsignatura.setSelection(adapterAsignatura.getPosition("Desarrollo de Software"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class TraerGrupos extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=8";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(HorarioFragmentModificar.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("grupos");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    String[] spinnerArray = new String[arrays.size()];
                    spMapGrupo = new HashMap<Integer, String>();

                    int i = 0;

                    for (final JSONObject valores : arrays) {
                        spMapGrupo.put(i, valores.getString("id"));
                        spinnerArray[i] = valores.getString("NombreGrupo");
                        i++;
                    }


                    adapterGrupo = new ArrayAdapter<String>(HorarioFragmentModificar.this, android.R.layout.simple_spinner_item, spinnerArray);
                    adapterGrupo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spGrupo.setAdapter(adapterGrupo);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class TraerAulas extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=31";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(HorarioFragmentModificar.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("aulas");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    String[] spinnerArray = new String[arrays.size()];
                    spMapAula = new HashMap<Integer, String>();

                    int i = 0;

                    for (final JSONObject valores : arrays) {
                        spMapAula.put(i, valores.getString("id"));
                        spinnerArray[i] = valores.getString("nombre");
                        i++;
                    }


                    adapterAula = new ArrayAdapter<String>(HorarioFragmentModificar.this, android.R.layout.simple_spinner_item, spinnerArray);
                    adapterAula.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spAula.setAdapter(adapterAula);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class TraerDias extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=32";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(HorarioFragmentModificar.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("dias");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    String[] spinnerArray = new String[arrays.size()];
                    spMapDia = new HashMap<Integer, String>();

                    int i = 0;

                    for (final JSONObject valores : arrays) {
                        spMapDia.put(i, valores.getString("id"));
                        spinnerArray[i] = valores.getString("dia");
                        i++;
                    }


                    adapterDia = new ArrayAdapter<String>(HorarioFragmentModificar.this, android.R.layout.simple_spinner_item, spinnerArray);
                    adapterDia.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spDia.setAdapter(adapterDia);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class TraerHoraInicio extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=37";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(HorarioFragmentModificar.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                params.put("dia", strings[1]);
                params.put("horamod", strings[2]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("horas");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    String[] spinnerArray = new String[arrays.size()];
                    spMapHoraInicio = new HashMap<Integer, String>();

                    int i = 0;

                    for (final JSONObject valores : arrays) {
                        spMapHoraInicio.put(i, valores.getString("id"));
                        spinnerArray[i] = valores.getString("hora");
                        i++;
                    }


                    adapterHoraInicio = new ArrayAdapter<String>(HorarioFragmentModificar.this, android.R.layout.simple_spinner_item, spinnerArray);
                    adapterHoraInicio.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spHoraInicio.setAdapter(adapterHoraInicio);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class TraerHoraFin extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=38";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(HorarioFragmentModificar.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                params.put("dia", strings[1]);
                params.put("hora", strings[2]);
                params.put("horamod", strings[3]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("horas");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    String[] spinnerArray = new String[arrays.size()];
                    spMapHoraFin = new HashMap<Integer, String>();

                    int i = 0;

                    for (final JSONObject valores : arrays) {
                        spMapHoraFin.put(i, valores.getString("id"));
                        spinnerArray[i] = valores.getString("hora");
                        i++;
                    }


                    adapterHoraFin = new ArrayAdapter<String>(HorarioFragmentModificar.this, android.R.layout.simple_spinner_item, spinnerArray);
                    adapterHoraFin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spHoraFin.setAdapter(adapterHoraFin);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class UpdateHora extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=39";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(HorarioFragmentModificar.this);
            this.pDialog.setMessage("Actualizando Hora ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("dia", strings[0]);
                params.put("horainicio", strings[1]);
                params.put("horafinal", strings[2]);
                params.put("id", strings[3]);
                params.put("grupo", strings[4]);
                params.put("asignatura", strings[5]);
                params.put("aula", strings[6]);
                params.put("idHora", strings[7]);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(HorarioFragmentModificar.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
                finish();

            } else if (success == 2) {
                Log.d("Exists", message);
            } else {
                Log.d("Error", message);
            }
        }
    }

    private class TraerDatosHora extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=36";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(HorarioFragmentModificar.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("horario");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    for (final JSONObject valores : arrays) {
                        spAsignatura.setSelection(adapterAsignatura.getPosition(valores.get("asignatura").toString()));
                        spGrupo.setSelection(adapterGrupo.getPosition(valores.get("grupo").toString()));
                        spAula.setSelection(adapterAula.getPosition(valores.get("aula").toString()));
                        spDia.setSelection(adapterDia.getPosition(valores.get("dia").toString()));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

}
