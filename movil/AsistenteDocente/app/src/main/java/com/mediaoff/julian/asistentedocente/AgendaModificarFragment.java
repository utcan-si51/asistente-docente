package com.mediaoff.julian.asistentedocente;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Ismael on 26-Mar-18.
 * CLASE: AgendaModificarFragment: Permite modificar los pendientes en el sistema, usando el WS
 */

public class AgendaModificarFragment extends AppCompatActivity {

    int idPendiente, idProfesor;
    Context context = this;
    EditText editHour, editDate;
    Calendar mcurrentTime = Calendar.getInstance();
    Calendar mcurrentDate = Calendar.getInstance();
    String fecha;
    SimpleDateFormat sdf;
    TextView txtTitulo, txtCuerpo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_agenda_modificar);

        idPendiente = getIntent().getIntExtra("id", 0);
        idProfesor = getIntent().getIntExtra("idProfesor", 0);
        fecha = getIntent().getStringExtra("fecha");
        String titulo = getIntent().getStringExtra("titulo");
        String cuerpo = getIntent().getStringExtra("cuerpo");

        txtTitulo = findViewById(R.id.tTitulo);
        txtTitulo.setText(titulo);

        txtCuerpo = findViewById(R.id.tDescripcion);
        txtCuerpo.setText(cuerpo);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ///////////<- Time Selector -> ////////////////

        editHour = findViewById(R.id.tHora);

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            mcurrentTime.setTime(sdf.parse(fecha));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        editHour.setText(new SimpleDateFormat("HH:mm:ss").format(mcurrentTime.getTime()));

        editHour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AgendaModificarFragment.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mcurrentTime.set(0, 0, 0, selectedHour, selectedMinute);
                        editHour.setText(new SimpleDateFormat("HH:mm:ss").format(mcurrentTime.getTime()));
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.show();

            }
        });


        ///////////<- Date Selector -> ////////////////

        editDate = findViewById(R.id.tFecha);

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            mcurrentDate.setTime(sdf.parse(fecha));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        editDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(mcurrentTime.getTime()));

        editDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener mDatePicker;

                mDatePicker = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mcurrentDate.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                        editDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(mcurrentDate.getTime()));
                    }

                };

                new DatePickerDialog(context, mDatePicker, mcurrentDate.get(Calendar.YEAR), mcurrentDate.get(Calendar.MONTH), mcurrentDate.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        Button btnModificar = findViewById(R.id.btnMody);
        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtTitulo.getText().toString().equals("") || txtCuerpo.getText().toString().equals("")) {
                    Toast.makeText(AgendaModificarFragment.this, "Los datos no son validos!", Toast.LENGTH_SHORT).show();
                } else {
                    ModificarPendiente intentoAgregar = new ModificarPendiente();
                    intentoAgregar.execute(String.valueOf(idPendiente), String.valueOf(idProfesor), txtTitulo.getText().toString(), editDate.getText().toString() + " " + editHour.getText().toString(), txtCuerpo.getText().toString());
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class ModificarPendiente extends AsyncTask<String, String, JSONObject> {
        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=26"; //Modificar conforme al WS
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(AgendaModificarFragment.this);
            this.pDialog.setMessage("Modificando pendiente ...");
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setIndeterminate(false);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>(); //Modificar atrubutos conforme el WS
                params.put("id", strings[0]);
                params.put("IdProfesor", strings[1]);
                params.put("titulo", strings[2]);
                params.put("fechahora", strings[3]);
                params.put("cuerpo", strings[4]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(AgendaModificarFragment.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
                finish();

            } else if (success == 2) {
                Log.d("Exists", message);
            } else {
                Log.d("Error", message);
            }
        }
    }
}
