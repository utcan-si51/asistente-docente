package com.mediaoff.julian.asistentedocente;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Julian on 21-Feb-18.
 */

public class AccionesAgregarFragment extends AppCompatActivity {

    int idUsuario;
    Spinner spAlumno;
    HashMap<Integer, String> spMapAlumno;
    Context context = this;
    EditText editHour, editDate;
    Calendar mcurrentTime = Calendar.getInstance();
    Calendar mcurrentDate = Calendar.getInstance();
    TextView txtActividad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_acciones_agregar);

        spAlumno = findViewById(R.id.spAlumno);
        idUsuario = getIntent().getIntExtra("idProfesor", 0);
        txtActividad = findViewById(R.id.tActividad);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TraerAlumnos intentoTraer = new TraerAlumnos();
        intentoTraer.execute();


        ///////////<- Time Selector -> ////////////////

        editHour = findViewById(R.id.tHora);
        editHour.setText(new SimpleDateFormat("HH:mm:ss").format(mcurrentTime.getTime()));

        editHour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AccionesAgregarFragment.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mcurrentTime.set(0, 0, 0, selectedHour, selectedMinute);
                        editHour.setText(new SimpleDateFormat("HH:mm:ss").format(mcurrentTime.getTime()));
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.show();

            }
        });


        ///////////<- Date Selector -> ////////////////

        editDate = findViewById(R.id.tFecha);

        editDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(mcurrentTime.getTime()));

        editDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener mDatePicker;

                mDatePicker = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mcurrentDate.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                        editDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(mcurrentDate.getTime()));
                    }

                };

                new DatePickerDialog(context, mDatePicker, mcurrentDate.get(Calendar.YEAR), mcurrentDate.get(Calendar.MONTH), mcurrentDate.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        Button btnModificar = findViewById(R.id.btnAgregar);
        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtActividad.getText().toString().equals("")) {
                    Toast.makeText(AccionesAgregarFragment.this, "No ingreso datos validos!", Toast.LENGTH_SHORT).show();
                } else {
                    AgregarAccion intentoAgregar = new AgregarAccion();
                    intentoAgregar.execute(String.valueOf(idUsuario), txtActividad.getText().toString(), spMapAlumno.get(spAlumno.getSelectedItemPosition()), editDate.getText().toString() + " " + editHour.getText().toString());
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class TraerAlumnos extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=11";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(AccionesAgregarFragment.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("alumnos");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    String[] spinnerArray = new String[arrays.size()];
                    spMapAlumno = new HashMap<Integer, String>();

                    int i = 0;

                    for (final JSONObject valores : arrays) {
                        spMapAlumno.put(i, valores.getString("id"));
                        spinnerArray[i] = valores.getString("NombreAlumno");
                        i++;
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AccionesAgregarFragment.this, android.R.layout.simple_spinner_item, spinnerArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spAlumno.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class AgregarAccion extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=16";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(AccionesAgregarFragment.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("profesor", String.valueOf(strings[0]));
                params.put("actividad", String.valueOf(strings[1]));
                params.put("alumno", String.valueOf(strings[2]));
                params.put("fechahora", String.valueOf(strings[3]));
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(AccionesAgregarFragment.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
                finish();
            } else {
                Log.d("Failure", message);
            }
        }
    }
}
