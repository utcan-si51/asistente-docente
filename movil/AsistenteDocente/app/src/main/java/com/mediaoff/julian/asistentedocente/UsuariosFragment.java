package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Julian on 21-Feb-18.
 */

public class UsuariosFragment extends Fragment {

    View rootView = null;
    LinearLayout lista = null;
    SwipeRefreshLayout swipeLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_usuarios, container, false);
        lista = rootView.findViewById(R.id.usuariosLista);

        TraerUsuarios intentoTraer = new TraerUsuarios();
        intentoTraer.execute();

        swipeLayout = rootView.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                lista.removeAllViews();

                TraerUsuarios intentoTraer = new TraerUsuarios();
                intentoTraer.execute();
                swipeLayout.setRefreshing(false);
            }
        });

        final FloatingActionButton fab = rootView.findViewById(R.id.fabtoolbar_fab);
        MainActivity.morph = rootView.findViewById(R.id.fabtoolbar);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fab.getId() == R.id.fabtoolbar_fab) {
                    MainActivity.morph.show();
                }

                MainActivity.morph.hide();
            }
        });

        RelativeLayout btnAgregar = rootView.findViewById(R.id.layoutAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.morph.hide();

                int PICK_CONTACT_REQUEST = 1;

                Intent pickContactIntent = new Intent(getActivity(), UsuariosAgregarFragment.class);
                startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        lista.removeAllViews();

        TraerUsuarios intentoTraer = new TraerUsuarios();
        intentoTraer.execute();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private class TraerUsuarios extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=4";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getActivity());
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("usuarios");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    for (final JSONObject valores : arrays) {
                        LinearLayout registro = (LinearLayout) getLayoutInflater().inflate(R.layout.item_usuario, null);

                        TextView matricula = registro.findViewById(R.id.info);
                        matricula.setText("Matricula: " + valores.getString("matricula"));

                        TextView nombre = registro.findViewById(R.id.materia);
                        nombre.setText("Nombre: " + valores.getString("nombre") + " " + valores.getString("apellido"));

                        TextView tipo = registro.findViewById(R.id.materia2);

                        if (valores.getInt("tipo") == 0) {
                            tipo.setText("Profesor de Asignatura");
                        } else if (valores.getInt("tipo") == 1) {
                            tipo.setText("Profesor de Tiempo Completo");
                        }

                        LinearLayout b = registro.findViewById(R.id.layoutEliminar);
                        b.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setTitle("¿Esta seguro?");
                                alertDialogBuilder.setMessage("¿Esta seguro que quiere eliminar a este usuario?");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        BajaUsuario intentoBaja = new BajaUsuario();
                                        try {
                                            intentoBaja.execute(valores.getInt("id"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        lista.removeAllViews();

                                        TraerUsuarios intentoTraer = new TraerUsuarios();
                                        intentoTraer.execute();
                                    }
                                });

                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }

                        });

                        LinearLayout b2 = registro.findViewById(R.id.layoutContra);
                        b2.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                Intent intent = new Intent(getActivity(), UsuariosContraFragment.class);
                                try {
                                    intent.putExtra("id", valores.getInt("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                startActivity(intent);
                            }

                        });

                        lista.addView(registro);

                        LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                        paramsSep.height = 4;
                        paramsSep.setMargins(0, 0, 0, 20);

                        View separador = new View(getActivity());
                        separador.setLayoutParams(paramsSep);
                        separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                        lista.addView(separador);

                        SwipeLayout swipeNavLayout = registro.findViewById(R.id.sample1);
                        swipeNavLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class BajaUsuario extends AsyncTask<Integer, Integer, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=3";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getActivity());
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Integer... integers) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(integers[0]));
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }
}
