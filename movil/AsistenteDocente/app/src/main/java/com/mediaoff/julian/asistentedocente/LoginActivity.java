package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;
    private int success = 0;
    private String message = "";
    private EditText txtMatricula, txtContra;
    private Button btnLogin;
    private ProgressDialog pDialog;

    public static boolean isInternetAvailable() {
        String host = "www.google.com";
        int port = 80;
        Socket socket = new Socket();

        try {
            socket.connect(new InetSocketAddress(host, port), 2000);
            socket.close();
            return true;
        } catch (IOException e) {
            try {
                socket.close();
            } catch (IOException es) {
            }
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        AndroidNetworking.initialize(getApplicationContext());

        if (!isInternetAvailable()) {
            Toast.makeText(LoginActivity.this, "Error de conexion. ¿Tiene internet?", Toast.LENGTH_LONG).show();
        }

        //Gestor de "Auto-Login" al momento de salir de la aplicacion sin cerrar la sesion
        sharedpreferences = getSharedPreferences("Login", MODE_PRIVATE);

        if (sharedpreferences.getBoolean("estado", false)) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("id", sharedpreferences.getInt("id", 0));
            intent.putExtra("tipo", sharedpreferences.getInt("tipo", 0));
            intent.putExtra("nombre", sharedpreferences.getString("nombre", "nombre"));
            intent.putExtra("apellido", sharedpreferences.getString("apellido", "apellido"));
            intent.putExtra("Matricula", sharedpreferences.getString("matricula", "matricula"));
            startActivity(intent);
        }

        //Login Regular
        txtMatricula = findViewById(R.id.matricula);
        txtContra = findViewById(R.id.password);

        btnLogin = findViewById(R.id.iniciar);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Login(txtMatricula.getText().toString(),sha1(txtContra.getText().toString()));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    private void Login(String matricula, String contra){
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Intentando hacer Login ...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        AndroidNetworking.post("https://jpapke.tk/webservice/?opcion=1")
                .addBodyParameter("matricula", matricula)
                .addBodyParameter("contra", contra)
                .setPriority(Priority.HIGH)
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d("Analytics", " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d("Analytics", " bytesSent : " + bytesSent);
                        Log.d("Analytics", " bytesReceived : " + bytesReceived);
                        Log.d("Analytics", " isFromCache : " + isFromCache);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            try {
                                success = response.getInt("success");
                                message = response.getString("message");
                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        if (success == 1) {
                            Log.d("Datos validos", message);

                            txtContra.setText("");

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            try {
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putBoolean("estado", true);
                                editor.putInt("id", response.getInt("id"));
                                editor.putInt("tipo", response.getInt("tipo"));
                                editor.putString("nombre", response.getString("nombre"));
                                editor.putString("apellido", response.getString("apellido"));
                                editor.putString("matricula", response.getString("matricula"));
                                editor.commit();

                                intent.putExtra("id", response.getInt("id"));
                                intent.putExtra("tipo", response.getInt("tipo"));
                                intent.putExtra("nombre", response.getString("nombre"));
                                intent.putExtra("apellido", response.getString("apellido"));
                                intent.putExtra("Matricula", response.getString("matricula"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            startActivity(intent);
                        } else {
                            Log.d("Datos Invalidos", message);
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(ANError anError) {
                        pDialog.dismiss();
                        if (anError.getErrorCode() != 0) {
                            // received ANError from server
                            Log.d("Login Error", "onError errorCode : " + anError.getErrorCode());
                            Log.d("Login Error", "onError errorBody : " + anError.getErrorBody());
                            Log.d("Login Error", "onError errorDetail : " + anError.getErrorDetail());
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d("Login Error", "onError errorDetail : " + anError.getErrorDetail());
                            Toast.makeText(LoginActivity.this, "Error al iniciar sesion. ¿Tiene internet?", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
