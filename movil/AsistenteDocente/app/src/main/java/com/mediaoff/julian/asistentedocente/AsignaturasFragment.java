package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Julian on 21-Feb-18.
 */

public class AsignaturasFragment extends Fragment {

    View rootView = null;
    LinearLayout listaAsignaturas;
    String id;
    SwipeRefreshLayout swipeLayout;
    String m_Text = "";
    FABToolbarLayout morph;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_asignaturas_lista, container, false);
        listaAsignaturas = rootView.findViewById(R.id.layoutAsignaturas);

        id = String.valueOf(getActivity().getIntent().getIntExtra("id", 0));

        TraerAsignaturas intentoTraer = new TraerAsignaturas(getContext());
        intentoTraer.execute(id);

        swipeLayout = rootView.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                listaAsignaturas.removeAllViews();

                TraerAsignaturas intentoTraer = new TraerAsignaturas(getContext());
                intentoTraer.execute(id);
                swipeLayout.setRefreshing(false);
            }
        });

        final FloatingActionButton fab = rootView.findViewById(R.id.fabtoolbar_fab);
        morph = rootView.findViewById(R.id.fabtoolbar);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fab.getId() == R.id.fabtoolbar_fab) {
                    morph.show();
                }

                morph.hide();
            }
        });

        RelativeLayout btnAgregar = rootView.findViewById(R.id.layoutAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                morph.hide();

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Agregar Asignatura");

                View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.text_inpu_password, null, false);
                final EditText input = viewInflated.findViewById(R.id.input);
                builder.setView(viewInflated);

                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        m_Text = input.getText().toString();

                        AgregarAsignatura intentoAgregar = new AgregarAsignatura();
                        intentoAgregar.execute(id, m_Text);

                        listaAsignaturas.removeAllViews();

                        TraerAsignaturas intentoTraer = new TraerAsignaturas(getContext());
                        intentoTraer.execute(id);
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        listaAsignaturas.removeAllViews();

        TraerAsignaturas intentoTraer = new TraerAsignaturas(getContext());
        intentoTraer.execute(id);
    }

    private class TraerAsignaturas extends AsyncTask<String, Context, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=30";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        private final Context mContext;
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        public TraerAsignaturas(final Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getContext());
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("asignaturas");
                    int size = jsonArray.length();

                    if (size == 0) {
                        TextView vacio = new TextView(mContext);
                        vacio.setText("¡No tiene ninguna asignatura asignada!");
                        vacio.setGravity(Gravity.CENTER_HORIZONTAL);

                        listaAsignaturas.addView(vacio);
                    } else {
                        for (int i = 0; i < size; i++) {
                            JSONObject another_json_object = jsonArray.getJSONObject(i);
                            arrays.add(another_json_object);
                        }

                        JSONObject[] jsons = new JSONObject[arrays.size()];
                        arrays.toArray(jsons);

                        for (final JSONObject valores : arrays) {
                            LinearLayout registro = (LinearLayout) getLayoutInflater().inflate(R.layout.item_asignatura, null);

                            LinearLayout asignatura = registro.findViewById(R.id.unidadesContainer);
                            asignatura.setId(valores.getInt("id"));

                            TextView nombre = asignatura.findViewById(R.id.NombreAsig);
                            nombre.setText(valores.getString("nombre"));

                            Button btnAgregar = asignatura.findViewById(R.id.btnAgregar);
                            btnAgregar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    int PICK_CONTACT_REQUEST = 1;

                                    Intent pickContactIntent = new Intent(getContext(), AsignaturaFragmentUnidadAgregar.class);
                                    try {
                                        pickContactIntent.putExtra("id", valores.getString("id"));
                                        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            Button btnEliminar = asignatura.findViewById(R.id.btnEliminar);
                            btnEliminar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                                    alertDialogBuilder.setTitle("¿Esta seguro?");
                                    alertDialogBuilder.setMessage("¿Esta seguro que quiere eliminar a esta asignatura?");
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            EliminarAsignatura intentoEliminar = new EliminarAsignatura();
                                            try {
                                                intentoEliminar.execute(valores.getString("id"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();
                                }
                            });

                            listaAsignaturas.addView(registro);

                            TraerUnidades intentoTraer = new TraerUnidades(mContext);
                            intentoTraer.execute(valores.getString("id"));

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class TraerUnidades extends AsyncTask<String, Context, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=40";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        private final Context mContext;
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        public TraerUnidades(final Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getContext());
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<JSONObject>();

            if (jsonObject != null) {
                try {

                    JSONArray jsonArray = jsonObject.getJSONArray("unidades");
                    int size = jsonArray.length();

                    for (int i = 0; i < size; i++) {
                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                        arrays.add(another_json_object);
                    }

                    JSONObject[] jsons = new JSONObject[arrays.size()];
                    arrays.toArray(jsons);

                    for (final JSONObject valores : arrays) {
                        LinearLayout registro = (LinearLayout) getLayoutInflater().inflate(R.layout.item_asignatura_lista, null);

                        int id = getResources().getIdentifier(valores.getString("idAsig"), "id", getContext().getPackageName());
                        LinearLayout test = rootView.findViewById(id);
                        LinearLayout test2 = test.findViewById(R.id.unidadesContainerLista);

                        TextView unidad = registro.findViewById(R.id.UnidadTitulo);
                        unidad.setText(valores.getString("unidad"));

                        TextView unidadContenido = registro.findViewById(R.id.UnidadContenido);
                        unidadContenido.setText(valores.getString("contenidoCorto"));

                        LinearLayout layoutContenido = registro.findViewById(R.id.layoutContenido);
                        layoutContenido.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                int PICK_CONTACT_REQUEST = 1;

                                Intent pickContactIntent = new Intent(getContext(), AsignaturasContenidoFragment.class);
                                try {
                                    pickContactIntent.putExtra("id", valores.getString("id"));
                                    startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                        paramsSep.height = 4;
                        paramsSep.setMargins(0, 0, 0, 20);

                        View separador = new View(mContext);
                        separador.setLayoutParams(paramsSep);
                        separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                        test2.addView(registro);
                        test2.addView(separador);

                        SwipeLayout swipeNavLayout = registro.findViewById(R.id.sample1);
                        swipeNavLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    private class EliminarAsignatura extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=44";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getContext());
            this.pDialog.setMessage("Eliminando Asignatura ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

                    listaAsignaturas.removeAllViews();

                    TraerAsignaturas intentoTraer = new TraerAsignaturas(getContext());
                    intentoTraer.execute(id);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else if (success == 2) {
                Log.d("Exists", message);
            } else {
                Log.d("Error", message);
            }
        }
    }

    private class AgregarAsignatura extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=49";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getContext());
            this.pDialog.setMessage("Agregando Asignatura ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", strings[0]);
                params.put("nombre", strings[1]);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else if (success == 2) {
                Log.d("Exists", message);
            } else {
                Log.d("Error", message);
            }
        }
    }
}
