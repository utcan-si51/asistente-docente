package com.mediaoff.julian.asistentedocente;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ismael on 26-March-18.
 */

public class AgendaFragment extends Fragment {

    View rootView = null; //Variable de vista maestrs
    LinearLayout lista = null; //Variable de vista con estilo LinearLayout
    SwipeRefreshLayout swipeLayout;

    //Función que recupera el ID del profesor
    public int ObtieneIDUsuario() {
        int idProfesor = getActivity().getIntent().getIntExtra("id", 0);
        return idProfesor;
    }

    //Función que obtiene la fecha y hora de conclusión de actividades
    public String ObtieneFechaHoraCierre() {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        int hora = today.hour;
        int minuto = today.minute;
        int dia = today.monthDay;
        int mes = today.month;
        int anio = today.year;
        mes = mes + 1;
        String fechaHora = anio + "/" + mes + "/" + dia + " " + hora + ":" + minuto + ":00";

        return fechaHora;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_agenda, container, false);
        lista = rootView.findViewById(R.id.agendaLista);
        //Ejecución de la consulta de Agenda
        TraerPendientes intentoTraer = new TraerPendientes();
        intentoTraer.execute();

        swipeLayout = rootView.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                lista.removeAllViews();

                TraerPendientes intentoTraer = new TraerPendientes();
                intentoTraer.execute();
                swipeLayout.setRefreshing(false);
            }
        });

        final FloatingActionButton fab = rootView.findViewById(R.id.fabtoolbar_fab);
        MainActivity.morph = rootView.findViewById(R.id.fabtoolbar);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fab.getId() == R.id.fabtoolbar_fab) {
                    MainActivity.morph.show();
                }

                MainActivity.morph.hide();
            }
        });

        RelativeLayout btnAgregar = rootView.findViewById(R.id.layoutAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.morph.hide();

                int PICK_CONTACT_REQUEST = 1;

                Intent pickContactIntent = new Intent(getActivity(), AgendaAgregarFragment.class);
                pickContactIntent.putExtra("idProfesor", ObtieneIDUsuario());
                startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
            }
        });

        return rootView;
    }

    //Función que se ejecuta cuando la vista es creada en ejecución
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    //Recarga la pantalla de inicio con los datos actualzados
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        lista.removeAllViews();

        TraerPendientes intentoTraer = new TraerPendientes();
        intentoTraer.execute();
    }

    //Función que muestra la lisa de usuarios en la aplicación
    private class TraerPendientes extends AsyncTask<String, String, JSONObject> {
        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=27";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        //Funcion que ejecuta los parámetros dentro del WebService
        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> parametros = new HashMap<>();
                parametros.put("id", String.valueOf(ObtieneIDUsuario()));
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", parametros);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        //Si la aplicación está esperando información del webservice muestra un mensaje de espera
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getActivity());
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        //Ejecución de recogida y despliegue de datos de la agenda
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";
            ArrayList<JSONObject> arrays = new ArrayList<>();

            if (jsonObject != null) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("pendientes"); //Revisar el nombre: agenda o pendientes
                    int size = jsonArray.length();

                    if (size == 0) {
                        TextView vacio = new TextView(getActivity());
                        vacio.setText("¡No tiene ningun pendiente!");
                        vacio.setGravity(Gravity.CENTER_HORIZONTAL);

                        lista.addView(vacio);
                    } else {
                        for (int i = 0; i < size; i++) {
                            JSONObject another_json_object = jsonArray.getJSONObject(i);
                            arrays.add(another_json_object);
                        }

                        JSONObject[] ArrJson = new JSONObject[arrays.size()];
                        arrays.toArray(ArrJson);

                        //Muestra la información recuperada de la agenda en pantalla
                        for (final JSONObject valores : arrays) {
                            LinearLayout agenda = (LinearLayout) getLayoutInflater().inflate(R.layout.item_agenda, null);

                            TextView Fecha = agenda.findViewById(R.id.lFechaHora);
                            Fecha.setText("Fecha y hora: " + valores.getString("fechainicio")); //Modificar valores con respecto al WS
                            TextView TituloAgenda = agenda.findViewById(R.id.lTituloAngenda);
                            TituloAgenda.setText("Titulo: " + valores.getString("titulo")); //Modificar valores con respecto al WS
                            TextView ContenidoAgenda = agenda.findViewById(R.id.lContenidoAgenda);
                            ContenidoAgenda.setText("Contenido: " + valores.getString("cuerpo")); //Modificar valores con respecto al WS

                            //Añade funcionalidad al botón de Finalizar
                            LinearLayout btnFin = agenda.findViewById(R.id.layoutConcluir);
                            btnFin.setOnClickListener(new View.OnClickListener() {
                                //Muestra un mensaje de alerta en pantalla
                                @Override
                                public void onClick(View view) {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setTitle("¿Esta seguro?");
                                    alertDialogBuilder.setMessage("¿Esta seguro que quiere concluir esta actividad?");
                                    alertDialogBuilder.setCancelable(false);

                                    //Opción verdadera, finalza la actividad de la agenda
                                    alertDialogBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            concluirActividad intentoConcluir = new concluirActividad();
                                            try {
                                                intentoConcluir.execute(valores.getInt("id"));
                                                TraerPendientes intentoTraer = new TraerPendientes();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            lista.removeAllViews();

                                            TraerPendientes intentoTraer = new TraerPendientes();
                                            intentoTraer.execute();
                                        }
                                    });
                                    //Opcion falsa: No realiza ninguna acción y continpua el tiempo de ejecución
                                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();
                                }
                            });
                            //Añade funcionalidad al botón de modificar
                            LinearLayout btnEdit = agenda.findViewById(R.id.layoutMod);
                            btnEdit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    int PICK_CONTACT_REQUEST = 1;
                                    try {
                                        Intent pickContactIntent = new Intent(getActivity(), AgendaModificarFragment.class);
                                        pickContactIntent.putExtra("id", valores.getInt("id"));
                                        pickContactIntent.putExtra("idProfesor", ObtieneIDUsuario());
                                        pickContactIntent.putExtra("titulo", valores.getString("titulo"));
                                        pickContactIntent.putExtra("cuerpo", valores.getString("cuerpo"));
                                        pickContactIntent.putExtra("fecha", valores.getString("fechainicio"));
                                        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            });
                            lista.addView(agenda);
                            //Añade elementos gráficos a la pantalla
                            LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                            paramsSep.height = 4;
                            paramsSep.setMargins(0, 0, 0, 20);

                            View separador = new View(getActivity());
                            separador.setLayoutParams(paramsSep);
                            separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                            lista.addView(separador);

                            SwipeLayout swipeNavLayout = agenda.findViewById(R.id.sample1);
                            swipeNavLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (this.pDialog != null && this.pDialog.isShowing()) {
                    this.pDialog.dismiss();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }

    //Funcion que permite dar de baja información de la Agenda
    private class concluirActividad extends AsyncTask<Integer, Integer, JSONObject> {
        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=25";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected JSONObject doInBackground(Integer... integers) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(integers[0]));
                params.put("fechafin", ObtieneFechaHoraCierre());
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(getActivity());
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }
            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (success == 1) {
                Log.d("Success", message);
            } else {
                Log.d("Failure", message);
            }
        }
    }
}