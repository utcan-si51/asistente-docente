package com.mediaoff.julian.asistentedocente;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static FABToolbarLayout morph;
    NavigationView navigationView;
    int idProfesor;
    String nombre, apellido, matricula;
    Fragment fragment = null;
    private int tipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        idProfesor = getIntent().getIntExtra("id",0);
        tipo = getIntent().getIntExtra("tipo", 0);
        nombre = getIntent().getStringExtra("nombre");
        apellido = getIntent().getStringExtra("apellido");
        matricula = getIntent().getStringExtra("Matricula");

        NavigationView navigationView = findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);

        TextView nombreMenu = hView.findViewById(R.id.nombreUsuario);
        nombreMenu.setText(nombre + " " + apellido);

        TextView matriculaMenu = hView.findViewById(R.id.matriculaUsuario);
        matriculaMenu.setText(matricula);

        if (savedInstanceState == null) {

            Fragment fragment;
            fragment = new InicioFragment();

            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();

                ft.replace(R.id.screen_area, fragment);

                ft.commit();
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (morph != null) {
            if (morph.isFab()) {
                morph = null;
                super.onBackPressed();
            } else {
                morph.hide();
            }
        } else {
            FragmentManager fm = MainActivity.this.getSupportFragmentManager();
            if (fm.getBackStackEntryCount() != 0) {
                fm.popBackStack();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("¿Esta seguro que quiere cerrar su sesion?")
                        .setCancelable(false)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPreferences sharedpreferences = getSharedPreferences("Login", MainActivity.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();

                                MainActivity.this.finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_inicio) {
            fragment = new InicioFragment();
        } else if (id == R.id.nav_horario) {
            fragment = new HorarioFragment();
        } else if (id == R.id.nav_asignaturas) {
            fragment = new AsignaturasFragment();
        } else if (id == R.id.nav_agenda) {
            fragment = new AgendaFragment();
        } else if (id == R.id.nav_acciones) {
            fragment = new AccionesFragment();
        } else if (id == R.id.nav_visitas) {
            fragment = new VisitasFragment();
        } else if (id == R.id.nav_cursos) {
            fragment = new CursosFragment();
        } else if (id == R.id.nav_contra) {
            fragment = new ContraFragment();
        } else if (id == R.id.nav_alumnos) {
            fragment = new AlumnosFragment();
        } else if (id == R.id.nav_grupos) {
            fragment = new GruposFragment();
        } else if (id == R.id.nav_cerrar) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("¿Esta seguro que quiere cerrar su sesion?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SharedPreferences sharedpreferences = getSharedPreferences("Login", MainActivity.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.clear();
                            editor.commit();

                            MainActivity.this.finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else if (id == R.id.nav_reportes) {
            fragment = new ReportesFragment();
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();

            if (LoginActivity.isInternetAvailable()) {
                ft.replace(R.id.screen_area, fragment).addToBackStack(null);

                ft.commit();
            } else {
                Toast.makeText(MainActivity.this, "Error de conexion. ¿Tiene internet?", Toast.LENGTH_LONG).show();
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
