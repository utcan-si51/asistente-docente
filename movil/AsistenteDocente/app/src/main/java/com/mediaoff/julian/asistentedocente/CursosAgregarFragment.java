package com.mediaoff.julian.asistentedocente;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Julian on 21-Feb-18.
 */

public class CursosAgregarFragment extends AppCompatActivity {

    Context context = this;
    int idUsuario;
    EditText editHour, editDate, txtDescripcion, txtLugar;
    Calendar mcurrentTime = Calendar.getInstance();
    Calendar mcurrentDate = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_cursos_agregar);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtDescripcion = findViewById(R.id.tCurso);
        txtLugar = findViewById(R.id.tLugar);

        idUsuario = getIntent().getIntExtra("id", 0);

        ///////////<- Time Selector -> ////////////////

        editHour = findViewById(R.id.tHora);
        editHour.setText(new SimpleDateFormat("HH:mm:ss").format(mcurrentTime.getTime()));

        editHour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CursosAgregarFragment.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mcurrentTime.set(0, 0, 0, selectedHour, selectedMinute);
                        editHour.setText(new SimpleDateFormat("HH:mm:ss").format(mcurrentTime.getTime()));
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.show();

            }
        });


        ///////////<- Date Selector -> ////////////////

        editDate = findViewById(R.id.tFecha);
        editDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(mcurrentTime.getTime()));

        editDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener mDatePicker;

                mDatePicker = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mcurrentDate.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                        editDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(mcurrentDate.getTime()));
                    }

                };

                new DatePickerDialog(context, mDatePicker, mcurrentDate.get(Calendar.YEAR), mcurrentDate.get(Calendar.MONTH), mcurrentDate.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        Button btnAgregar = findViewById(R.id.btnAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtDescripcion.getText().equals("") || txtLugar.getText().equals("")) {
                    Toast.makeText(CursosAgregarFragment.this, "No ingreso datos validos!", Toast.LENGTH_SHORT).show();
                } else {
                    AgregarCurso intentoMod = new AgregarCurso();
                    intentoMod.execute(String.valueOf(idUsuario), txtDescripcion.getText().toString(), editDate.getText().toString() + " " + editHour.getText().toString(), txtLugar.getText().toString());
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class AgregarCurso extends AsyncTask<String, String, JSONObject> {

        private static final String LOGIN_URL = "https://jpapke.tk/webservice/?opcion=20";
        //private static final String LOGIN_URL = "http://10.0.2.236:81/webservice/index.php";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.pDialog = new ProgressDialog(CursosAgregarFragment.this);
            this.pDialog.setMessage("Cargando ...");
            this.pDialog.setIndeterminate(false);
            this.pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pDialog.setCancelable(false);
            this.pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("IdProfesor", String.valueOf(strings[0]));
                params.put("descripcion", String.valueOf(strings[1]));
                params.put("fechahora", String.valueOf(strings[2]));
                params.put("lugar", String.valueOf(strings[3]));
                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if (json != null) {
                    Log.d("JSON result", json.toString());
                    return json;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            int success = 0;
            String message = "";

            if (this.pDialog != null && this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }

            if (jsonObject != null) {
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    message = jsonObject.getString(TAG_MESSAGE);
                    Toast.makeText(CursosAgregarFragment.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Log.d("Success", message);
                finish();
            } else {
                Log.d("Failure", message);
            }
        }
    }
}
