package com.mediaoff.julian.asistentedocente;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Julian on 21-Feb-18.
 */

public class InicioFragment extends Fragment {

    View rootView = null;
    LinearLayout layoutHorarioHoy, layoutPendientesHoy;
    NavigationView navigationView;
    SwipeRefreshLayout swipeLayout;
    int id, diaNum;
    String fechaSQL;
    private ArrayList<JSONObject> arrays = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_inicio, container, false);
        swipeLayout = rootView.findViewById(R.id.swipe_container);

        AndroidNetworking.initialize(getActivity().getApplicationContext());

        layoutHorarioHoy = rootView.findViewById(R.id.layoutHorarioHoy);
        layoutPendientesHoy = rootView.findViewById(R.id.layoutPendientesHoy);
        navigationView = getActivity().findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_inicio);

        id = getActivity().getIntent().getIntExtra("id", 0);

        Calendar calendar = Calendar.getInstance();
        diaNum = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormatter.setLenient(false);
        Date hoy = new Date();
        fechaSQL = dateFormatter.format(hoy);

        TraerHorario(id,diaNum);

        TraerPendientes(id,fechaSQL);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                layoutHorarioHoy.removeAllViews();
                layoutPendientesHoy.removeAllViews();

                TraerHorario(id,diaNum);

                TraerPendientes(id,fechaSQL);

                swipeLayout.setRefreshing(false);
            }
        });

        TextView verHorario = rootView.findViewById(R.id.verhorario);
        verHorario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new HorarioFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();

                ft.replace(R.id.screen_area, fragment);

                ft.commit();

                navigationView.setCheckedItem(R.id.nav_horario);

            }
        });

        TextView verAgenda = rootView.findViewById(R.id.veragenda);
        verAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new AgendaFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();

                ft.replace(R.id.screen_area, fragment);

                ft.commit();

                navigationView.setCheckedItem(R.id.nav_agenda);
            }
        });

        return rootView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void TraerHorario(int id, int dia){
        swipeLayout.setRefreshing(true);
        AndroidNetworking.post("https://jpapke.tk/webservice/?opcion=45")
                .addBodyParameter("id", String.valueOf(id))
                .addBodyParameter("dia", String.valueOf(dia))
                .setPriority(Priority.LOW)
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d("Analytics", " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d("Analytics", " bytesSent : " + bytesSent);
                        Log.d("Analytics", " bytesReceived : " + bytesReceived);
                        Log.d("Analytics", " isFromCache : " + isFromCache);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            try {

                                JSONArray jsonArray = response.getJSONArray("horario");
                                int size = jsonArray.length();

                                if (size == 0) {
                                    LinearLayout.LayoutParams paramsVacio = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    paramsVacio.setMargins(0, 20, 0, 20);

                                    TextView vacio = new TextView(getContext());
                                    vacio.setLayoutParams(paramsVacio);
                                    vacio.setGravity(Gravity.CENTER_HORIZONTAL);
                                    vacio.setText("¡Usted no tiene ninguna hora programada hoy!");
                                    layoutHorarioHoy.addView(vacio);

                                } else {
                                    for (int i = 0; i < size; i++) {
                                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                                        arrays.add(another_json_object);
                                    }

                                    JSONObject[] jsons = new JSONObject[arrays.size()];
                                    arrays.toArray(jsons);

                                    for (final JSONObject valores : arrays) {
                                        LinearLayout registro = (LinearLayout) getLayoutInflater().inflate(R.layout.item_horario_home, null);

                                        TextView titulo = registro.findViewById(R.id.horarioHoraGrupo);
                                        titulo.setText(valores.getString("horainicio") + " - " + valores.getString("horafin") + " - Grupo " + valores.getString("grupo"));

                                        TextView materia = registro.findViewById(R.id.materia);
                                        materia.setText(valores.getString("asignatura"));

                                        TextView aula = registro.findViewById(R.id.aula);
                                        aula.setText("Aula: " + valores.getString("aula"));

                                        layoutHorarioHoy.addView(registro);

                                        LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                                        paramsSep.height = 4;
                                        paramsSep.setMargins(0, 0, 0, 20);

                                        View separador = new View(getContext());
                                        separador.setLayoutParams(paramsSep);
                                        separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                                        layoutHorarioHoy.addView(separador);

                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received ANError from server
                            Log.d("Home Error", "onError errorCode : " + anError.getErrorCode());
                            Log.d("Home Error", "onError errorBody : " + anError.getErrorBody());
                            Log.d("Home Error", "onError errorDetail : " + anError.getErrorDetail());
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d("Home Error", "onError errorDetail : " + anError.getErrorDetail());
                            Toast.makeText(getContext(), "Error al traer el Horario. ¿Tiene internet?", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void TraerPendientes(int id, String dia){
        AndroidNetworking.post("https://jpapke.tk/webservice/?opcion=46")
                .addBodyParameter("id", String.valueOf(id))
                .addBodyParameter("dia", dia)
                .setPriority(Priority.LOW)
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d("Analytics", " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d("Analytics", " bytesSent : " + bytesSent);
                        Log.d("Analytics", " bytesReceived : " + bytesReceived);
                        Log.d("Analytics", " isFromCache : " + isFromCache);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ArrayList<JSONObject> arrays = new ArrayList<>();

                        if (response != null) {
                            try {
                                JSONArray jsonArray = response.getJSONArray("pendientes");
                                int size = jsonArray.length();

                                if (size == 0) {
                                    LinearLayout.LayoutParams paramsVacio = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    paramsVacio.setMargins(0, 20, 0, 20);

                                    TextView vacio = new TextView(getActivity());
                                    vacio.setLayoutParams(paramsVacio);
                                    vacio.setGravity(Gravity.CENTER_HORIZONTAL);
                                    vacio.setText("¡Usted no tiene pendientes para hoy!");
                                    layoutPendientesHoy.addView(vacio);

                                } else {

                                    for (int i = 0; i < size; i++) {
                                        JSONObject another_json_object = jsonArray.getJSONObject(i);
                                        arrays.add(another_json_object);
                                    }

                                    JSONObject[] ArrJson = new JSONObject[arrays.size()];
                                    arrays.toArray(ArrJson);

                                    for (final JSONObject valores : arrays) {
                                        LinearLayout agenda = (LinearLayout) getLayoutInflater().inflate(R.layout.item_agenda_inicio, null);

                                        TextView Fecha = agenda.findViewById(R.id.lFechaHora);
                                        Fecha.setText("Fecha y hora: " + valores.getString("fechainicio")); //Modificar valores con respecto al WS
                                        TextView TituloAgenda = agenda.findViewById(R.id.lTituloAngenda);
                                        TituloAgenda.setText("Titulo: " + valores.getString("titulo")); //Modificar valores con respecto al WS
                                        TextView ContenidoAgenda = agenda.findViewById(R.id.lContenidoAgenda);
                                        ContenidoAgenda.setText("Contenido: " + valores.getString("cuerpo")); //Modificar valores con respecto al WS

                                        layoutPendientesHoy.addView(agenda);

                                        LinearLayout.LayoutParams paramsSep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                                        paramsSep.height = 4;
                                        paramsSep.setMargins(0, 0, 0, 20);

                                        View separador = new View(getActivity());
                                        separador.setLayoutParams(paramsSep);
                                        separador.setBackgroundColor(getResources().getColor(R.color.darkGrey));

                                        layoutPendientesHoy.addView(separador);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        swipeLayout.setRefreshing(false);
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (anError.getErrorCode() != 0) {
                            // received ANError from server
                            Log.d("Home Error", "onError errorCode : " + anError.getErrorCode());
                            Log.d("Home Error", "onError errorBody : " + anError.getErrorBody());
                            Log.d("Home Error", "onError errorDetail : " + anError.getErrorDetail());
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d("Home Error", "onError errorDetail : " + anError.getErrorDetail());
                            Toast.makeText(getContext(), "Error al traer pendientes. ¿Tiene internet?", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }



}
