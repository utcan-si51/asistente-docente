<?php

require_once('./config/Conexion.php');

class visita {	
	private $db;
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function nuevaVisita($NombreVisita,$DescripcionVisita,$Fecha,$IDProfesor,$IDGrupo){
		$json = array();
		$query = 'INSERT INTO visitas(NombreVisita, DesripcionVisita, Fecha, IDProfesor, Grupo) VALUES("'.$NombreVisita.'","'.$DescripcionVisita.'","'.$Fecha.'","'.$IDProfesor.'","'.$IDGrupo.'")';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Visita programada!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}
	
	public function concluirVisita($id,$FechaConcluido){
		$json = array();
		$query = 'SELECT IDVisita FROM visitas WHERE IDVisita = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Esta visita no existe!";
		}else{
			$query = 'UPDATE visitas SET Estatus = 1, FechaHoraConcluido = "'.$FechaConcluido.'"  WHERE IDVisita ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Visita fue concluida!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json);
	}
	
	public function modificarVisita($id,$NombreVisita,$DescripcionVisita,$Fecha,$IDGrupo){
		$json = array();
		$query = 'SELECT IDVisita FROM visitas WHERE IDVisita = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Esta visita no existe!";
		}else{
			$query = 'UPDATE visitas SET NombreVisita = "'.$NombreVisita.'", DesripcionVisita = "'.$DescripcionVisita.'", Fecha = "'.$Fecha.'", Grupo = "'.$IDGrupo.'" WHERE IDVisita ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Visita actualizada!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json, JSON_UNESCAPED_UNICODE);	
	}
	
	public function listarVisitas($id){
		$json = "";
		$query = 'SELECT visitas.IDVisita, visitas.NombreVisita, visitas.DesripcionVisita, visitas.Fecha, visitas.FechaHoraConcluido, visitas.IDProfesor, grupos.NombreGrupo AS Grupo FROM visitas JOIN grupos ON visitas.Grupo = grupos.IDGrupo WHERE visitas.Estatus != 1 AND visitas.IDProfesor = "'.$id.'" ORDER BY visitas.Fecha;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'visitas': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDVisita'],'nombre' => $datos['NombreVisita'],'descripcion' => $datos['DesripcionVisita'],'fechainicio' => $datos['Fecha'],'fechafin' => $datos['FechaHoraConcluido'],'idProfesor' => $datos['IDProfesor'],'grupo' => $datos['Grupo']));
			}else{
				$json .= json_encode(array('id' => $datos['IDVisita'],'nombre' => $datos['NombreVisita'],'descripcion' => $datos['DesripcionVisita'],'fechainicio' => $datos['Fecha'],'fechafin' => $datos['FechaHoraConcluido'],'idProfesor' => $datos['IDProfesor'],'grupo' => $datos['Grupo'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
}
?>