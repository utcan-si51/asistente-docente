<?php

require_once('./config/Conexion.php');

class accion {	
	private $db;
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function nuevaAccion($Actividad,$IDAlumno,$FechaHora,$IDProfesor){
		$json = array();
		$query = 'INSERT INTO acciones_de_mejora(Actividad, IDAlumno, FechaHora, IDProfesor) VALUES("'.$Actividad.'","'.$IDAlumno.'","'.$FechaHora.'","'.$IDProfesor.'")';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Accion de mejora programada!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}
	
	public function concluirAccion($id,$FechaConcluido,$aprobado){
		$json = array();
		$query = 'SELECT IDActMejora FROM acciones_de_mejora WHERE IDActMejora = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Esta accion de mejora no existe!";
		}else{
			$query = 'UPDATE acciones_de_mejora SET Estatus = 1, FechaHoraConcluido = "'.$FechaConcluido.'", Calificacion = "'.$aprobado.'"  WHERE IDActMejora ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Accion de mejora fue concluida!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json);
	}
	
	public function modificarAccion($id,$Actividad,$IDAlumno,$FechaHora){
		$json = array();
		$query = 'SELECT IDActMejora FROM acciones_de_mejora WHERE IDActMejora = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Esta accion de mejora no existe!";
		}else{
			$query = 'UPDATE acciones_de_mejora SET Actividad = "'.$Actividad.'", IDAlumno = "'.$IDAlumno.'", FechaHora = "'.$FechaHora.'" WHERE IDActMejora ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Accion de mejora actualizada!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json, JSON_UNESCAPED_UNICODE);	
	}
	
	public function listarAcciones($id){
		$json = "";
		$query = 'SELECT acciones_de_mejora.*, alumnos.NombreAlumno, grupos.NombreGrupo FROM acciones_de_mejora JOIN alumnos ON acciones_de_mejora.IDAlumno = alumnos.IDAlumno JOIN grupos ON alumnos.IDGrupo = grupos.IDGrupo WHERE acciones_de_mejora.Estatus != 1 AND acciones_de_mejora.IDProfesor = "'.$id.'" ORDER BY acciones_de_mejora.FechaHora;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'acciones': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDActMejora'],'actividad' => $datos['Actividad'],'IDAlumno' => $datos['IDAlumno'],'NombreAlumno' => $datos['NombreAlumno'],'Grupo' => $datos['NombreGrupo'],'fechainicio' => $datos['FechaHora'],'fechafin' => $datos['FechaHoraConcluido'],'idProfesor' => $datos['IDProfesor']));
			}else{
				$json .= json_encode(array('id' => $datos['IDActMejora'],'actividad' => $datos['Actividad'],'IDAlumno' => $datos['IDAlumno'],'NombreAlumno' => $datos['NombreAlumno'],'Grupo' => $datos['NombreGrupo'],'fechainicio' => $datos['FechaHora'],'fechafin' => $datos['FechaHoraConcluido'],'idProfesor' => $datos['IDProfesor'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
}
?>