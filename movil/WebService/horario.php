<?php

require_once('./config/Conexion.php');

class horario {	
	private $db;
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function listarHorario($id){
		$json = "";
		$query = 'SELECT m.IDHorario, grupos.NombreGrupo, asignaturas.NombreAsignatura, aulas.NombreAula, u1.Hora AS "HoraInicio", u2.Hora AS "HoraFin", m.Dia FROM horariodetalle m JOIN grupos ON m.IDGrupo = grupos.IDGrupo JOIN asignaturas ON m.IDAsignatura = asignaturas.IDAsignatura JOIN aulas ON m.IDAula = aulas.IDAula LEFT JOIN `horas` u1 ON (u1.IDHora = m.HoraInicio) LEFT JOIN `horas` u2 ON (u2.IDHora = m.HoraFinal) WHERE m.IDProfesor ="'.$id.'" ORDER BY m.Dia ASC, u1.Hora ASC;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'horario': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDHorario'],'grupo' => $datos['NombreGrupo'],'asignatura' => $datos['NombreAsignatura'],'aula' => $datos['NombreAula'],'horainicio' => $datos['HoraInicio'],'horafin' => $datos['HoraFin'],'dia' => $datos['Dia']));
			}else{
				$json .= json_encode(array('id' => $datos['IDHorario'],'grupo' => $datos['NombreGrupo'],'asignatura' => $datos['NombreAsignatura'],'aula' => $datos['NombreAula'],'horainicio' => $datos['HoraInicio'],'horafin' => $datos['HoraFin'],'dia' => $datos['Dia'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function listarHorasInicio($id,$dia){
		$json = "";
		$listahoras = array();
		$listahorasFinal = array();
		$query = 'SELECT HoraInicio, HoraFinal FROM horariodetalle WHERE Dia = "'.$dia.'" AND IDProfesor = "'.$id.'";';
		$result = array_filter($this->db->seleccionarValores($query));
		
		
		foreach($result as $datos){
			$HoraInicio = $datos['HoraInicio'];
			$HoraFin = $datos['HoraFinal'];

			for ($i = $HoraInicio; $i < $HoraFin; $i++) {
				array_push($listahoras, $i);
			}
		}
		
		$query2 = 'SELECT IDHora, Hora FROM horas;';
		$result2 = array_filter($this->db->seleccionarValores($query2));
		
		
		foreach($result2 as $datos2){
			if(!in_array($datos2['IDHora'], $listahoras)){
				$listahorasFinal[] = array("id"=>$datos2['IDHora'],"Hora"=>$datos2['Hora']);
			}
		}
		
		$json.="{'horas': [";
		
		$i = 0;
		$cant = count($listahorasFinal);
		
		foreach($listahorasFinal as $datos){

			if ($i == $cant - 1) {
				$json .= json_encode(array("id"=>$datos['id'],"hora" => $datos['Hora']));
			}else{
				$json .= json_encode(array("id"=>$datos['id'],"hora" => $datos['Hora'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}

	public function listarHorasFin($id,$dia,$hora){
		$json = "";
		$listahoras = array();
		$listahorasFinal = array();
		$query = 'SELECT HoraInicio, HoraFinal FROM horariodetalle WHERE Dia = "'.$dia.'" AND IDProfesor = "'.$id.'";';
		$result = array_filter($this->db->seleccionarValores($query));
		
		
		foreach($result as $datos){
			$HoraInicio = $datos['HoraInicio'];
			$HoraFin = $datos['HoraFinal'];

			for ($i = $HoraInicio; $i < $HoraFin; $i++) {
				array_push($listahoras, $i);
			}
		}
		
		$query2 = 'SELECT IDHora, Hora FROM horas WHERE IDHora > "'.$hora.'";';
		$result2 = array_filter($this->db->seleccionarValores($query2));
		
		
		foreach($result2 as $datos2){
			if(in_array($datos2['IDHora'], $listahoras)){
				$listahorasFinal[] = array("id"=>$datos2['IDHora'],"Hora"=>$datos2['Hora']);
				break;
			}			
			if(!in_array($datos2['IDHora'], $listahoras)){
				$listahorasFinal[] = array("id"=>$datos2['IDHora'],"Hora"=>$datos2['Hora']);
			}
		}
		
		$json.="{'horas': [";
		
		$i = 0;
		$cant = count($listahorasFinal);
		
		foreach($listahorasFinal as $datos){

			if ($i == $cant - 1) {
				$json .= json_encode(array("id"=>$datos['id'],"hora" => $datos['Hora']));
			}else{
				$json .= json_encode(array("id"=>$datos['id'],"hora" => $datos['Hora'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function agregarHorario($Dia,$HoraInicio,$HoraFinal,$IDProfesor,$IDGrupo,$IDAsignatura,$IDAula){
		$json = array();
		$query = 'INSERT INTO horariodetalle(Dia, HoraInicio, HoraFinal, IDProfesor, IDGrupo, IDAsignatura, IDAula) VALUES ("'.$Dia.'","'.$HoraInicio.'","'.$HoraFinal.'","'.$IDProfesor.'","'.$IDGrupo.'","'.$IDAsignatura.'","'.$IDAula.'")';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Hora programada!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}	
	
	public function bajaHora($id){
		$json = array();
		$query = 'DELETE FROM horariodetalle WHERE IDHorario = "'.$id.'"';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Hora eliminada!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}

	public function traerHora($id){
		$json = "";
		$query = 'SELECT m.IDHorario, grupos.NombreGrupo, asignaturas.NombreAsignatura, aulas.NombreAula, u1.Hora AS "HoraInicio", u2.Hora AS "HoraFin", dias.Dia AS Dia FROM horariodetalle m JOIN grupos ON m.IDGrupo = grupos.IDGrupo JOIN asignaturas ON m.IDAsignatura = asignaturas.IDAsignatura JOIN aulas ON m.IDAula = aulas.IDAula LEFT JOIN `horas` u1 ON (u1.IDHora = m.HoraInicio) LEFT JOIN `horas` u2 ON (u2.IDHora = m.HoraFinal) JOIN dias ON m.Dia = dias.IDDia WHERE m.IDHorario = "'.$id.'" ORDER BY m.Dia ASC, u1.Hora ASC;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'horario': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDHorario'],'grupo' => $datos['NombreGrupo'],'asignatura' => $datos['NombreAsignatura'],'aula' => $datos['NombreAula'],'horainicio' => $datos['HoraInicio'],'horafin' => $datos['HoraFin'],'dia' => $datos['Dia']), JSON_UNESCAPED_UNICODE);
			}else{
				$json .= json_encode(array('id' => $datos['IDHorario'],'grupo' => $datos['NombreGrupo'],'asignatura' => $datos['NombreAsignatura'],'aula' => $datos['NombreAula'],'horainicio' => $datos['HoraInicio'],'horafin' => $datos['HoraFin'],'dia' => $datos['Dia']), JSON_UNESCAPED_UNICODE).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function listarHorasInicioMod($id,$dia,$idMod){
		$json = "";
		$listahoras = array();
		$listahorasFinal = array();
		
		$query = 'SELECT HoraInicio, HoraFinal FROM horariodetalle WHERE IDHorario = "'.$idMod.'" ORDER BY HoraInicio ASC;';
		$result = array_filter($this->db->traerValores($query));
		
		$horaQueryExclude=$this->db->traerValores("SELECT HoraInicio, HoraFinal FROM horariodetalle WHERE IDHorario = $idMod ORDER BY HoraInicio ASC;");		
		$horaExclude = $horaQueryExclude['HoraInicio'];
		
		$query = 'SELECT HoraInicio, HoraFinal FROM horariodetalle WHERE Dia = "'.$dia.'" AND IDProfesor = "'.$id.'" AND HoraInicio != "'.$horaExclude.'";';
		$result = array_filter($this->db->seleccionarValores($query));
		
		
		foreach($result as $datos){
			$HoraInicio = $datos['HoraInicio'];
			$HoraFin = $datos['HoraFinal'];

			for ($i = $HoraInicio; $i < $HoraFin; $i++) {
				array_push($listahoras, $i);
			}
		}
		
		$query2 = 'SELECT IDHora, Hora FROM horas;';
		$result2 = array_filter($this->db->seleccionarValores($query2));
		
		
		foreach($result2 as $datos2){
			if(!in_array($datos2['IDHora'], $listahoras)){
				$listahorasFinal[] = array("id"=>$datos2['IDHora'],"Hora"=>$datos2['Hora']);
			}
		}
		
		$json.="{'horas': [";
		
		$i = 0;
		$cant = count($listahorasFinal);
		
		foreach($listahorasFinal as $datos){

			if ($i == $cant - 1) {
				$json .= json_encode(array("id"=>$datos['id'],"hora" => $datos['Hora']));
			}else{
				$json .= json_encode(array("id"=>$datos['id'],"hora" => $datos['Hora'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}

	public function listarHorasFinMod($id,$dia,$hora,$idMod){
		$json = "";
		$listahoras = array();
		$listahorasFinal = array();
		
		$query = 'SELECT HoraInicio, HoraFinal FROM horariodetalle WHERE IDHorario = "'.$idMod.'" ORDER BY HoraInicio ASC;';
		$result = array_filter($this->db->traerValores($query));
		
		$horaQueryExclude=$this->db->traerValores("SELECT HoraInicio, HoraFinal FROM horariodetalle WHERE IDHorario = $idMod ORDER BY HoraInicio ASC;");		
		$horaExclude = $horaQueryExclude['HoraInicio'];
		
		$query = 'SELECT HoraInicio, HoraFinal FROM horariodetalle WHERE Dia = "'.$dia.'" AND IDProfesor = "'.$id.'" AND HoraInicio != "'.$horaExclude.'";';
		$result = array_filter($this->db->seleccionarValores($query));
		
		
		foreach($result as $datos){
			$HoraInicio = $datos['HoraInicio'];
			$HoraFin = $datos['HoraFinal'];

			for ($i = $HoraInicio; $i < $HoraFin; $i++) {
				array_push($listahoras, $i);
			}
		}
		
		$query2 = 'SELECT IDHora, Hora FROM horas WHERE IDHora > "'.$hora.'";';
		$result2 = array_filter($this->db->seleccionarValores($query2));
		
		
		foreach($result2 as $datos2){
			if(in_array($datos2['IDHora'], $listahoras)){
				$listahorasFinal[] = array("id"=>$datos2['IDHora'],"Hora"=>$datos2['Hora']);
				break;
			}			
			if(!in_array($datos2['IDHora'], $listahoras)){
				$listahorasFinal[] = array("id"=>$datos2['IDHora'],"Hora"=>$datos2['Hora']);
			}
		}
		
		$json.="{'horas': [";
		
		$i = 0;
		$cant = count($listahorasFinal);
		
		foreach($listahorasFinal as $datos){

			if ($i == $cant - 1) {
				$json .= json_encode(array("id"=>$datos['id'],"hora" => $datos['Hora']));
			}else{
				$json .= json_encode(array("id"=>$datos['id'],"hora" => $datos['Hora'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function actualizarHorario($Dia,$HoraInicio,$HoraFinal,$IDProfesor,$IDGrupo,$IDAsignatura,$IDAula,$idHora){
		$json = array();
		$query = 'UPDATE horariodetalle SET Dia = "'.$Dia.'", HoraInicio = "'.$HoraInicio.'", HoraFinal = "'.$HoraFinal.'", IDProfesor = "'.$IDProfesor.'", IDGrupo = "'.$IDGrupo.'", IDAsignatura = "'.$IDAsignatura.'", IDAula= "'.$IDAula.'" WHERE IDHorario = "'.$idHora.'"';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Hora actualizada!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}
	
}
?>