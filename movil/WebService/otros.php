<?php

require_once('./config/Conexion.php');

class otros {	
	private $db;
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function listarAulas(){
		$json = "";
		$query = 'SELECT * FROM aulas;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'aulas': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDAula'],'nombre' => $datos['NombreAula']));
			}else{
				$json .= json_encode(array('id' => $datos['IDAula'],'nombre' => $datos['NombreAula'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function listarDias(){
		$json = "";
		$query = 'SELECT * FROM dias;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'dias': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDDia'],'dia' => $datos['Dia']), JSON_UNESCAPED_UNICODE);
			}else{
				$json .= json_encode(array('id' => $datos['IDDia'],'dia' => $datos['Dia']), JSON_UNESCAPED_UNICODE).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}

	public function listarCarreras(){
		$json = "";
		$query = 'SELECT * FROM carreras;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'carreras': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDCarrera'],'NombreCarrera' => $datos['NombreCarrera']), JSON_UNESCAPED_UNICODE);
			}else{
				$json .= json_encode(array('id' => $datos['IDCarrera'],'NombreCarrera' => $datos['NombreCarrera'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}	
}
?>