<?php

require_once('./config/Conexion.php');

class curso {	
	private $db;
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function nuevoCurso($IdProfesor,$DescripcionCurso,$FechaHora,$Lugar){
		$json = array();
		$query = 'INSERT INTO cursos(IdProfesor, DescripcionCurso, FechaHora, Lugar) VALUES("'.$IdProfesor.'","'.$DescripcionCurso.'","'.$FechaHora.'","'.$Lugar.'")';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Curso programado!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}
	
	public function concluirCurso($id,$FechaConcluido){
		$json = array();
		$query = 'SELECT IDCurso FROM cursos WHERE IDCurso = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Este curso no existe!";
		}else{
			$query = 'UPDATE cursos SET Estatus = 1, FechaHoraConcluido = "'.$FechaConcluido.'"  WHERE IDCurso ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Curso fue concluido!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json);
	}
	
	public function modificarCurso($id,$DescripcionCurso,$FechaHora,$Lugar){
		$json = array();
		$query = 'SELECT IDCurso FROM cursos WHERE IDCurso = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Este curso no existe!";
		}else{
			$query = 'UPDATE cursos SET DescripcionCurso = "'.$DescripcionCurso.'", FechaHora = "'.$FechaHora.'", Lugar = "'.$Lugar.'" WHERE IDCurso ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Curso actualizado!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json, JSON_UNESCAPED_UNICODE);	
	}
	
	public function listarCursos($id){
		$json = "";
		$query = 'SELECT * FROM cursos WHERE Estatus != 1 AND IDProfesor = "'.$id.'" ORDER BY FechaHora;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'cursos': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDCurso'],'idProfesor' => $datos['IdProfesor'],'descripcion' => $datos['DescripcionCurso'],'fechainicio' => $datos['FechaHora'],'fechafin' => $datos['FechaHoraConcluido'],'Lugar' => $datos['Lugar']));
			}else{
				$json .= json_encode(array('id' => $datos['IDCurso'],'idProfesor' => $datos['IdProfesor'],'descripcion' => $datos['DescripcionCurso'],'fechainicio' => $datos['FechaHora'],'fechafin' => $datos['FechaHoraConcluido'],'Lugar' => $datos['Lugar'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
}
?>