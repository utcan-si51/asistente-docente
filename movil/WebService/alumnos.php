<?php

require_once('./config/Conexion.php');

class alumno {	
	private $db;
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function nuevoAlumno($nombre,$idGrupo){
		$json = array();
		$query = 'SELECT NombreAlumno FROM alumnos WHERE NombreAlumno = "'.$nombre.'" AND IDGrupo = "'.$idGrupo.'" AND Estatus != 0';
		$result = $this->db->totalRegistros($query);
		
		if($result == 1){
			$json['success'] = 2;
			$json['message'] = "Este alumno ya existe!";
		}else{
			$query = 'INSERT INTO alumnos(NombreAlumno, IDGrupo) VALUES("'.$nombre.'","'.$idGrupo.'")';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Alumno agregado!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
				$json['error'] = $query;
				$json['exists'] = $existe;
			}
		}
		
		return json_encode($json);
	}
	
	public function bajaAlumno($id){
		$json = array();
		$query = 'SELECT IDAlumno FROM alumnos WHERE IDAlumno = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Este alumno no existe!";
		}else{
			$query = 'UPDATE alumnos SET Estatus = 1 WHERE IDAlumno ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Alumno fue dado de baja!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json);
	}
	
	public function listarAlumnos(){
		$json = "";
		$query = 'SELECT alumnos.IDAlumno,alumnos.NombreAlumno,grupos.NombreGrupo FROM alumnos JOIN grupos ON alumnos.IDGrupo = grupos.IDGrupo WHERE alumnos.Estatus != 1;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'alumnos': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDAlumno'],'NombreAlumno' => $datos['NombreAlumno'],'NombreGrupo' => $datos['NombreGrupo']));
			}else{
				$json .= json_encode(array('id' => $datos['IDAlumno'],'NombreAlumno' => $datos['NombreAlumno'],'NombreGrupo' => $datos['NombreGrupo'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
}
?>