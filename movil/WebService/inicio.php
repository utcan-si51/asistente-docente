<?php

require_once('./config/Conexion.php');

class inicio {	
	private $db;

	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function listarHorarioHoy($id,$dia){
		$json = "";
		$query = 'SELECT m.IDHorario, grupos.NombreGrupo, asignaturas.NombreAsignatura, aulas.NombreAula, u1.Hora AS "HoraInicio", u2.Hora AS "HoraFin", m.Dia FROM horariodetalle m JOIN grupos ON m.IDGrupo = grupos.IDGrupo JOIN asignaturas ON m.IDAsignatura = asignaturas.IDAsignatura JOIN aulas ON m.IDAula = aulas.IDAula LEFT JOIN `horas` u1 ON (u1.IDHora = m.HoraInicio) LEFT JOIN `horas` u2 ON (u2.IDHora = m.HoraFinal) WHERE m.IDProfesor ="'.$id.'" AND m.Dia = "'.$dia.'" ORDER BY m.Dia ASC, u1.Hora ASC;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'horario': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDHorario'],'grupo' => $datos['NombreGrupo'],'asignatura' => $datos['NombreAsignatura'],'aula' => $datos['NombreAula'],'horainicio' => $datos['HoraInicio'],'horafin' => $datos['HoraFin'],'dia' => $datos['Dia']));
			}else{
				$json .= json_encode(array('id' => $datos['IDHorario'],'grupo' => $datos['NombreGrupo'],'asignatura' => $datos['NombreAsignatura'],'aula' => $datos['NombreAula'],'horainicio' => $datos['HoraInicio'],'horafin' => $datos['HoraFin'],'dia' => $datos['Dia'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function listarPendientesHoy($id,$dia){
		$json = "";
		$query = 'SELECT * FROM pendientes WHERE Estatus != 1 AND IdProfesor ="'.$id.'" AND DATE(FechaHora) = "'.$dia.'" ORDER BY FechaHora;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'pendientes': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDPendiente'],'idProfesor' => $datos['IdProfesor'],'titulo' => $datos['TituloPendiente'],'cuerpo' => $datos['CuerpoPendiente'],'fechainicio' => $datos['FechaHora'],'fechafin' => $datos['FechaHoraConcluido']));
			}else{
				$json .= json_encode(array('id' => $datos['IDPendiente'],'idProfesor' => $datos['IdProfesor'],'titulo' => $datos['TituloPendiente'],'cuerpo' => $datos['CuerpoPendiente'],'fechainicio' => $datos['FechaHora'],'fechafin' => $datos['FechaHoraConcluido'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function estadisticasAdmin(){
		$json = "";
		$query = 'SELECT (SELECT COUNT(IdProfesor) FROM profesores WHERE Estatus != 1 AND TipoProfesor != 2) AS CantProf, (SELECT COUNT(IDAsignatura) FROM asignaturas WHERE Estatus != 1) AS CantAsig, (SELECT COUNT(IDAlumno) FROM alumnos WHERE Estatus != 1) AS CantAlumnos, (SELECT COUNT(IDGrupo) FROM grupos WHERE Estatus != 1) AS CantGrupos, (SELECT COUNT(IDPendiente) FROM pendientes WHERE Estatus != 1) AS CantPendientesActivos, (SELECT COUNT(IDPendiente) FROM pendientes WHERE Estatus = 1) AS CantPendientesConcluidos;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'estadistica': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('profesores' => $datos['CantProf'],'asignaturas' => $datos['CantAsig'],'alumnos' => $datos['CantAlumnos'],'grupo' => $datos['CantAlumnos'],'pendientesActivos' => $datos['CantPendientesActivos'],'pendientesConcluidos' => $datos['CantPendientesConcluidos']));
			}else{
				$json .= json_encode(array('profesores' => $datos['CantProf'],'asignaturas' => $datos['CantAsig'],'alumnos' => $datos['CantAlumnos'],'grupo' => $datos['CantAlumnos'],'pendientesActivos' => $datos['CantPendientesActivos'],'pendientesConcluidos' => $datos['CantPendientesConcluidos'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}	
}
?>