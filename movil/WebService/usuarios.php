<?php

require_once('./config/Conexion.php');

class user {	
	private $db;
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function nuevoUsuario($matricula,$contra,$nombre,$apellido,$tipo){
		$json = array();
		$query = 'SELECT MatriculaProfesor FROM profesores WHERE MatriculaProfesor = "'.$matricula.'"';
		$result = $this->db->totalRegistros($query);
		
		if($result == 1){
			$json['success'] = 2;
			$json['message'] = "Esta matricula ya existe para un usuario agregado anteriormente";
		}else{
			$query = 'INSERT INTO profesores(MatriculaProfesor, Contra, NombreProfesor, ApellidoProfesor, TipoProfesor) VALUES("'.$matricula.'","'.$contra.'","'.$nombre.'","'.$apellido.'","'.$tipo.'")';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Profesor agregado!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
				$json['error'] = $query;
				$json['exists'] = $existe;
			}
		}
		
		return json_encode($json);
	}
	
	public function login($matricula, $contra){
		$json = array();
		$query = 'SELECT MatriculaProfesor FROM profesores WHERE MatriculaProfesor ="'.$matricula.'" AND Contra = "'.$contra.'"';
		$result = $this->db->totalRegistros($query);
		
		$value = false;
		if($result == 1){
			$value = true;
			
			$query = 'SELECT TipoProfesor,IdProfesor,Estatus,NombreProfesor,ApellidoProfesor,MatriculaProfesor FROM profesores WHERE MatriculaProfesor ="'.$matricula.'"';
			$result = $this->db->traerValores($query);
			
			if($result['Estatus'] == 1){
				$value = false;
				$json['success'] = 0;
				$json['message'] = "Este usuario esta dado de baja!";
			}else{
				$json['success'] = 1;
				$json['message'] = "Login correcto!";
				$json['tipo'] = $result['TipoProfesor'];
				$json['id'] = $result['IdProfesor'];
				$json['nombre'] = $result['NombreProfesor'];
				$json['apellido'] = $result['ApellidoProfesor'];
				$json['matricula'] = $result['MatriculaProfesor'];
			}

		} else {
			$value = false;
			$json['success'] = 0;
			$json['message'] = "Datos incorrectos!";
		}
		
		return json_encode($json);
	}
	
	public function bajaUsuario($id){
		$json = array();
		$query = 'SELECT IdProfesor FROM profesores WHERE IdProfesor = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Esta matricula no existe!";
		}else{
			$query = 'UPDATE profesores SET Estatus = 1 WHERE IdProfesor ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Profesor fue dado de baja!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json);
	}
	
	public function updateContraUsuario($id,$contra){
		$json = array();
		$query = 'SELECT IdProfesor FROM profesores WHERE IdProfesor = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Este usuario no existe!";
		}else{
			$query = 'UPDATE profesores SET Contra = "'.$contra.'" WHERE IdProfesor ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Contraseña actualizada!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json, JSON_UNESCAPED_UNICODE);	
	}
	
	public function listarUsuarios(){
		$json = "";
		$query = 'SELECT * FROM profesores WHERE TipoProfesor != 2 AND Estatus != 1;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'usuarios': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IdProfesor'],'matricula' => $datos['MatriculaProfesor'],'nombre' => $datos['NombreProfesor'],'apellido' => $datos['ApellidoProfesor'],'tipo' => $datos['TipoProfesor']));
			}else{
				$json .= json_encode(array('id' => $datos['IdProfesor'],'matricula' => $datos['MatriculaProfesor'],'nombre' => $datos['NombreProfesor'],'apellido' => $datos['ApellidoProfesor'],'tipo' => $datos['TipoProfesor'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
}
?>