<?php
require('./fpdf/mysql_table.php');

class PDF extends PDF_MySQL_Table{
	function Header(){
		// Title
		$this->SetFont('Arial','',18);
		$this->Cell(0,6,'Profesores Registrados',0,1,'C');
		$this->Ln(10);
		// Ensure table header is printed
		parent::Header();
	}
}

class reportes {	
	
	public function generarProfesores(){
		
		$json = array();

		// Connect to database
		$link = mysqli_connect(SERVIDOR,USUARIO,CONTRASENIA,BD);

		$pdf = new PDF();
		$pdf->AddPage("L");
		// First table: output all columns
		$prop = array('HeaderColor'=>array(52,58,64),
					'color1'=>array(102,193,123),
					'padding'=>2);
		$pdf->Table($link,'select MatriculaProfesor AS Matricula, NombreProfesor AS Nombres, ApellidoProfesor AS Apellidos from profesores WHERE TipoProfesor != 2 AND Estatus != 1', $prop);
		$pdf->Output("./pdf/profesores.pdf", "F");
		
		$json['success'] = 1;
		$json['url'] = "https://docs.google.com/gview?embedded=true&url=https://jpapke.tk/webservice/pdf/profesores.pdf";
		
		return json_encode($json, JSON_UNESCAPED_SLASHES);
	}
}
?>