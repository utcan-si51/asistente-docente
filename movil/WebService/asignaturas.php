<?php

require_once('./config/Conexion.php');

class asignatura {	
	private $db;
	private $table = "profesores";
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function listarAsignaturas($id){
		$json = "";
		$query = 'SELECT * FROM asignaturas WHERE IDProfesor ="'.$id.'" AND Estatus != 1;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'asignaturas': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDAsignatura'],'nombre' => $datos['NombreAsignatura']));
			}else{
				$json .= json_encode(array('id' => $datos['IDAsignatura'],'nombre' => $datos['NombreAsignatura'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function listarUnidad($id){
		$json = "";
		$query = 'SELECT temarios.*,SUBSTRING(REPLACE(temarios.Contenido, "\r\n", ""), 1, 43) AS contenidoCorto,asignaturas.NombreAsignatura FROM temarios JOIN asignaturas ON temarios.IDAsignatura = asignaturas.IDAsignatura WHERE temarios.IDAsignatura ="'.$id.'" AND temarios.Estatus != 1;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'unidades': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDTemario'],'idAsig' => $datos['IDAsignatura'],'unidad' => $datos['Unidad'],'contenido' => $datos['Contenido'],'contenidoCorto' => $datos['contenidoCorto']." ...",'nombreAsignatura' => $datos['NombreAsignatura']));
			}else{
				$json .= json_encode(array('id' => $datos['IDTemario'],'idAsig' => $datos['IDAsignatura'],'unidad' => $datos['Unidad'],'contenido' => $datos['Contenido'],'contenidoCorto' => $datos['contenidoCorto']." ...",'nombreAsignatura' => $datos['NombreAsignatura'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	public function datosUnidad($id){
		$json = "";
		$query = 'SELECT temarios.*,SUBSTRING(REPLACE(temarios.Contenido, "\r\n", ""), 1, 43) AS contenidoCorto,asignaturas.NombreAsignatura FROM temarios JOIN asignaturas ON temarios.IDAsignatura = asignaturas.IDAsignatura WHERE temarios.IDTemario ="'.$id.'" AND temarios.Estatus != 1;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'unidades': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDTemario'],'idAsig' => $datos['IDAsignatura'],'unidad' => $datos['Unidad'],'contenido' => $datos['Contenido'],'contenidoCorto' => $datos['contenidoCorto']." ...",'nombreAsignatura' => $datos['NombreAsignatura']));
			}else{
				$json .= json_encode(array('id' => $datos['IDTemario'],'idAsig' => $datos['IDAsignatura'],'unidad' => $datos['Unidad'],'contenido' => $datos['Contenido'],'contenidoCorto' => $datos['contenidoCorto']." ...",'nombreAsignatura' => $datos['NombreAsignatura'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function agregarAsignatura($id,$nombre){
		$json = array();
		$query = 'INSERT INTO asignaturas(NombreAsignatura, IDProfesor) VALUES ("'.$nombre.'","'.$id.'")';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Asignatura agregada!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}
	
	public function agregarUnidad($id,$nombre,$contenido){
		$json = array();
		$query = 'INSERT INTO temarios(IDAsignatura, Unidad, Contenido) VALUES ("'.$id.'","'.$nombre.'","'.$contenido.'")';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Unidad agregada!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}

	public function eliminarUnidad($id){
		$json = array();
		$query = 'UPDATE temarios SET Estatus = 1 WHERE IDTemario = "'.$id.'"';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Esta unidad fue dado de baja!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}

	public function eliminarAsignatura($id){
		$json = array();
		$query = 'UPDATE asignaturas SET Estatus = 1 WHERE IDAsignatura = "'.$id.'"';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Esta asignatura fue dado de baja!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}	
}
?>