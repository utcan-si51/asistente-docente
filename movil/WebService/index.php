<?php

require_once('./inicio.php');
require_once('./usuarios.php');
require_once('./grupos.php');
require_once('./alumnos.php');
require_once('./visitas.php');
require_once('./acciones.php');
require_once('./cursos.php');
require_once('./agenda.php');
require_once('./horario.php');
require_once('./asignaturas.php');
require_once('./otros.php');
require_once('./reportes.php');

$inicio = new inicio();
$usuario = new user();
$grupo = new grupo();
$alumno = new alumno();
$visita = new visita();
$accion = new accion();
$curso = new curso();
$agenda = new agenda();
$horario = new horario();
$asignatura = new asignatura();
$otros = new otros();
$reportes = new reportes();

if(isset($_GET['opcion'])){
	$opcion = $_GET['opcion'];

	if($opcion == 1){
		
		if(isset($_POST['matricula']) && isset($_POST['contra']) ){
			$matricula = $_POST['matricula'];
			$contra = $_POST['contra'];
			
			$resultLogin = $usuario->login($matricula,$contra);
			echo $resultLogin;
		}
		
	}else if($opcion == 2){
		
		if(isset($_POST['matricula']) && isset($_POST['contra']) && isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['tipo'])){
			$matricula = $_POST['matricula'];
			$contra = $_POST['contra'];
			$nombre = $_POST['nombre'];
			$apellido = $_POST['apellido'];
			$tipo = $_POST['tipo'];
			
			$resultNuevo = $usuario->nuevoUsuario($matricula,$contra,$nombre,$apellido,$tipo);
			echo $resultNuevo;
		}
		
	}else if($opcion == 3){

		if(isset($_POST['id'])){
			$id = $_POST['id'];
				
			$resultBaja = $usuario->bajaUsuario($id);
			echo $resultBaja;
		}
		
	}else if($opcion == 4){
		
		$resultListar = $usuario->listarUsuarios();
		echo $resultListar;
		
	}else if($opcion == 5){
		if(isset($_POST['id']) && isset($_POST['contra'])){
			$id = $_POST['id'];
			$contra = $_POST['contra'];
			
			$resultActualizarContra = $usuario->updateContraUsuario($id,$contra);
			echo $resultActualizarContra;
		}
	}else if($opcion == 6){
		
		if(isset($_POST['nombre']) && isset($_POST['carrera'])){
			$NombreGrupo = $_POST['nombre'];
			$IDCarrera = $_POST['carrera'];
			
			$resultNuevo = $grupo->nuevoGrupo($NombreGrupo,$IDCarrera);
			echo $resultNuevo;
		}
		
	}else if($opcion == 7){

		if(isset($_POST['id'])){
			$id = $_POST['id'];
				
			$resultBaja = $grupo->bajaGrupo($id);
			echo $resultBaja;
		}
		
	}else if($opcion == 8){
			$resultListar = $grupo->listarGrupos();
			echo $resultListar;
		
	}else if($opcion == 9){
		
		if(isset($_POST['nombre']) && isset($_POST['grupo'])){
			$NombreAlumno = $_POST['nombre'];
			$IDGrupo = $_POST['grupo'];
			
			$resultNuevo = $alumno->nuevoAlumno($NombreAlumno,$IDGrupo);
			echo $resultNuevo;
		}
		
	}else if($opcion == 10){

		if(isset($_POST['id'])){
			$id = $_POST['id'];
				
			$resultBaja = $alumno->bajaAlumno($id);
			echo $resultBaja;
		}
		
	}else if($opcion == 11){
		
		$resultListar = $alumno->listarAlumnos();
		echo $resultListar;
		
	}else if($opcion == 12){
		
		if(isset($_POST['nombre']) && isset($_POST['descripcion']) && isset($_POST['fechainicio']) && isset($_POST['profesor']) && isset($_POST['grupo'])){
			$nombre = $_POST['nombre'];
			$descripcion = $_POST['descripcion'];
			$fechainicio = $_POST['fechainicio'];
			$idProfesor = $_POST['profesor'];
			$idGrupo = $_POST['grupo'];
			
			$resultNuevo = $visita->nuevaVisita($nombre,$descripcion,$fechainicio,$idProfesor,$idGrupo);
			echo $resultNuevo;
		}
		
	}else if($opcion == 13){

		if(isset($_POST['id']) && isset($_POST['fechafin'])){
			$id = $_POST['id'];
			$fechafin = $_POST['fechafin'];
				
			$resultConcluir = $visita->concluirVisita($id,$fechafin);
			echo $resultConcluir;
		}
		
	}else if($opcion == 14){
		
		if(isset($_POST['id']) && isset($_POST['nombre']) && isset($_POST['descripcion']) && isset($_POST['fechainicio']) && isset($_POST['grupo'])){
			$idVisita = $_POST['id'];
			$nombre = $_POST['nombre'];
			$descripcion = $_POST['descripcion'];
			$fechainicio = $_POST['fechainicio'];
			$idGrupo = $_POST['grupo'];
			
			$resultNuevo = $visita->modificarVisita($idVisita,$nombre,$descripcion,$fechainicio,$idGrupo);
			echo $resultNuevo;
		}
		
	}else if($opcion == 15){
		if(isset($_POST['id'])){
			$idUsuario = $_POST['id'];
			$resultListar = $visita->listarVisitas($idUsuario);
			echo $resultListar;
		}
		
	}else if($opcion == 16){
		
		if(isset($_POST['actividad']) && isset($_POST['alumno']) && isset($_POST['fechahora']) && isset($_POST['profesor'])){
			$actividad = $_POST['actividad'];
			$IDAlumno = $_POST['alumno'];
			$fechainicio = $_POST['fechahora'];
			$idProfesor = $_POST['profesor'];
			
			$resultNuevo = $accion->nuevaAccion($actividad,$IDAlumno,$fechainicio,$idProfesor);
			echo $resultNuevo;
		}
		
	}else if($opcion == 17){

		if(isset($_POST['id']) && isset($_POST['fechafin']) && isset($_POST['resultado'])){
			$id = $_POST['id'];
			$fechafin = $_POST['fechafin'];
			$resultado = $_POST['resultado'];
				
			$resultConcluir = $accion->concluirAccion($id,$fechafin,$resultado);
			echo $resultConcluir;
		}
		
	}else if($opcion == 18){
		
		if(isset($_POST['id']) && isset($_POST['actividad']) && isset($_POST['alumno']) && isset($_POST['fechahora'])){
			$idAccion = $_POST['id'];
			$actividad = $_POST['actividad'];
			$IDAlumno = $_POST['alumno'];
			$fechainicio = $_POST['fechahora'];
			
			$resultNuevo = $accion->modificarAccion($idAccion,$actividad,$IDAlumno,$fechainicio);
			echo $resultNuevo;
		}
		
	}else if($opcion == 19){
		
		if(isset($_POST['id'])){
			$idUsuario = $_POST['id'];
			
			$resultListar = $accion->listarAcciones($idUsuario);
			echo $resultListar;
		}

	}else if($opcion == 20){
		
		if(isset($_POST['IdProfesor']) && isset($_POST['descripcion']) && isset($_POST['fechahora']) && isset($_POST['lugar'])){
			$IdProfesor = $_POST['IdProfesor'];
			$descripcion = $_POST['descripcion'];
			$fechainicio = $_POST['fechahora'];
			$lugar = $_POST['lugar'];
			
			$resultNuevo = $curso->nuevoCurso($IdProfesor,$descripcion,$fechainicio,$lugar);
			echo $resultNuevo;
		}
		
	}else if($opcion == 21){

		if(isset($_POST['id']) && isset($_POST['fechafin'])){
			$id = $_POST['id'];
			$fechafin = $_POST['fechafin'];
				
			$resultConcluir = $curso->concluirCurso($id,$fechafin);
			echo $resultConcluir;
		}
		
	}else if($opcion == 22){
		
		if(isset($_POST['id']) && isset($_POST['descripcion']) && isset($_POST['fechahora']) && isset($_POST['lugar'])){
			$idCurso = $_POST['id'];
			$descripcion = $_POST['descripcion'];
			$fechainicio = $_POST['fechahora'];
			$lugar = $_POST['lugar'];
			
			$resultNuevo = $curso->modificarCurso($idCurso,$descripcion,$fechainicio,$lugar);
			echo $resultNuevo;
		}
		
	}else if($opcion == 23){
		
		if(isset($_POST['id'])){
			$id = $_POST['id'];
		
			$resultListar = $curso->listarCursos($id);
			echo $resultListar;
		}
		
	}else if($opcion == 24){
		
		if(isset($_POST['IdProfesor']) && isset($_POST['titulo']) && isset($_POST['fechahora']) && isset($_POST['cuerpo'])){
			$IdProfesor = $_POST['IdProfesor'];
			$Titulo = $_POST['titulo'];
			$fechainicio = $_POST['fechahora'];
			$Cuerpo = $_POST['cuerpo'];
			
			$resultNuevo = $agenda->nuevoPendiente($IdProfesor,$Titulo,$Cuerpo,$fechainicio);
			echo $resultNuevo;
		}
		
	}else if($opcion == 25){

		if(isset($_POST['id']) && isset($_POST['fechafin'])){
			$id = $_POST['id'];
			$fechafin = $_POST['fechafin'];
				
			$resultConcluir = $agenda->concluirPendiente($id,$fechafin);
			echo $resultConcluir;
		}
		
	}else if($opcion == 26){
		
		if(isset($_POST['id']) && isset($_POST['IdProfesor']) && isset($_POST['titulo']) && isset($_POST['fechahora']) && isset($_POST['cuerpo'])){
			$idCurso = $_POST['id'];
			$IdProfesor = $_POST['IdProfesor'];
			$Titulo = $_POST['titulo'];
			$fechainicio = $_POST['fechahora'];
			$Cuerpo = $_POST['cuerpo'];
			
			$resultNuevo = $agenda->modificarPendiente($idCurso,$IdProfesor,$Titulo,$Cuerpo,$fechainicio);
			echo $resultNuevo;
		}
		
	}else if($opcion == 27){
		if(isset($_POST['id'])){
			$idUsuario = $_POST['id'];
			
			$resultListar = $agenda->listarPendientes($idUsuario);
			echo $resultListar;
		}
		
	}else if($opcion == 28){
		if(isset($_POST['id'])){
			$idUsuario = $_POST['id'];
			
			$resultListar = $horario->listarHorario($idUsuario);
			echo $resultListar;
		}
		
	}else if($opcion == 29){
		if(isset($_POST['id']) && isset($_POST['dia'])){
			$idUsuario = $_POST['id'];
			$dia = $_POST['dia'];
			
			$resultListar = $horario->listarHorasInicio($idUsuario,$dia);
			echo $resultListar;
		}
		
	}else if($opcion == 30){
		if(isset($_POST['id'])){
			$idUsuario = $_POST['id'];
			
			$resultListar = $asignatura->listarAsignaturas($idUsuario);
			echo $resultListar;
		}
		
	}else if($opcion == 31){
		$resultListar = $otros->listarAulas();
		echo $resultListar;
	}else if($opcion == 32){
		$resultListar = $otros->listarDias();
		echo $resultListar;
	}else if($opcion == 33){
		if(isset($_POST['id']) && isset($_POST['dia']) && isset($_POST['hora'])){
			$idUsuario = $_POST['id'];
			$dia = $_POST['dia'];
			$hora = $_POST['hora'];
			
			$resultListar = $horario->listarHorasFin($idUsuario,$dia,$hora);
			echo $resultListar;
		}
		
	}else if($opcion == 34){
		if(isset($_POST['dia']) && isset($_POST['horainicio']) && isset($_POST['horafinal']) && isset($_POST['id']) && isset($_POST['grupo']) && isset($_POST['asignatura']) && isset($_POST['aula'])){
			$Dia = $_POST['dia'];
			$HoraInicio = $_POST['horainicio'];
			$HoraFinal = $_POST['horafinal'];
			$IDProfesor = $_POST['id'];
			$IDGrupo = $_POST['grupo'];
			$IDAsignatura = $_POST['asignatura'];
			$IDAula = $_POST['aula'];
			
			$resultListar = $horario->agregarHorario($Dia,$HoraInicio,$HoraFinal,$IDProfesor,$IDGrupo,$IDAsignatura,$IDAula);
			echo $resultListar;
		}
		
	}else if($opcion == 35){
		if(isset($_POST['id'])){
			$idHora = $_POST['id'];
			
			$resultListar = $horario->bajaHora($idHora);
			echo $resultListar;
		}
		
	}else if($opcion == 36){
		if(isset($_POST['id'])){
			$idHora = $_POST['id'];
			
			$resultListar = $horario->traerHora($idHora);
			echo $resultListar;
		}
		
	}else if($opcion == 37){
		if(isset($_POST['id']) && isset($_POST['dia']) && isset($_POST['horamod'])){
			$idUsuario = $_POST['id'];
			$dia = $_POST['dia'];
			$horamod = $_POST['horamod'];
			
			$resultListar = $horario->listarHorasInicioMod($idUsuario,$dia,$horamod);
			echo $resultListar;
		}
		
	}else if($opcion == 38){
		if(isset($_POST['id']) && isset($_POST['dia']) && isset($_POST['hora']) && isset($_POST['horamod'])){
			$idUsuario = $_POST['id'];
			$dia = $_POST['dia'];
			$hora = $_POST['hora'];
			$horamod = $_POST['horamod'];
			
			$resultListar = $horario->listarHorasFinMod($idUsuario,$dia,$hora,$horamod);
			echo $resultListar;
		}
		
	}else if($opcion == 39){
		if(isset($_POST['dia']) && isset($_POST['horainicio']) && isset($_POST['horafinal']) && isset($_POST['id']) && isset($_POST['grupo']) && isset($_POST['asignatura']) && isset($_POST['aula']) && isset($_POST['idHora'])){
			$Dia = $_POST['dia'];
			$HoraInicio = $_POST['horainicio'];
			$HoraFinal = $_POST['horafinal'];
			$IDProfesor = $_POST['id'];
			$IDGrupo = $_POST['grupo'];
			$IDAsignatura = $_POST['asignatura'];
			$IDAula = $_POST['aula'];
			$idHora = $_POST['idHora'];
			
			$resultListar = $horario->actualizarHorario($Dia,$HoraInicio,$HoraFinal,$IDProfesor,$IDGrupo,$IDAsignatura,$IDAula,$idHora);
			echo $resultListar;
		}
		
	}else if($opcion == 40){
		if(isset($_POST['id'])){
			$idAsig= $_POST['id'];
			
			$resultListar = $asignatura->listarUnidad($idAsig);
			echo $resultListar;
		}
		
	}else if($opcion == 41){
		if(isset($_POST['id'])){
			$idAsig= $_POST['id'];
			
			$resultListar = $asignatura->datosUnidad($idAsig);
			echo $resultListar;
		}
		
	}else if($opcion == 42){
		if(isset($_POST['id']) && isset($_POST['nombre']) && isset($_POST['contenido'])){
			$id = $_POST['id'];
			$nombre = $_POST['nombre'];
			$contenido = $_POST['contenido'];
			
			$resultListar = $asignatura->agregarUnidad($id,$nombre,$contenido);
			echo $resultListar;
		}
		
	}else if($opcion == 43){
		if(isset($_POST['id'])){
			$idAsig= $_POST['id'];
			
			$resultEliminar = $asignatura->eliminarUnidad($idAsig);
			echo $resultEliminar;
		}
		
	}else if($opcion == 44){
		if(isset($_POST['id'])){
			$idAsig= $_POST['id'];
			
			$resultEliminar = $asignatura->eliminarAsignatura($idAsig);
			echo $resultEliminar;
		}
		
	}else if($opcion == 45){
		if(isset($_POST['id']) && isset($_POST['dia'])){
			$id= $_POST['id'];
			$dia= $_POST['dia'];
			
			$resultListar = $inicio->listarHorarioHoy($id,$dia);
			echo $resultListar;
		}
		
	}else if($opcion == 46){
		if(isset($_POST['id']) && isset($_POST['dia'])){
			$id= $_POST['id'];
			$dia= $_POST['dia'];
			
			$resultListar = $inicio->listarPendientesHoy($id,$dia);
			echo $resultListar;
		}
		
	}else if($opcion == 47){
		$resultListar = $inicio->estadisticasAdmin();
		echo $resultListar;
	}else if($opcion == 48){
		$resultListar = $otros->listarCarreras();
		echo $resultListar;
	}else if($opcion == 49){
		if(isset($_POST['nombre']) && isset($_POST['id'])){
			$idUsuario= $_POST['id'];
			$nombre= $_POST['nombre'];
			
			$resultAgregar = $asignatura->agregarAsignatura($idUsuario,$nombre);
			echo $resultAgregar;
		}
		
	}else if($opcion == 50){
		$resultado = $reportes->generarProfesores();
		echo $resultado;
	}
}
?>