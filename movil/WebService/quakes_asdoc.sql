-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 25, 2018 at 02:28 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quakes_asdoc`
--

-- --------------------------------------------------------

--
-- Table structure for table `acciones_de_mejora`
--

CREATE TABLE `acciones_de_mejora` (
  `IDActMejora` int(5) NOT NULL,
  `Actividad` text NOT NULL,
  `IDAlumno` int(5) NOT NULL,
  `FechaHora` datetime NOT NULL,
  `FechaHoraConcluido` datetime DEFAULT NULL,
  `Calificacion` varchar(50) NOT NULL DEFAULT 'Sin calificacion',
  `Estatus` tinyint(4) NOT NULL DEFAULT '0',
  `IDProfesor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acciones_de_mejora`
--

INSERT INTO `acciones_de_mejora` (`IDActMejora`, `Actividad`, `IDAlumno`, `FechaHora`, `FechaHoraConcluido`, `Calificacion`, `Estatus`, `IDProfesor`) VALUES
(1, 'Rehacer Documento de Factibilidad', 1, '2018-01-21 12:01:00', '2018-01-31 13:26:25', '80', 1, 1),
(2, 'Test2', 2, '2018-01-28 01:01:00', '2018-01-21 16:25:47', '80', 1, 1),
(3, 'Realizar un Examen', 4, '2018-02-14 00:00:00', NULL, 'Sin calificacion', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `alumnos`
--

CREATE TABLE `alumnos` (
  `IDAlumno` int(5) NOT NULL,
  `NombreAlumno` varchar(50) NOT NULL,
  `IDGrupo` int(5) NOT NULL,
  `IDProfesor` int(11) NOT NULL,
  `Estatus` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumnos`
--

INSERT INTO `alumnos` (`IDAlumno`, `NombreAlumno`, `IDGrupo`, `IDProfesor`, `Estatus`) VALUES
(1, 'Josue Ismael', 1, 1, 0),
(2, 'Marc Julian Papke Vosselmann', 1, 1, 0),
(4, 'Juan Perez', 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `asignaturas`
--

CREATE TABLE `asignaturas` (
  `IDAsignatura` int(5) NOT NULL,
  `NombreAsignatura` varchar(50) NOT NULL,
  `IDProfesor` int(11) NOT NULL,
  `Estatus` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asignaturas`
--

INSERT INTO `asignaturas` (`IDAsignatura`, `NombreAsignatura`, `IDProfesor`, `Estatus`) VALUES
(1, 'Ingenieria de Software', 1, 0),
(2, 'Desarrollo de Software', 1, 0),
(3, 'Expresion Oral II', 1, 0),
(4, 'Test', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `aulas`
--

CREATE TABLE `aulas` (
  `IDAula` int(5) NOT NULL,
  `NombreAula` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aulas`
--

INSERT INTO `aulas` (`IDAula`, `NombreAula`) VALUES
(1, 'H122'),
(2, 'H123');

-- --------------------------------------------------------

--
-- Table structure for table `carreras`
--

CREATE TABLE `carreras` (
  `IDCarrera` int(5) NOT NULL,
  `NombreCarrera` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carreras`
--

INSERT INTO `carreras` (`IDCarrera`, `NombreCarrera`) VALUES
(1, 'Sistemas Informaticos'),
(2, 'Redes y telecomunicaciones'),
(3, 'Mantenimiento Industrial'),
(4, 'Administracion'),
(5, 'Contaduría'),
(6, 'Desarollo de Negocios'),
(7, 'Gastronomia'),
(8, 'Turismo - Hoteleria'),
(9, 'Turismo - DPA'),
(10, 'Terapia Física');

-- --------------------------------------------------------

--
-- Table structure for table `cursos`
--

CREATE TABLE `cursos` (
  `IDCurso` int(5) NOT NULL,
  `IdProfesor` int(5) NOT NULL,
  `DescripcionCurso` text,
  `FechaHora` datetime NOT NULL,
  `FechaHoraConcluido` datetime DEFAULT NULL,
  `Lugar` varchar(100) NOT NULL DEFAULT 'Sin lugar',
  `Estatus` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cursos`
--

INSERT INTO `cursos` (`IDCurso`, `IdProfesor`, `DescripcionCurso`, `FechaHora`, `FechaHoraConcluido`, `Lugar`, `Estatus`) VALUES
(1, 1, 'Curso de 5 dias sobre la educacion.', '2018-01-20 00:00:00', NULL, 'Sin lugar', 0),
(2, 1, 'test', '2018-01-19 02:01:00', '2018-01-21 14:04:27', 'Centro de Convenciones', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dias`
--

CREATE TABLE `dias` (
  `IDDia` int(5) NOT NULL,
  `Dia` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dias`
--

INSERT INTO `dias` (`IDDia`, `Dia`) VALUES
(1, 'Lunes'),
(2, 'Martes'),
(3, 'Miércoles'),
(4, 'Jueves'),
(5, 'Viernes');

-- --------------------------------------------------------

--
-- Table structure for table `grupos`
--

CREATE TABLE `grupos` (
  `IDGrupo` int(5) NOT NULL,
  `NombreGrupo` varchar(5) NOT NULL DEFAULT 'NA',
  `IDCarrera` int(5) NOT NULL,
  `Estatus` tinyint(4) NOT NULL DEFAULT '0',
  `IDProfesor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupos`
--

INSERT INTO `grupos` (`IDGrupo`, `NombreGrupo`, `IDCarrera`, `Estatus`, `IDProfesor`) VALUES
(1, 'SI51', 1, 0, 1),
(2, 'SI52', 1, 0, 1),
(5, 'Test', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `horariodetalle`
--

CREATE TABLE `horariodetalle` (
  `IDHorario` int(5) NOT NULL,
  `Dia` int(5) NOT NULL,
  `HoraInicio` int(5) NOT NULL,
  `HoraFinal` int(5) NOT NULL,
  `IDProfesor` int(5) NOT NULL,
  `IDGrupo` int(5) NOT NULL,
  `IDAsignatura` int(5) NOT NULL,
  `IDAula` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `horariodetalle`
--

INSERT INTO `horariodetalle` (`IDHorario`, `Dia`, `HoraInicio`, `HoraFinal`, `IDProfesor`, `IDGrupo`, `IDAsignatura`, `IDAula`) VALUES
(1, 3, 1, 3, 1, 1, 1, 1),
(2, 1, 6, 8, 1, 1, 3, 2),
(3, 3, 6, 7, 1, 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `horas`
--

CREATE TABLE `horas` (
  `IDHora` int(5) NOT NULL,
  `Hora` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `horas`
--

INSERT INTO `horas` (`IDHora`, `Hora`) VALUES
(1, '07:00:00'),
(2, '07:50:00'),
(3, '08:40:00'),
(4, '09:00:00'),
(5, '09:50:00'),
(6, '10:40:00'),
(7, '11:30:00'),
(8, '12:20:00'),
(9, '12:40:00'),
(10, '13:30:00'),
(11, '14:20:00'),
(12, '15:10:00'),
(13, '16:00:00'),
(14, '16:40:00'),
(15, '17:30:00'),
(16, '18:20:00'),
(17, '19:10:00'),
(18, '20:00:00'),
(19, '20:50:00'),
(20, '21:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `pendientes`
--

CREATE TABLE `pendientes` (
  `IDPendiente` int(5) NOT NULL,
  `IdProfesor` int(5) NOT NULL,
  `TituloPendiente` varchar(50) NOT NULL,
  `CuerpoPendiente` varchar(1000) DEFAULT 'Sin descripcion',
  `FechaHora` datetime NOT NULL,
  `FechaHoraConcluido` datetime DEFAULT NULL,
  `Estatus` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendientes`
--

INSERT INTO `pendientes` (`IDPendiente`, `IdProfesor`, `TituloPendiente`, `CuerpoPendiente`, `FechaHora`, `FechaHoraConcluido`, `Estatus`) VALUES
(1, 1, 'Test', 'Sin descripcion', '2018-01-20 00:00:00', '2018-01-31 13:24:44', 1),
(2, 1, 'Test', 'Test', '2018-01-21 01:01:00', '2018-01-21 17:25:05', 1),
(3, 1, 'Test5', 'Test5', '2018-01-07 10:01:00', '2018-01-21 17:24:55', 1),
(4, 2, 'Hacer esto y esto', 'IMPORTANTE !!!!!!!', '2018-01-22 10:10:00', '2018-01-31 13:20:30', 1),
(5, 1, 'Hacer esto', ' y esto 2', '2018-01-31 01:01:00', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `profesores`
--

CREATE TABLE `profesores` (
  `IdProfesor` int(5) NOT NULL,
  `MatriculaProfesor` varchar(20) NOT NULL,
  `Contra` varchar(45) NOT NULL,
  `NombreProfesor` varchar(50) NOT NULL,
  `ApellidoProfesor` varchar(50) NOT NULL,
  `TipoProfesor` int(5) DEFAULT '0',
  `Estatus` tinyint(4) NOT NULL DEFAULT '0',
  `Imagen` varchar(100) NOT NULL DEFAULT '/icons/a4.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `profesores`
--

INSERT INTO `profesores` (`IdProfesor`, `MatriculaProfesor`, `Contra`, `NombreProfesor`, `ApellidoProfesor`, `TipoProfesor`, `Estatus`, `Imagen`) VALUES
(0, 'Admin', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin', 'Admin', 2, 0, '/icons/a4.jpg'),
(1, '2010', '51eac6b471a284d3341d8c0c63d0f1a286262a18', 'Mayra Guadalupe', 'Fuentes Sosa', 1, 0, '/icons/a4.jpg'),
(2, '2011', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Test Test', 'Test Test', 0, 0, '/icons/a4.jpg'),
(3, '2012', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Test', 'Test', 1, 0, '/icons/a4.jpg'),
(4, '2013', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Test2', 'Test2', 0, 0, '/icons/a4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `temarios`
--

CREATE TABLE `temarios` (
  `IDTemario` int(5) NOT NULL,
  `IDAsignatura` int(5) NOT NULL,
  `Unidad` varchar(50) NOT NULL,
  `Contenido` text NOT NULL,
  `Estatus` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temarios`
--

INSERT INTO `temarios` (`IDTemario`, `IDAsignatura`, `Unidad`, `Contenido`, `Estatus`) VALUES
(1, 1, 'Unidad 1', 'En esta unidad el estudiante aprende sobre ....\r\n\r\n- Lorem Ipsum\r\n-Y esto\r\n\r\nLos entregables de esta unidad son:\r\n-Protafolio de evidencias\r\n-Proyecto sobre ciclos.', 0),
(2, 4, 'Unidad 1', 'En esta unidad haremos esto y esto 2.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `visitas`
--

CREATE TABLE `visitas` (
  `IDVisita` int(5) NOT NULL,
  `NombreVisita` varchar(50) NOT NULL,
  `DesripcionVisita` text NOT NULL,
  `Fecha` datetime NOT NULL,
  `FechaHoraConcluido` datetime DEFAULT NULL,
  `IDProfesor` int(11) NOT NULL,
  `Estatus` tinyint(4) NOT NULL DEFAULT '0',
  `Grupo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitas`
--

INSERT INTO `visitas` (`IDVisita`, `NombreVisita`, `DesripcionVisita`, `Fecha`, `FechaHoraConcluido`, `IDProfesor`, `Estatus`, `Grupo`) VALUES
(1, 'Visita a Empresa Y', 'Los Alumnos aprenderan sobre como hacer esto duranta el recorrido por la empresa Y. ', '2018-01-20 12:01:00', NULL, 1, 0, 1),
(2, 'Test', 'Test', '2018-01-28 01:01:00', '2018-01-21 16:04:44', 1, 1, 2),
(3, 'Test2', 'Test23', '2018-01-28 01:01:00', '2018-01-21 14:35:46', 1, 1, 1),
(4, 'Test3', 'Test4', '2018-01-14 13:01:00', '2018-01-21 14:35:43', 1, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acciones_de_mejora`
--
ALTER TABLE `acciones_de_mejora`
  ADD PRIMARY KEY (`IDActMejora`),
  ADD KEY `IDAlumno` (`IDAlumno`),
  ADD KEY `IDProfesor` (`IDProfesor`);

--
-- Indexes for table `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`IDAlumno`),
  ADD KEY `IDGrupo` (`IDGrupo`),
  ADD KEY `IDProfesor` (`IDProfesor`);

--
-- Indexes for table `asignaturas`
--
ALTER TABLE `asignaturas`
  ADD PRIMARY KEY (`IDAsignatura`),
  ADD KEY `IDProfesor` (`IDProfesor`);

--
-- Indexes for table `aulas`
--
ALTER TABLE `aulas`
  ADD PRIMARY KEY (`IDAula`);

--
-- Indexes for table `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`IDCarrera`);

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`IDCurso`),
  ADD KEY `IdProfesor` (`IdProfesor`);

--
-- Indexes for table `dias`
--
ALTER TABLE `dias`
  ADD PRIMARY KEY (`IDDia`);

--
-- Indexes for table `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`IDGrupo`),
  ADD KEY `IDCarrera` (`IDCarrera`),
  ADD KEY `IDProfesor` (`IDProfesor`);

--
-- Indexes for table `horariodetalle`
--
ALTER TABLE `horariodetalle`
  ADD PRIMARY KEY (`IDHorario`),
  ADD KEY `Dia` (`Dia`),
  ADD KEY `HoraInicio` (`HoraInicio`),
  ADD KEY `HoraFinal` (`HoraFinal`),
  ADD KEY `IDProfesor` (`IDProfesor`),
  ADD KEY `IDGrupo` (`IDGrupo`),
  ADD KEY `IDAsignatura` (`IDAsignatura`),
  ADD KEY `IDAula` (`IDAula`);

--
-- Indexes for table `horas`
--
ALTER TABLE `horas`
  ADD PRIMARY KEY (`IDHora`);

--
-- Indexes for table `pendientes`
--
ALTER TABLE `pendientes`
  ADD PRIMARY KEY (`IDPendiente`),
  ADD KEY `IdProfesor` (`IdProfesor`);

--
-- Indexes for table `profesores`
--
ALTER TABLE `profesores`
  ADD PRIMARY KEY (`IdProfesor`);

--
-- Indexes for table `temarios`
--
ALTER TABLE `temarios`
  ADD PRIMARY KEY (`IDTemario`),
  ADD KEY `IDAsignatura` (`IDAsignatura`);

--
-- Indexes for table `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`IDVisita`),
  ADD KEY `IDProfesor` (`IDProfesor`),
  ADD KEY `Grupo` (`Grupo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acciones_de_mejora`
--
ALTER TABLE `acciones_de_mejora`
  MODIFY `IDActMejora` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `IDAlumno` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `asignaturas`
--
ALTER TABLE `asignaturas`
  MODIFY `IDAsignatura` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `aulas`
--
ALTER TABLE `aulas`
  MODIFY `IDAula` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carreras`
--
ALTER TABLE `carreras`
  MODIFY `IDCarrera` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cursos`
--
ALTER TABLE `cursos`
  MODIFY `IDCurso` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `dias`
--
ALTER TABLE `dias`
  MODIFY `IDDia` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `grupos`
--
ALTER TABLE `grupos`
  MODIFY `IDGrupo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `horariodetalle`
--
ALTER TABLE `horariodetalle`
  MODIFY `IDHorario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `horas`
--
ALTER TABLE `horas`
  MODIFY `IDHora` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pendientes`
--
ALTER TABLE `pendientes`
  MODIFY `IDPendiente` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `profesores`
--
ALTER TABLE `profesores`
  MODIFY `IdProfesor` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `temarios`
--
ALTER TABLE `temarios`
  MODIFY `IDTemario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `visitas`
--
ALTER TABLE `visitas`
  MODIFY `IDVisita` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `acciones_de_mejora`
--
ALTER TABLE `acciones_de_mejora`
  ADD CONSTRAINT `acciones_de_mejora_ibfk_1` FOREIGN KEY (`IDAlumno`) REFERENCES `alumnos` (`IDAlumno`),
  ADD CONSTRAINT `acciones_de_mejora_ibfk_2` FOREIGN KEY (`IDProfesor`) REFERENCES `profesores` (`IdProfesor`);

--
-- Constraints for table `alumnos`
--
ALTER TABLE `alumnos`
  ADD CONSTRAINT `alumnos_ibfk_1` FOREIGN KEY (`IDGrupo`) REFERENCES `grupos` (`IDGrupo`),
  ADD CONSTRAINT `alumnos_ibfk_2` FOREIGN KEY (`IDProfesor`) REFERENCES `profesores` (`IdProfesor`);

--
-- Constraints for table `asignaturas`
--
ALTER TABLE `asignaturas`
  ADD CONSTRAINT `asignaturas_ibfk_1` FOREIGN KEY (`IDProfesor`) REFERENCES `profesores` (`IdProfesor`);

--
-- Constraints for table `cursos`
--
ALTER TABLE `cursos`
  ADD CONSTRAINT `cursos_ibfk_1` FOREIGN KEY (`IdProfesor`) REFERENCES `profesores` (`IdProfesor`);

--
-- Constraints for table `grupos`
--
ALTER TABLE `grupos`
  ADD CONSTRAINT `grupos_ibfk_1` FOREIGN KEY (`IDCarrera`) REFERENCES `carreras` (`IDCarrera`),
  ADD CONSTRAINT `grupos_ibfk_2` FOREIGN KEY (`IDProfesor`) REFERENCES `profesores` (`IdProfesor`);

--
-- Constraints for table `horariodetalle`
--
ALTER TABLE `horariodetalle`
  ADD CONSTRAINT `horariodetalle_ibfk_1` FOREIGN KEY (`Dia`) REFERENCES `dias` (`IDDia`),
  ADD CONSTRAINT `horariodetalle_ibfk_2` FOREIGN KEY (`HoraInicio`) REFERENCES `horas` (`IDHora`),
  ADD CONSTRAINT `horariodetalle_ibfk_3` FOREIGN KEY (`HoraFinal`) REFERENCES `horas` (`IDHora`),
  ADD CONSTRAINT `horariodetalle_ibfk_4` FOREIGN KEY (`IDProfesor`) REFERENCES `profesores` (`IdProfesor`),
  ADD CONSTRAINT `horariodetalle_ibfk_5` FOREIGN KEY (`IDGrupo`) REFERENCES `grupos` (`IDGrupo`),
  ADD CONSTRAINT `horariodetalle_ibfk_6` FOREIGN KEY (`IDAsignatura`) REFERENCES `asignaturas` (`IDAsignatura`),
  ADD CONSTRAINT `horariodetalle_ibfk_7` FOREIGN KEY (`IDAula`) REFERENCES `aulas` (`IDAula`);

--
-- Constraints for table `pendientes`
--
ALTER TABLE `pendientes`
  ADD CONSTRAINT `pendientes_ibfk_1` FOREIGN KEY (`IdProfesor`) REFERENCES `profesores` (`IdProfesor`);

--
-- Constraints for table `temarios`
--
ALTER TABLE `temarios`
  ADD CONSTRAINT `temarios_ibfk_1` FOREIGN KEY (`IDAsignatura`) REFERENCES `asignaturas` (`IDAsignatura`);

--
-- Constraints for table `visitas`
--
ALTER TABLE `visitas`
  ADD CONSTRAINT `visitas_ibfk_1` FOREIGN KEY (`IDProfesor`) REFERENCES `profesores` (`IdProfesor`),
  ADD CONSTRAINT `visitas_ibfk_2` FOREIGN KEY (`Grupo`) REFERENCES `grupos` (`IDGrupo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
