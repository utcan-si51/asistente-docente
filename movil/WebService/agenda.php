<?php

require_once('./config/Conexion.php');

class agenda {	
	private $db;
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function nuevoPendiente($IdProfesor,$TituloPendiente,$CuerpoPendiente,$FechaHora){
		$json = array();
		$query = 'INSERT INTO pendientes(IdProfesor, TituloPendiente, CuerpoPendiente, FechaHora) VALUES("'.$IdProfesor.'","'.$TituloPendiente.'","'.$CuerpoPendiente.'","'.$FechaHora.'")';
		if($this->db->insertar($query)){
			$json['success'] = 1;
			$json['message'] = "Pendiente Agregado!";
		}else{
			$json['success'] = 0;
			$json['message'] = "Error";
			$json['error'] = $query;
			$json['exists'] = $existe;
		}
		
		return json_encode($json);
	}
	
	public function concluirPendiente($id,$FechaConcluido){
		$json = array();
		$query = 'SELECT IDPendiente FROM pendientes WHERE IDPendiente = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Este pendiente no existe!";
		}else{
			$query = 'UPDATE pendientes SET Estatus = 1, FechaHoraConcluido = "'.$FechaConcluido.'"  WHERE IDPendiente ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Pendiente fue concluido!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json);
	}
	
	public function modificarPendiente($id,$IdProfesor,$TituloPendiente,$CuerpoPendiente,$FechaHora){
		$json = array();
		$query = 'SELECT IDPendiente FROM pendientes WHERE IDPendiente = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Este pendiente no existe!";
		}else{
			$query = 'UPDATE pendientes SET IdProfesor = "'.$IdProfesor.'", TituloPendiente = "'.$TituloPendiente.'", CuerpoPendiente = "'.$CuerpoPendiente.'", FechaHora = "'.$FechaHora.'" WHERE IDPendiente ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Pendiente actualizado!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json, JSON_UNESCAPED_UNICODE);	
	}
	
	public function listarPendientes($idUsuario){
		$json = "";
		$query = 'SELECT * FROM pendientes WHERE Estatus != 1 AND IdProfesor ="'.$idUsuario.'" ORDER BY FechaHora;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'pendientes': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDPendiente'],'idProfesor' => $datos['IdProfesor'],'titulo' => $datos['TituloPendiente'],'cuerpo' => $datos['CuerpoPendiente'],'fechainicio' => $datos['FechaHora'],'fechafin' => $datos['FechaHoraConcluido']));
			}else{
				$json .= json_encode(array('id' => $datos['IDPendiente'],'idProfesor' => $datos['IdProfesor'],'titulo' => $datos['TituloPendiente'],'cuerpo' => $datos['CuerpoPendiente'],'fechainicio' => $datos['FechaHora'],'fechafin' => $datos['FechaHoraConcluido'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
}
?>