<?php

require_once('./config/Conexion.php');

class grupo {	
	private $db;
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function nuevoGrupo($nombre,$idCarrera,$idProfesor){
		$json = array();
		$query = 'SELECT NombreGrupo FROM grupos WHERE NombreGrupo = "'.$nombre.'" AND Estatus != 0';
		$result = $this->db->totalRegistros($query);
		
		if($result == 1){
			$json['success'] = 2;
			$json['message'] = "Este grupo ya existe!";
		}else{
			$query = 'INSERT INTO grupos(NombreGrupo, IDCarrera) VALUES("'.$nombre.'","'.$idCarrera.'")';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Grupo agregado!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
				$json['error'] = $query;
				$json['exists'] = $existe;
			}
		}
		
		return json_encode($json);
	}
	
	public function bajaGrupo($id){
		$json = array();
		$query = 'SELECT IDGrupo FROM grupos WHERE IDGrupo = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Este grupo no existe!";
		}else{
			$query = 'UPDATE grupos SET Estatus = 1 WHERE IDGrupo ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Grupo fue dado de baja!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json);
	}
	
	public function listarGrupos(){
		$json = "";
		$query = 'SELECT grupos.IDGrupo, grupos.NombreGrupo, carreras.NombreCarrera FROM grupos JOIN carreras ON grupos.IDCarrera = carreras.IDCarrera WHERE Estatus != 1;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'grupos': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IDGrupo'],'NombreGrupo' => $datos['NombreGrupo'],'NombreCarrera' => $datos['NombreCarrera']));
			}else{
				$json .= json_encode(array('id' => $datos['IDGrupo'],'NombreGrupo' => $datos['NombreGrupo'],'NombreCarrera' => $datos['NombreCarrera'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
}
?>