<?php

require_once('./config/Conexion.php');

class user {	
	private $db;
	private $table = "profesores";
	
	public function __construct(){
		$this->db = new Conexion();
	}
	
	public function existeUsuario($MatriculaProfesor){
		$json = array();
		$query = 'SELECT MatriculaProfesor FROM profesores WHERE MatriculaProfesor = "'.$MatriculaProfesor.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 1){
			$json['success'] = 1;
			$json['message'] = "Esta matricula existe!";
		} else {
			$json['success'] = 0;
			$json['message'] = "Esta matricula no existe!";
		}
		
		return json_encode($json);
	}
	
	public function nuevoUsuario($matricula,$contra,$nombre,$apellido,$tipo){
		$json = array();
		$query = 'SELECT MatriculaProfesor FROM profesores WHERE MatriculaProfesor = "'.$matricula.'"';
		$result = $this->db->totalRegistros($query);
		
		if($result == 1){
			$json['success'] = 2;
			$json['message'] = "Esta matricula ya existe para un usuario agregado anteriormente";
		}else{
			$query = 'INSERT INTO profesores(MatriculaProfesor, Contra, NombreProfesor, ApellidoProfesor, TipoProfesor) VALUES("'.$matricula.'","'.$contra.'","'.$nombre.'","'.$apellido.'","'.$tipo.'")';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Profesor agregado!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
				$json['error'] = $query;
				$json['exists'] = $existe;
			}
		}
		
		return json_encode($json);
	}
	
	public function login($matricula, $contra){
		$json = array();
		$query = 'SELECT MatriculaProfesor FROM profesores WHERE MatriculaProfesor ="'.$matricula.'" AND Contra = "'.$contra.'"';
		$result = $this->db->totalRegistros($query);
		
		$value = false;
		if($result == 1){
			$value = true;
			
			$query = 'SELECT TipoProfesor FROM profesores WHERE MatriculaProfesor ="'.$matricula.'"';
			$result = $this->db->traerValores($query);
			
			$json['success'] = 1;
			$json['message'] = "Login correcto!";
			$json['tipo'] = $result['TipoProfesor'];

		} else {
			$value = false;
			$json['success'] = 0;
			$json['message'] = "Datos incorrectos!";
		}
		
		return json_encode($json);
	}
	
	public function baja($id){
		$json = array();
		$query = 'SELECT IdProfesor FROM profesores WHERE IdProfesor = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Esta matricula no existe!";
		}else{
			$query = 'UPDATE profesores SET Estatus = 1 WHERE IdProfesor ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Profesor fue dado de baja!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json);
	}
	
	public function updateContra($id,$contra){
		$json = array();
		$query = 'SELECT IdProfesor FROM profesores WHERE IdProfesor = "'.$id.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Este usuario no existe!";
		}else{
			$query = 'UPDATE profesores SET Contra = "'.$contra.'" WHERE IdProfesor ="'.$id.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Contraseña actualizada!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json, JSON_UNESCAPED_UNICODE);	
	}	
	
	public function actualizar($matricula,$contra,$nombre,$apellido,$tipo){
		$json = array();
		$query = 'SELECT MatriculaProfesor FROM profesores WHERE MatriculaProfesor = "'.$matricula.'" AND Estatus != 1';
		$result = $this->db->totalRegistros($query);
		
		if($result == 0){
			$json['success'] = 0;
			$json['message'] = "Esta matricula no existe!";
		}else{
			$query = 'UPDATE profesores SET Contra = "'.$contra.'", NombreProfesor = "'.$nombre.'", ApellidoProfesor = "'.$apellido.'", TipoProfesor = "'.$tipo.'" WHERE MatriculaProfesor ="'.$matricula.'"';
			if($this->db->insertar($query)){
				$json['success'] = 1;
				$json['message'] = "Profesor fue actualizado!";
			}else{
				$json['success'] = 0;
				$json['message'] = "Error";
			}
		}
		
		return json_encode($json);	
	}
	
	public function listar(){
		$json = "";
		$query = 'SELECT * FROM profesores WHERE TipoProfesor != 2 AND Estatus != 1;';
		$result = array_filter($this->db->seleccionarValores($query));
		$json.="{'usuarios': [";
		
		$i = 0;
		$cant = count($result);
		
		foreach($result as $datos){
			
			if ($i == $cant - 1) {
				$json .= json_encode(array('id' => $datos['IdProfesor'],'matricula' => $datos['MatriculaProfesor'],'nombre' => $datos['NombreProfesor'],'apellido' => $datos['ApellidoProfesor'],'tipo' => $datos['TipoProfesor']));
			}else{
				$json .= json_encode(array('id' => $datos['IdProfesor'],'matricula' => $datos['MatriculaProfesor'],'nombre' => $datos['NombreProfesor'],'apellido' => $datos['ApellidoProfesor'],'tipo' => $datos['TipoProfesor'])).",";
			}
			$i++;

		}
		$json.="]}";
		return $json;
	}
	
	public function buscar($busqueda, $valor){
		$json = array();
		$result = null;
		
		if($busqueda == 1){
			
			$query = 'SELECT * FROM profesores WHERE IdProfesor LIKE "%'.$valor.'%" AND TipoProfesor != 2';
			$result = array_filter($this->db->seleccionarValores($query));
			
		}else if($busqueda == 2){
			
			$query = 'SELECT * FROM profesores WHERE MatriculaProfesor LIKE "%'.$valor.'%" AND TipoProfesor != 2';
			$result = array_filter($this->db->seleccionarValores($query));
						
		}else if($busqueda == 3){
			
			$query = 'SELECT * FROM profesores WHERE NombreProfesor LIKE "%'.$valor.'%" AND TipoProfesor != 2';
			$result = array_filter($this->db->seleccionarValores($query));
						
		}else if($busqueda == 4){
			
			$query = 'SELECT * FROM profesores WHERE ApellidoProfesor LIKE "%'.$valor.'%" AND TipoProfesor != 2';
			$result = array_filter($this->db->seleccionarValores($query));
						
		}else if($busqueda == 5){
			
			$query = 'SELECT * FROM profesores WHERE TipoProfesor LIKE "%'.$valor.'%" AND TipoProfesor != 2';
			$result = array_filter($this->db->seleccionarValores($query));
						
		}else if($busqueda == 6){
			
			$query = 'SELECT * FROM profesores WHERE Estatus LIKE "%'.$valor.'%" AND TipoProfesor != 2';
			$result = array_filter($this->db->seleccionarValores($query));
						
		}
		
		foreach($result as $datos){
			$json[$datos['IdProfesor']] = array($datos['IdProfesor'],$datos['MatriculaProfesor'],$datos['NombreProfesor'],$datos['ApellidoProfesor'],$datos['TipoProfesor']);
		}
		
		return json_encode($json);
	}
}
?>