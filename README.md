# Asistente Docente

En este proyecto se desarrollo un asistente docente para llevar el control de actividades, tareas asignadas a los alumnos, control de horarios y una agenda para anotar sus compromisos que tiene dentro de la institución.


# Lenguajes de programacion utilizados:

Para este proyecto se utilizo HTML, CSS, PHP y Javascript (AJAX).



# Desarollado por:
Josue Ismael Caamal Ek
Marcos Antonio Palomino Cruz
Marc Julian Papke Vosselmann

# Video Android Studio:
https://youtu.be/3SFiRgFMIn4